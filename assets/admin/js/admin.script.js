jQuery(document).ready(function ($) {

	var changeCount = 0;

	/* Parameters for featured image
	********************************/
	$imageTestMode = false;
	$areaX = 0;
	$areaY = 0;
	$areaW = 0;
	$areaH = 0;


	/*
	var savedTabNo = getCookie("lastClickedTab");


	if (savedTabNo == '') savedTabNo = 1;


	var storedTabNo = Number(savedTabNo);
	var storedTab = $('.ub_tab_header[data-tab="' + storedTabNo + '"]');
	
	var isSelected = $(storedTab).hasClass("selected");
	var isDisabled = $(storedTab).hasClass("disabledTab");

	
	if (isSelected == false && isDisabled === false) {

		var currentlySelectedTabHeader = $(".ub_tab_header.selected");
		var currentlySelectedTabNo = $(currentlySelectedTabHeader).data("tab");
		var currentlySelectedTabPage = $('.ub_tab_page[data-tab="' + currentlySelectedTabNo + '"]');

		currentlySelectedTabHeader.removeClass("selected");
		currentlySelectedTabPage.addClass("hiddenTabPage");

		$(storedTab).addClass("selected");
		
		var clickedTabPage = $('.ub_tab_page[data-tab="' + storedTabNo + '"]');
		clickedTabPage.removeClass("hiddenTabPage");

	}



	setCookie("lastClickedTab", storedTabNo, 60);
	*/
	

	$selectedProductID = $("#ubwchsd_saved_product_id").val();
	
	if ($selectedProductID != '') {
		
		$("#ubwchsd-product-selection > table").hide();
		
	}

	

    $(".ub_tab_header").click(function(){

        var clickedTabNo = $(this).data("tab");

        
        var isSelected = $(this).hasClass("selected");
        var isDisabled = $(this).hasClass("disabledTab");

        if (isSelected == false && isDisabled === false) {

            var currentlySelectedTabHeader = $(".ub_tab_header.selected");
            var currentlySelectedTabNo = $(currentlySelectedTabHeader).data("tab");
            var currentlySelectedTabPage = $('.ub_tab_page[data-tab="' + currentlySelectedTabNo + '"]');

            currentlySelectedTabHeader.removeClass("selected");
            currentlySelectedTabPage.addClass("hiddenTabPage");

            $(this).addClass("selected");
            
            var clickedTabPage = $('.ub_tab_page[data-tab="' + clickedTabNo + '"]');
            clickedTabPage.removeClass("hiddenTabPage");

			setCookie("lastClickedTab", clickedTabNo,60);
        }

    });
	
	/* Show table
	**************************/
	$("#showProductSelectionTable").click(function(e){
		
		e.preventDefault();
		
		$("#ubwchsd-product-selection > table").fadeIn();
		$(this).fadeOut();
		
	});

	/* Hide table
	**************************/
	$("#btnCancelProductSelection").click(function(e){

		e.preventDefault();

		$("#ubwchsd-product-selection > table").fadeOut();
		$("#showProductSelectionTable").fadeIn();

	});



    /* Plugin status change
     * **********************************************/

    $(".toggle-switch-plugin-status").bootstrapToggle({
        on: 'Enabled',
        off: 'Disabled',
        onstyle: 'success',
        offstyle: 'secondary'
    });

    $('.toggle-switch-plugin-status').change(function () {

        var isEnabled = $(this).prop('checked');
        var spinnerDiv = $(".spinner-pluginstatus");

        $(spinnerDiv).addClass("ubwchsd-spinner-visible");


        var data = {
            'action' : 'setPluginStatus',
            'isEnabled' : isEnabled
        };

        

        $.post(ajax_object.ajax_url, data, function (response) {

            $(spinnerDiv).removeClass("ubwchsd-spinner-visible");
            
        });
    })



	/* Save selected product
	**************************/
	$('#ubwchsd-save_selected_product').click(function(e){
		
		e.preventDefault();
		
		var spinnerDiv = $(".spinner-selected-product-save");
        $(spinnerDiv).addClass("ubwchsd-spinner-visible");
		
		var selectedProductID = $("#sb_all_products option:selected").val();
		var selectedProductName = $("#sb_all_products option:selected").text();

        var data = {
            'action' : 'setSelectedProduct',
            'selectedProductID' : selectedProductID,
			'selectedProductName' : selectedProductName
        };
		
        $.post(ajax_object.ajax_url, data, function (response) {

            $(spinnerDiv).removeClass("ubwchsd-spinner-visible");
			
			document.location.reload(true);
            
        });		
		

	})

	

	/* Save product attribute settings
	**********************************/
	$('#ubwchsd-save_variations').click(function(e){
		
		e.preventDefault();
		
		hideErrorBox();

		var sizeAttribute = $('#sb_size_variation').val();
		var finishingOptionsAttribute = $('#sb_finishing_options_variation').val();
		var finishingTypeAttribute = $("#sb_finishing_type").val();
		
		if (sizeAttribute == "none" || finishingOptionsAttribute == "none" || finishingTypeAttribute == "none") {
			
			displayErrorBox("ERROR : Please select an attribute in all dropdown lists");
			return false;
			
		}
		
		if (sizeAttribute == finishingOptionsAttribute || sizeAttribute == finishingTypeAttribute || finishingOptionsAttribute == finishingTypeAttribute) {
			
			displayErrorBox("ERROR : Please select different attributes");
			return false;
			
		}		
		
		var spinnerDiv = $(".spinner-variation-props-save");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");
		
		var data = {
			'action' : 'setSizeAndFinishingAttributes',
			'sizeVar' : sizeAttribute,
			'finishingVar' : finishingOptionsAttribute,
			'finishingTypeVar' : finishingTypeAttribute
			
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            $(spinnerDiv).removeClass("ubwchsd-spinner-visible");

			document.location.reload(true);
            
        });

		
		return false;
		
	});


	/* Save size attribute value settings
	**********************************/
	$('#ubwchsd-save_attribute_values').click(function(e){

		e.preventDefault();

		var spinnerDiv = $(".spinner-save-size-attribute-values");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");

		var value_prices = [];

		$(".size_attribute_values").each(function(i, obj) {
			
			var value_slug=	$(obj).data("value-slug");
			var value_attr_record_id = $(obj).data("value-attr-record-id");
			var price_type = $(obj).data("value-price-type");
			var price = $(obj).val();

			value_prices.push([value_slug, value_attr_record_id, price_type, price]);

		});

		var data = {
			'action' : 'saveSizeAttributeValues',
			'sizeAttrValue' : JSON.stringify(value_prices)
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            $(spinnerDiv).removeClass("ubwchsd-spinner-visible");

			$("#variations_list").html(response.data);
        });

	});	


	/* Save finishing type attribute value settings
	***********************************************/
	$('#ubwchsd-save_finishing_type_values').click(function(e){

		e.preventDefault();

		var spinnerDiv = $(".spinner-save_finishing_type_values");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");

		var finishing_type_prices = [];

		$(".finishing_type_attribute_values").each(function(i, obj) {
			
			var value_slug=	$(obj).data("value-slug");
			var value_attr_record_id = $(obj).data("value-attr-record-id");
			var price_type = $(obj).data("value-price-type");
			var price = $(obj).val();

			finishing_type_prices.push([value_slug, value_attr_record_id, price_type, price]);

		});

		var data = {
			'action' : 'saveFinishingTypeValues',
			'finishingTypeValue' : JSON.stringify(finishing_type_prices)
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            $(spinnerDiv).removeClass("ubwchsd-spinner-visible");

			$("#variations_list").html(response.data);
            
        });

	});	





	/* Delete all variations
	**************************/
	$('#ubwchsd-remove-all-variations').click(function(e){

		e.preventDefault();

		var productID = $(this).data('product-id');


		var data = {
			'action' : 'deleteAllVariations',
			'selectedProductID' : productID
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            
        });

	});		

	

	/* Create all variations
	**************************/
	$('#ubwchsd-create-all-variations').click(function(e){

		e.preventDefault();

		var data = {
			'action' : 'createMaxVariations'
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            
        });

	});			


	/* Remove and create all variations
	***********************************/
	$('#ubwchsd-remove-all-variations-and-create-new').click(function(e){

		e.preventDefault();

		let result = window.confirm("All existing variations will be deleted and created anew.\n\nAre you sure you want to continue ?");

		if (result === false) return false;		

		var spinnerDiv = $(".spinner-remove-and-create-variations");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");		

		var data = {
			'action' : 'deleteAllVariationsAndCreateAllVariants'
		};

		$.post(ajax_object.ajax_url, data, function (response) {

			$(spinnerDiv).removeClass("ubwchsd-spinner-visible");

			$("#variations_list").html(response.data);

            
        });		

	});


	/* Create all variations
	***********************************/
	$('#ubwchsd-create-new-variations').click(function(e){

		e.preventDefault();

		var spinnerDiv = $(".spinner-create-all-variations");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");		

		var data = {
			'action' : 'deleteAllVariationsAndCreateAllVariants'
		};

		$.post(ajax_object.ajax_url, data, function (response) {

			$(spinnerDiv).removeClass("ubwchsd-spinner-visible");

			document.location.reload(true);

            
        });		

	});	



	/* Update variation prices
	***************************/
	$('#ubwchsd-update-variation-prices').click(function(e){


		var spinnerDiv = $(".spinner-remove-and-create-variations");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");		

		var data = {
			'action' : 'updateVariationPrices'
		};

		$.post(ajax_object.ajax_url, data, function (response) {

			$(spinnerDiv).removeClass("ubwchsd-spinner-visible");
            //document.location.reload(true);
			
			$("#variations_list").html(response.data);

        });		

	});	

	

	function displayErrorBox(message) {
		
		$(".errorBox").text(message).fadeIn();
		
		setTimeout(hideErrorBox, 5000);
	}
	
	function hideErrorBox() {
		
		$(".errorBox").fadeOut();
	}

	function setCookie(cname, cvalue, exSeconds) {
		const d = new Date();
		d.setTime(d.getTime() + (exSeconds * 1000));
		let expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	  }
	  
	  function getCookie(cname) {
		let name = cname + "=";
		let ca = document.cookie.split(';');
		for(let i = 0; i < ca.length; i++) {
		  let c = ca[i];
		  while (c.charAt(0) == ' ') {
			c = c.substring(1);
		  }
		  if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		  }
		}
		return "";
	  }	

	  


	$("#link_delete_options").click(function(e){

		e.preventDefault();

		let result = window.confirm("Emin misin ?");

		if (result === false) return false;

		var data = {
			'action' : 'deleteOptions'
		};

		$.post(ajax_object.ajax_url, data, function (response) {

            document.location.reload(true);

        });


	});

	

	var croppr;

	function createRegionSelectionObject() {

		croppr = new Croppr('#img_large_variation', {
			aspectRatio: null,
			maxSize: { width: null, height: null },
			minSize: { width: null, height: null },
			startSize: { width: 50, height: 50, unit: 'px' },
			returnMode: 'real',
			onInitialize: (instance) => { console.log(instance); },
			onCropStart: (data) => { getCropprCoordinates(data); },
			onCropEnd: (data) => { getCropprCoordinates(data); },
			onCropMove: (data) => {  getCropprCoordinates(data); }
		});			


	}

	



	/* Read saved parameters
	***********************/
	var saved_region_X = $("#ubwchsd_selected_region_x").val();
	var saved_region_Y = $("#ubwchsd_selected_region_y").val();
	var saved_region_W = $("#ubwchsd_selected_region_w").val();
	var saved_region_H = $("#ubwchsd_selected_region_h").val();

	
	if ($(".variation_image_panel").length) createRegionSelectionObject();


	var initial_region_x = saved_region_W == '' ? 50 : parseInt(saved_region_X);
	var initial_region_y = saved_region_W == '' ? 50 : parseInt(saved_region_Y);
	var initial_region_w = saved_region_W == '' ? 100 : parseInt(saved_region_W);
	var initial_region_h = saved_region_W == '' ? 100 : parseInt(saved_region_H);

	if ($(".variation_image_panel").length) croppr.resizeTo(initial_region_w, initial_region_h);
	if ($(".variation_image_panel").length) croppr.moveTo(initial_region_x, initial_region_y);

	$("#selected_area_width").val(initial_region_w);
	$("#selected_area_height").val(initial_region_h);
	$("#selected_area_x").val(initial_region_x);
	$("#selected_area_y").val(initial_region_y);

	



	function getCropprCoordinates(data) {

		var posX = data.x;
		var posY = data.y;
		var areaW = data.width;
		var areaH = data.height;

		if (!isFinite(posX) || !isFinite(posY) || !isFinite(areaW) || !isFinite(areaH)) {

			posX = '';
			posY = '';
			areaW = '';
			areaH = '';
		}

		$("#selected_area_x").val(posX);
		$("#selected_area_y").val(posY);
		$("#selected_area_width").val(areaW);
		$("#selected_area_height").val(areaH);

	}



	$("#ubwchsd-test-plaque").click(function(e){

		e.preventDefault();


		if ($imageTestMode) {

			$imageTestMode = false;

			$(this).prop("value", "Start testing settings");

			$(".variation_image_panel table").show();
					
			$(".croppr-overlay,.croppr-handle, .croppr-region").show();

			$(".featured_image_properties table.tbl-region-settings").hide();

			$("#ubwchsd-save-parameters").show();

			$areaX = $("#selected_area_x").val();
			$areaY = $("#selected_area_y").val();
			$areaW = $("#selected_area_width").val();
			$areaH = $("#selected_area_height").val();


			croppr.resizeTo(parseInt($areaW), parseInt($areaH));
			croppr.moveTo(parseInt($areaX), parseInt($areaY));
			
			return;

		}				


		var regionW = $("#selected_area_width").val();
		var regionH = $("#selected_area_height").val();

		if (regionW == "" || regionH == "") {

			alert("Please select a region.");
			return false;
		}

		var iRegionW = parseInt(regionW);
		var iRegionH = parseInt(regionH);

		if (iRegionW < 200 || iRegionH < 100) {

			alert("Min. width is 200 and min. height is 100.");
			return false;

		}


		if ($imageTestMode === false) {

			$imageTestMode = true;

			$(this).prop("value", "Cancel testing");

			$areaX = $("#selected_area_x").val();
			$areaY = $("#selected_area_y").val();
			$areaW = $("#selected_area_width").val();
			$areaH = $("#selected_area_height").val();

			$(".variation_image_panel table").hide();
			
			$(".croppr-overlay,.croppr-handle, .croppr-region").hide();
			
			$(".featured_image_properties table.tbl-region-settings").show();

			$("#ubwchsd-save-parameters").hide();
		}


		testSelectedRegionSettings();



	});


	function testSelectedRegionSettings() {

		var line1 = $("#test-line1").val();
		var line2 = $("#test-line2").val();	
		var featImg = $("#ubwchsd_url_variation_img").val();	

		line2 = line2.replace(/\n/g, "_lbr_");

		var x1 = $areaX;
		var y1 = $areaY;

		var x2 = parseInt(x1) + parseInt($areaW);
		var y2 = parseInt(y1) + parseInt($areaH);

		var url=$("#ubwchsd_img_editor_url").val();

		url += "?hsd_line1=" + line1;
		url += "&hsd_line2=" + line2;
		url += "&hsd_x1=" + x1;
		url += "&hsd_x2=" + x2;
		url += "&hsd_y1=" + y1;
		url += "&hsd_y2=" + y2;
		url += "&hsd_feat_img=" + featImg;
		url += "&hsd_font_name=" + $("#test-fonts").val();
		url += "&hsd_finishing=" + ($("#test-finishing").val());
		url += "&random=" + Math.random();

		$(".variation_image_panel img").attr("src", url);

	}



	$("#test-line1").on("keyup", function() {

		if ($imageTestMode) testSelectedRegionSettings();

	});


	$("#test-line2").on("keyup", function() {

		if ($imageTestMode) testSelectedRegionSettings();

	});	

	$("#test-finishing").change(function(){

		if ($imageTestMode) testSelectedRegionSettings();

	});	


	$("#test-fonts").change(function(){

		if ($imageTestMode) testSelectedRegionSettings();

	});	

	$("#ubwchsd-save-parameters").click(function(e){

		e.preventDefault();

		var regionW = $("#selected_area_width").val();
		var regionH = $("#selected_area_height").val();

		if (regionW == "" || regionH == "") {

			alert("Please select a region.");
			return false;
		}

		var iRegionW = parseInt(regionW);
		var iRegionH = parseInt(regionH);

		if (iRegionW < 200 || iRegionH < 100) {

			alert("Min. width is 200 and min. height is 100.");
			return false;

		}		

		var spinnerDiv = $(".spinner-save_region_values");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");			

		$areaX = $("#selected_area_x").val();
		$areaY = $("#selected_area_y").val();

		/* Save values into HTML
		*******************************/
		var elID = $("#cb_select_size").val();

		var selectedEl = $("#" + elID);

		$(selectedEl).attr("data-posx", $areaX);
		$(selectedEl).attr("data-posy", $areaY);
		$(selectedEl).attr("data-areaw", regionW);
		$(selectedEl).attr("data-areah", regionH);

		var productID = $("#ubwchsd_saved_product_id").val();
		var varIDs = $(selectedEl).attr("data-var-ids");

		var data = {
			'action' : 'setRegionParameters',
			'regionX' : $areaX,
			'regionY' : $areaY,
			'regionW' : regionW,
			'regionH' : regionH,
			'selectedProductID' : productID,
			'variationIDs' : varIDs
		};

		$.post(ajax_object.ajax_url, data, function (response) {

			$(spinnerDiv).removeClass("ubwchsd-spinner-visible");

        });		


	});

	$("#ubwchsd-create-new-wc-product").click(function(e){

		e.preventDefault();

		var productTitle = $("#hsd_new_product_title").val();

		if (productTitle == '') {

			alert("Please enter product title.");
			return false;

		}

		var spinnerDiv = $(".spinner-create-product-in-WC");
		$(spinnerDiv).addClass("ubwchsd-spinner-visible");			

	
		var data = {
			'action' : 'CreateProduct',
			'productTitle' : productTitle
		};

		$.post(ajax_object.ajax_url, data, function (response) {

			$(spinnerDiv).removeClass("ubwchsd-spinner-visible");
			document.location.reload(true);

        });				

	});

	$("#ubwchsd-choose-size").click(function (e){

		e.preventDefault();

	    var selectedSizeElID = $("#cb_select_size").val();

		if (selectedSizeElID == "") {

			$(".variation_image_properties_container").hide();
			return false;
		}

		var selectedField = $("#" + selectedSizeElID);

		var imgUrl = $(selectedField).attr("data-img");
		var imgFullSizeUrl = $(selectedField).attr("data-img-fullsize");
		var posX = $(selectedField).attr("data-posx");
		var posY = $(selectedField).attr("data-posy");
		var areaW = $(selectedField).attr("data-areaw");
		var areaH = $(selectedField).attr("data-areah");

		$("#ubwchsd_url_variation_img").val(imgUrl);
		$("#img_large_variation").prop("src", imgUrl);
		$("#img_large_variation").attr("data-original-image", imgUrl);
		$("#selected_area_x").val(posX);
		$("#selected_area_y").val(posY);
		$("#selected_area_width").val(areaW);
		$("#selected_area_height").val(areaH);

		//croppr.reset();
		croppr.setImage(imgUrl);
		//croppr.resizeTo(parseInt(areaW), parseInt(areaH));
		//croppr.moveTo(parseInt(posX), parseInt(posY));

		$(".variation_image_properties_container").show();


	});




	//#region CUSTOMER PHOTOS
	
	var chooseFallbackImage = false;


	/* Medial library functions
	**************************/
	var image_frame = wp.media({
		title: 'Select Media',
		multiple : true,
		library : {
			 type : 'image',
		 }
	});

	var image_frame_fb_image = wp.media({
		title: 'Select Media',
		multiple : false,
		library : {
			 type : 'image',
		 }
	});	
	
	image_frame.on('select',function() {

		

		var selection =  image_frame.state().get('selection');
		var ids = '';

		var panelUndefined = $(".ubwchsd-undefined-size");

		var imgPanel_pFont = '<p class="undefined-info">Font name not defined</p>';
		var imgPanel_pFinishingType = '<p class="undefined-info">Finishing type not defined</p>';

		var imgPanel_end = '</div>';

		var totalNew = 0;

		selection.each(function(attachment) {

			var attachment_id = attachment['id'];

			var attachment_url = attachment.toJSON().sizes.medium.url;
			
			var isExisting = $('[data-attachment-id="' + attachment_id + '"]').length > 0;
			

			if (!isExisting) {

				totalNew++;

				var imgPanel_start = '<div class="ubwchsd-image-panel" data-attachment-id="' + attachment_id + '"><div class="ubwchsd-spinner spinner-customer-photos-page"></div>';
				var attachment_file_name = attachment_url.substring(attachment_url.lastIndexOf('/')+1);

				var panelFileName = '<p class="img_fileName">' + attachment_file_name + '</p>';
				var img_html = '<img src="' + attachment_url + '" data-filename="' + attachment_file_name + '">';

				var result_html = imgPanel_start + img_html + imgPanel_pFont + imgPanel_pFinishingType + panelFileName + imgPanel_end;

				$(result_html).appendTo($(panelUndefined));

				

			}

		});

		//var ids = gallery_ids.join(",") + ",";
		//if(ids.length === 0) return true;//if closed withput selecting an image

		if (totalNew > 0) {

			$(".ub_tab_header").removeClass("selected");

			var tabHeaderUnsavedItems = $('.ub_tab_header[data-tab="100"]');
			var tabPageUnsavedItems = $('.ub_tab_page[data-tab="100"]');
	
			$(tabHeaderUnsavedItems).addClass("selected");
			$(tabPageUnsavedItems).removeClass("hiddenTabPage");

			resetInitialView();

		}
		
		
	});		  
	

	/* Fallback image uploader
	**************************/
	image_frame_fb_image.on('select',function() {

		var selection =  image_frame_fb_image.state().get('selection');
		var ids = '';

		

		var panelFBImage = $(".ubwchsd-default-image");

		var imgUrl = "";
		var attachment_id = "";
		var imgW = 0;
		var imgH = 0;

		selection.each(function(attachment) {


			if (imgUrl == "") {

				if (attachment.toJSON().sizes.large) {
					imgUrl = attachment.toJSON().sizes.large.url;
					imgW = attachment.toJSON().sizes.large.width;
					imgH = attachment.toJSON().sizes.large.height;
				}
				else {
					imgUrl = attachment.toJSON().sizes.full.url;
					imgW = attachment.toJSON().sizes.full.width;
					imgH = attachment.toJSON().sizes.full.height;
				}
			} 
		

		});

		/*
		if (imgW > imgH) {
			alert("Image's height needs to be more than its width (Width : " + imgW + " - Height : " + imgH + ")");
			return false;
		}
		*/

		var imgElement = $(panelFBImage).find('img');
		
		if ($(imgElement).length == 0) $(panelFBImage).append('<img src="">');
		
		$(panelFBImage).find('img').attr("src", imgUrl);

		/* Save
		*******/
		var data = {
			'action' : 'SetFallBackImage',
			'fallbackImageURL' : imgUrl,
			'fallbackImageW' : imgW,
			'fallbackImageH' : imgH
		};


		$.post(ajax_object.ajax_url, data, function (response) {});	


	});		  



	$("#ubwchsd-add-new-photo").click(function(){

		if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {


			$("#ubwchsd-new-image-ids").val('');

			image_frame.open();

        }

	});

	$("#ubwchsd-select-fallback-image").click(function(){

		if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {

			image_frame_fb_image.open();

        }

	});	



	$("body").on('click', '.ubwchsd-image-panel *', function(){

		if ($(this).parent().hasClass("selected-image")) {
			$(this).parent().removeClass("selected-image");
			$(this).parent().find('.img_fileName').fadeOut();
		}
		else {

			$(this).parent().addClass("selected-image");
			$(this).parent().find('.img_fileName').fadeIn();
		}

		var parentDiv = $(this).parent().parent();
		var totalSelected = $(parentDiv).find('.selected-image').length;

		var actionsPanel = $(".ubwchsd-actions-wrapper");

		if (totalSelected == 0) {

			$(actionsPanel).hide();

		}
		else {

			$(actionsPanel).show();
			setActionPanelParametersOnSelection();
		}


	});

	function setActionPanelParametersOnSelection() {

		var activeHeader = $(".ub_tab_header.selected");
		var activeTabID = $(activeHeader).data("tab");

		var activeTabContainer = $('.ub_tab_page[data-tab="' + activeTabID + '"]');

		var selectedImages = $(activeTabContainer).find('.selected-image'); 


		if ($(selectedImages).length == 1) {

			var relatedHiddenField = $("#ubwchsd_size_" + activeTabID);
			var relatedSizeName = $(relatedHiddenField).data("hidden-size-value");
			var relatedSizeClassName = getSizeClassName(relatedSizeName);

			var relatedFontName = $(selectedImages).first().find('p').first().text();
			var relatedFinishingType = $(selectedImages).first().find('p:not(.img_fileName)').last().text();

			$("#ubwchsd_size").val(relatedSizeClassName);
			$("#ubwchsd_fonts").val(relatedFontName);
			$("#ubwchsd_engraving").val(relatedFinishingType);

		}

	}


	function getSizeClassName(sizeText) {

		sizeText = sizeText.toLowerCase();
		sizeText = sizeText.replaceAll("cm", "");
		sizeText = sizeText.replaceAll(" ", "");

		return sizeText;
	}



	$("#ubwchsd-set-props").click(function(e){

		e.preventDefault();

		var activeHeader = $(".ub_tab_header.selected");
		var activeTabID = $(activeHeader).data("tab");

		var selectedFont = $("#ubwchsd_fonts").val();
		var selectedFinishingType = $("#ubwchsd_engraving").val();
		var selectedSize = $("#ubwchsd_size option:selected").text();

		var activeTabContainer = $('.ub_tab_page[data-tab="' + activeTabID + '"]');


		/* Get variation price
		**********************/
		var selectedSize_lc = selectedSize.toLowerCase();
		var selectedFinishingType_lc = selectedFinishingType.toLowerCase();

		var variation_price = '';

		var continueLoop = true;

		$(".hidden_variation_prices").each(function(){

			if (continueLoop) {

				var fieldSize = $(this).data("size").toLowerCase();
				var finishing = $(this).data("finishing").toLowerCase();
				var regular_price = $(this).data("regular-price");
				var sale_price = $(this).data("sale-price");

				if (fieldSize == selectedSize_lc && finishing == selectedFinishingType_lc) {

					variation_price = sale_price ? sale_price : regular_price;
					continueLoop = false;
				}

			}

		});

		
					
		/* Find size index
		******************/
		var size_field = $('*[data-hidden-size-value="' + selectedSize + '"]');
		var size_index = $(size_field).data("hidden-size-index");

		var relatedTab = $('.ub_tab_page[data-tab="' + size_index + '"]');
	

		var imgPanel_pFont = '<p>' + selectedFont + '</p>';
		var imgPanel_pFinishingType = '<p>' + selectedFinishingType + '</p>';

		var imgPanel_end = '</div>';

		var result_html = '';


		var totalSelectedImages = $(activeTabContainer).find('.selected-image').length;
		var processedImage = 0;


		setActionPanelBusy();

		$(activeTabContainer).find('.selected-image').each(function(){

			var thisImg = $(this);
			var spinnerDiv = $(this).find(".spinner-customer-photos-page");
			$(spinnerDiv).addClass("ubwchsd-spinner-visible");


			var attachment_id = $(this).data("attachment-id");
			var imgPanel_start = '<div class="ubwchsd-image-panel" data-attachment-id="' + attachment_id + '"><div class="ubwchsd-spinner spinner-customer-photos-page"></div>';
			var img_src = $(this).find('img').attr('src');
			var img_html = '<img src="' + img_src + '">';

			var data = {
				'action' : 'addUpdateCustomerPhoto',
				'image_attach_id' : attachment_id,
				'size_in_attribute' : selectedSize,
				'font_name' : selectedFont,
				'engraving_option' : selectedFinishingType,
				'variation_price' : variation_price
			};



			
			$.post(ajax_object.ajax_url, data, function (response) {

				processedImage++;

				$(spinnerDiv).removeClass("ubwchsd-spinner-visible");

				if (activeTabID == size_index) {

					$(thisImg).find('p').first().text(selectedFont);
					$(thisImg).find('p:not(.img_fileName)').last().text(selectedFinishingType);
					$(thisImg).removeClass("selected-image");

				}
				else {

					$(thisImg).remove();

				}
				
				if (processedImage == totalSelectedImages) {

					$(".ubwchsd-actions-wrapper").hide();
					resetActionPanel();
				} 

			});					
			

			result_html += imgPanel_start + img_html + imgPanel_pFont + imgPanel_pFinishingType + imgPanel_end;

			

		});


		if (activeTabID != size_index) {

			$(relatedTab).find('.ubwchsd-size-panel').first().append($(result_html));


			$(activeTabContainer).addClass("hiddenTabPage");
			$(relatedTab).removeClass("hiddenTabPage");
			$(relatedTab).addClass("hiddenTabPage");
			$(activeTabContainer).removeClass("hiddenTabPage");
		
		}

		

	});


	$("#ubwchsd-unselect-all").click(function(e){

		e.preventDefault();

		var activeHeader = $(".ub_tab_header.selected");
		var activeTabID = $(activeHeader).data("tab");

		var activeTabContainer = $('.ub_tab_page[data-tab="' + activeTabID + '"]');

		$(activeTabContainer).find('.selected-image').removeClass("selected-image");
		$(activeTabContainer).find('p.img_fileName').fadeOut();

		$(".ubwchsd-actions-wrapper").hide();

	});



	$("#ubwchsd-delete-selected").click(function(e){

		e.preventDefault();

		var activeHeader = $(".ub_tab_header.selected");
		var activeTabID = $(activeHeader).data("tab");
		
		var activeTabContainer = $('.ub_tab_page[data-tab="' + activeTabID + '"]');

		if (activeTabID == 100) {

			$(activeTabContainer).find('.selected-image').remove();

		}
		else 
		{

			var totalSelectedImages = $(activeTabContainer).find('.selected-image').length;
			var processedImage = 0;

			setActionPanelBusy();

			$(activeTabContainer).find('.selected-image').each(function(){

				var thisImg = $(this);
				var spinnerDiv = $(this).find(".spinner-customer-photos-page");
				$(spinnerDiv).addClass("ubwchsd-spinner-visible");
	
	
				var attachment_id = $(this).data("attachment-id");
	
				var data = {
					'action' : 'deleteCustomerPhoto',
					'image_attach_id' : attachment_id
				};
	
	
				$.post(ajax_object.ajax_url, data, function (response) {
	
					processedImage++;
					$(spinnerDiv).removeClass("ubwchsd-spinner-visible");
	
					$(thisImg).remove();			
					
					if (processedImage == totalSelectedImages) {

						$(".ubwchsd-actions-wrapper").hide();
						resetActionPanel();
					} 
	
				});					
				
				
	
			});			

		}

		refreshView();

		

	});


	function setActionPanelBusy() {

		$(".ubwchsd-panel-ivory, .ubwchsd-panel-white").hide();
		$(".ubwchsd-panel-yellow").show();

	}

	function resetActionPanel() {

		$(".ubwchsd-panel-ivory, .ubwchsd-panel-white").show();
		$(".ubwchsd-panel-yellow").hide();

	}


	function refreshView() {

		var activeHeader = $(".ub_tab_header.selected");
		var activeTabID = $(activeHeader).data("tab");

		var otherTabID = activeTabID != 100 ? 100 : 0;

		var activeTabContainer = $('.ub_tab_page[data-tab="' + activeTabID + '"]');
		var relatedTab = $('.ub_tab_page[data-tab="' + otherTabID + '"]');

		$(activeTabContainer).addClass("hiddenTabPage");
		$(relatedTab).removeClass("hiddenTabPage");
		$(relatedTab).addClass("hiddenTabPage");
		$(activeTabContainer).removeClass("hiddenTabPage");


	}

	function resetInitialView() {

		$(".ub_tabs_wrapper").addClass("hidden_wrapper");

		$(".ub_tab_header").removeClass("selected");
		$("ub_tab_page").removeClass("hiddenTabPage");
		
		$(".ubwchsd-size-info").each(function(){

			var id = $(this).data("hidden-size-index");
			
			$('.ub_tab_header[data-tab="' + id + '"]').addClass("selected");
			$('.ub_tab_page[data-tab="' + id + '"]').addClass("hiddenTabPage");

			$(".ub_tab_header").removeClass("selected");
			$("ub_tab_page").removeClass("hiddenTabPage");

		});

		$('.ub_tab_header[data-tab="100"]').addClass("selected");
		$('.ub_tab_page[data-tab="100"]').removeClass("hiddenTabPage");

		$(".ub_tabs_wrapper").removeClass("hidden_wrapper");

	}

	resetInitialView();

	//#endregion
		

});