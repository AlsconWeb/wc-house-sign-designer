(function ($) {

    var ubSlider = function ($container, options) {


		var sliderBlock = $container;
		var sliderClass = options === undefined || (options !== undefined && options.sliderClassName === undefined) ? 'ub_slide-image' : options.sliderClassName;



		initialize(sliderBlock);


		$container.parent().on("mouseenter", function(){

			$(".ubhsd-slider-left-arrow").animate({"left":"40px"}, "fast");
			$(".ubhsd-slider-right-arrow").animate({"right":"40px"}, "fast");

		});



		$container.parent().on("mouseleave", function(){

			$(".ubhsd-slider-left-arrow").animate({"left":"-50px"}, "fast");
			$(".ubhsd-slider-right-arrow").animate({"right":"-50px"}, "fast");

		});



		$("body").on("click",".ubhsd-slider-right-arrow", function(e){


			var currSlide = $(sliderBlock).find('.current-slide').first();
			var nextSlide = $(currSlide).next();

			if (nextSlide.length == 0) nextSlide = $(sliderBlock).find('.' + sliderClass).first();

			$(currSlide).removeClass("current-slide").fadeOut();
			$(nextSlide).addClass("current-slide").fadeIn();

			changeTexts();

		});



		$("body").on("click",".ubhsd-slider-left-arrow", function(e){

			var currSlide = $(sliderBlock).find('.current-slide').first();
			var prevSlide = $(currSlide).prev();

			if (prevSlide.length == 0) prevSlide = $(sliderBlock).find('.' + sliderClass).last();

			$(currSlide).removeClass("current-slide").fadeOut();
			$(prevSlide).addClass("current-slide").fadeIn();

			changeTexts();

		});



		function changeTexts() {

			var currSlide = $(sliderBlock).find('.current-slide').first();

			var fontName = $(currSlide).data("fontname");
			var finishingOption = $(currSlide).data("finishingoption");
			var price = $(currSlide).data("price");

			$(currSlide).parent().parent().find('p.ub_slider-block-font-name').text(fontName);
			$(currSlide).parent().parent().find('p.ub_slider-block-finishing-option').text(finishingOption);
			$(currSlide).parent().parent().find('p.ub_slider-block-price').html("&pound;" + price);

		}


		function initialize(container) {


			jQuery(container).find('.' + sliderClass).css("position", "absolute").hide();
			jQuery(container).find('.' + sliderClass).first().addClass("current-slide").show();

			jQuery(container).parent().append('<div class="ubhsd-slider-arrows"><div class="ubhsd-slider-left-arrow"><strong>&lt;</strong></div><div class="ubhsd-slider-right-arrow"><strong>&gt;</strong></div></div>');


		}

    };







    jQuery.fn.ubSlider = function (options) {
	    console.log($(this));
        return ubSlider($(this), options);
    };

}(jQuery));
