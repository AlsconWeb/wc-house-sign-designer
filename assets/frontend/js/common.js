jQuery(document).ready(function ($) {


	//#region --- Global variables ---

	var timer;
	var definedFonts = new Map();
	var changesWaiting = false;
	var loaderBusy = false;

	var lastUsedSize = '';
	var lastUsedFontName = '';
	var lastUsedFontSizeLine1 = 0;
	var lastUsedFontSizeLine2 = 0;
	var lastUsedLargeFontSizeLine1 = 0;
	var lastUsedLargeFontSizeLine2 = 0;

	var featuredImg;


	/* Featured image regions
	*************************/
	var areaX;
	var areaY;
	var regionW;
	var regionH;

	var areaLine2Y;
	var regionLine1H;
	var regionLine2H;

	var pLine1;
	var pLine2;
	var pLine1_overlay;
	var pLine2_overlay;


	/* Zoomed image regions
	*************************/
	var largeImg_areaX;
	var largeImg_areaY;
	var largeImg_regionW;
	var largeImg_regionH;

	var largeImg_areaLine2Y;
	var largeImg_regionLine1H;
	var largeImg_regionLine2H;

	var pLine1Large;
	var pLine2Large;
	var pLine1Large_overlay;
	var pLine2Large_overlay;

	var ratioLineH = 0.5;
	var ratioLineH_big = 0.7;

	//#endregion


	//#region --- Initial actions ---


	/* Apply font style to each font name in dropdown list*/
	let textStyleOptions = $('#text-style option');

	for (let i = 1; i < textStyleOptions.length; i++) {
		let element = textStyleOptions[i];
		let fontName = element.value;

		let fontFace = 'Courgette';

		if (fontName.includes('damento')) fontFace ='Fondamento';
		if (fontName.includes('askervi')) fontFace ='LibreBaskerville';
		if (fontName.includes('oboto')) fontFace ='Roboto';
		if (fontName.includes('oman')) fontFace ='TimesNewRoman';

		element.style.fontFamily = fontFace;
	}


	/* Wrap featured image with textplaceholder to display text*/
	var imgWrapper = $(".woocommerce-product-gallery__image");

	var divTextHTML = '<p class="ubhsd-text-placeholder ubhsd-pLine1 ubhsd-pLine1-text"></p><p class="ubhsd-text-placeholder ubhsd-pLine2 ubhsd-pLine2-text"></p><p class="ubhsd-text-placeholder ubhsd-pLine1 ubhsd-pLine1-overlay-text"></p><p class="ubhsd-text-placeholder ubhsd-pLine2 ubhsd-pLine2-overlay-text"></p>';

	$(divTextHTML).appendTo(imgWrapper);

	pLine1 = $("p.ubhsd-pLine1-text");
	pLine2 = $("p.ubhsd-pLine2-text");
	pLine1_overlay = $("p.ubhsd-pLine1-overlay-text");
	pLine2_overlay = $("p.ubhsd-pLine2-overlay-text");

	/* Place loader image on featured image*/
	var loader = $('<div class="picLoading"></div>');
	$(imgWrapper).append(loader);
	$(loader).hide();


	/* Move rating block in product page
	************************************/
	$(".woocommerce-product-rating").detach().insertAfter(".woocommerce-variation.single_variation");


	function populateFontsList() {

		$(".classVarSize label").text("Size");
		$(".classVarFinishing label").text("Engraving");
		//$('table.variations tr:eq(1) th label').text("Font");
		$(".et_pb_column.et_pb_column_2_5.et_pb_column_2.et_pb_css_mix_blend_mode_passthrough.et-last-child").show();


		var jsonFonts = $("#ubwchsd_fonts_list").val();

		var objFonts = JSON.parse(jsonFonts);

		for(var key in objFonts) {

			if (objFonts.hasOwnProperty(key)) {

				definedFonts.set(objFonts[key]["font_name"], objFonts[key]["font_file_name"] + "|" + objFonts[key]["font_group"]);

			}

		}

	}


	function relocateCustomFields() {


		var inputLine1 = $(".hsd-line-1");
		var inputLine2 = $(".hsd-line-2");
		var inputNote = $("#ub_wchsd_note");

		let trLine1 = $('<tr class="tr-line-1"><th class="label"><label for="txt-line-1">Line 1<span style="color: red;">*</span></label></th><td class="value" data-is-span-added="1" id="td-txt-line-1"></td></tr>');
		let trLine2 = $('<tr><th class="label"><label for="txt-line-2">Line 2</label></th><td class="value" data-is-span-added="1" id="td-txt-line-2"></td></tr>');
		let trNote = $('<tr><th class="label"><label for="txt-note">Note</label></th><td class="value" data-is-span-added="1" id="td-txt-note"></td></tr>');

		let tableVariations = $("table.variations tr:first");

		trNote.insertAfter(tableVariations);
		trLine2.insertAfter(tableVariations);
		trLine1.insertAfter(tableVariations);

		inputLine1.appendTo("#td-txt-line-1");
		inputLine2.appendTo("#td-txt-line-2");
		inputNote.appendTo("#td-txt-note");

		$(".wcpa_form_outer").remove();

	}

	function addRemainingCharacterCountInformation(){

		var lineInfo = '<div class="line-info-wrapper"><p class="line-char-info">16 characters left</p></div>';

		var inputLine1 = $(".hsd-line-1");
		var inputLine2 = $(".hsd-line-2");

		$(lineInfo).insertAfter(inputLine1);
		$(lineInfo).insertAfter(inputLine2);

		var div = $(".line-info-wrapper");
		var h = $(div).height();

		$(div).data("initialHeight", h);

	}


	// relocateCustomFields();

	populateFontsList();

	addRemainingCharacterCountInformation();


	//#endregion


	//#region --- Lightbox functions ---

	$("p.ubhsd-pLine1-text, p.ubhsd-pLine2-text, p.ubhsd-pLine1-overlay-text, p.ubhsd-pLine2-overlay-text").on("click", function(){

		$(".ubwchsd-lb-wrapper").show();

	});



	$(".woocommerce-product-gallery__wrapper a").click(function(e){

		e.preventDefault();

		$(".ubwchsd-lb-wrapper").show();

	});



	$(".ubwchsd-lb-wrapper, .ubwchsd-lb-innerContainer").click(function(){

		$(".ubwchsd-lb-wrapper").hide();

	});



	/* Adjust size and positions of lightbox wrapper and
	   large texts
	 */

	function adjustLightboxAndLargeTextPositions() {

		largeImg_areaX = $("#ubwchsd_selected_region_x_large").val();
		largeImg_areaY = $("#ubwchsd_selected_region_y_large").val();
		largeImg_regionW = $("#ubwchsd_selected_region_w_large").val();
		largeImg_regionH = $("#ubwchsd_selected_region_h_large").val();
		var largeImg_url = $("#ubwchsd_url_featured_img_large").val();


		var large_img_w = $("#ubwchsd_large_img_w").val();
		var large_img_h = $("#ubwchsd_large_img_h").val();


		var lightboxImgContainer = $(".ubwchsd-lb-innerContainer");
		var lightboxImg = $(".ubwchsd-lb-innerContainer img");

		lightboxImgContainer.height(large_img_h);
		lightboxImgContainer.width(large_img_w);

		var window_w = $(window).innerWidth();
		var window_h = $(window).innerHeight();

		var lb_x = (window_w - large_img_w) / 2;
		var lb_y = (window_h - large_img_h) / 2;


		lightboxImgContainer.css("left", lb_x + "px");
		lightboxImgContainer.css("top", lb_y + "px");

		lightboxImg.css("width", large_img_w + "px");
		lightboxImg.css("height", large_img_h + "px");

		pLine1Large = $(".ubhsd-pLine1-text-large");
		pLine2Large = $(".ubhsd-pLine2-text-large");
		pLine1Large_overlay = $(".ubhsd-pLine1-overlay-text-large");
		pLine2Large_overlay = $(".ubhsd-pLine2-overlay-text-large");

	}

	//#endregion


	//#region --- Slider for photos from customers

	$('img[class^="ubhsd-slide-"]').unwrap("p");

	var totalSlides = 0;

	$('img[class^="ubhsd-slide-"]').each(function(){

		totalSlides++;

		$(this).wrap('<div class="ubhsd-pfoc-slide" data-slideID="' + totalSlides +'"></div>');

	});

	var arrows = $('<div class="ubhsd-slider-arrows"><div class="ubhsd-slider-left-arrow"><strong>&lt;</strong></div><div class="ubhsd-slider-right-arrow"><strong>&gt;</strong></div></div>');

	$(".et_pb_tab_1 .et_pb_tab_content").append($(arrows)).wrapInner('<div class="ubhsd-pfoc-slider"></div>');

	$(".ubhsd-pfoc-slide").hide();
	$(".ubhsd-pfoc-slide").eq(0).addClass("current").show();

	$(".ubhsd-pfoc-slider").on("mouseenter", function(){

		$(".ubhsd-slider-left-arrow").animate({"left":"50px"}, "fast");
		$(".ubhsd-slider-right-arrow").animate({"right":"50px"}, "fast");

	});

	$(".ubhsd-pfoc-slider").on("mouseleave", function(){

		$(".ubhsd-slider-left-arrow").animate({"left":"-50px"}, "fast");
		$(".ubhsd-slider-right-arrow").animate({"right":"-50px"}, "fast");

	});

	$("body").on("click",".ubhsd-slider-right-arrow", function(e){

		var currSlide = $('.ubhsd-pfoc-slide.current');
		var currSlideID = $(currSlide).attr("data-slideid");

		var iCurrSlideID = parseInt(currSlideID);
		var iNextSlideID = iCurrSlideID + 1;

		if (iNextSlideID > totalSlides) iNextSlideID = 1;

		var nextSlide = $('[data-slideid="' + iNextSlideID.toString() + '"]');

		$(currSlide).removeClass("current").fadeOut();
		$(nextSlide).addClass("current").fadeIn();

	});


	$("body").on("click",".ubhsd-slider-left-arrow", function(e){

		var currSlide = $('.ubhsd-pfoc-slide.current');
		var currSlideID = $(currSlide).attr("data-slideid");

		var iCurrSlideID = parseInt(currSlideID);
		var iPrevSlideID = iCurrSlideID - 1;

		if (iPrevSlideID < 1) iPrevSlideID = totalSlides;

		var prevSlide = $('[data-slideid="' + iPrevSlideID.toString() + '"]');

		$(currSlide).removeClass("current").fadeOut();
		$(prevSlide).addClass("current").fadeIn();

	});


	function displayRelevantSlide() {

		var selectedSizeText = $("#select-size").val().toLowerCase();

		if (lastUsedSize == selectedSizeText) return;

		lastUsedSize = selectedSizeText;


		selectedSizeText = selectedSizeText.replaceAll('cm','');
		selectedSizeText = selectedSizeText.replaceAll(' ','');

		var imgClass = ".ubhsd-slide-" + selectedSizeText

		var currSlide = $('.ubhsd-pfoc-slide.current');
		var nextSlide = $(imgClass).eq(0).parent();

		$(currSlide).removeClass("current").fadeOut();
		$(nextSlide).addClass("current").fadeIn();

	}

	//#endregion


	//#region --- Read variations and assign classes to rows ---

	var sizeVarSlug = $("#ubwchsd_attr_slug_size").val();
	var finishingVarSlug = $("#ubwchsd_attr_slug_finishing_type").val();
	var textStyleVarSlug = $("#ubwchsd_attr_slug_text_style").val();


	var selectBoxSizeTRParent = $("#" + sizeVarSlug).parent().parent();
	var selectBoxFinishingTRParent = $("#" + finishingVarSlug).parent().parent();
	var selectBoxTextStyle =$("#" + textStyleVarSlug).parent().parent();

	let tableVariations = $("table.variations tr:first");


	$(selectBoxSizeTRParent).children().addClass("classVarSize");
	$(selectBoxFinishingTRParent).children().addClass("classVarFinishing");

	$(selectBoxFinishingTRParent).detach().insertAfter($(tableVariations));
	$(selectBoxTextStyle).detach().insertAfter($(tableVariations));

	//#endregion


	//#region --- TIMER FUNCTIONS ---


	function startTimer() {

		timer = setTimeout(applyImageEffects, 1000);

	}

	function stopTimer() {

		clearTimeout(timer);

	}

	//#endregion


	//#region --- TEXTBOX + SELECTBOX CHANGE EVENTS ---

	$("select#text-style").change(function(){

		applyImageEffects();

	});


	$("body").on("change","#ub_wchsd_line1", function(){

		applyImageEffects();

	});

	$("body").on("keyup","#ub_wchsd_line1", function(){

		lineChanged($(this), $(this).parent().find("p"));

	});

	$("body").on("change","#ub_wchsd_line2", function(e){

		applyImageEffects();

	});

	$("body").on("keyup","#ub_wchsd_line2", function(){

		lineChanged($(this), $(this).parent().find("p"));

	});



	function lineChanged(txtBox, pInfo) {


		var txtLen = $(txtBox).val().length;

		var remainingChars = 16 - txtLen;
		var charWord = remainingChars < 2 ? "character" : "characters";

		$(pInfo).text(remainingChars + " " + charWord + " left");

		if (remainingChars == 0) {

			$(pInfo).addClass("redText");
			$(pInfo).text("0 character left. Provide a text that is no longer than 16 characters.");

		}
		else {

			$(pInfo).removeClass("redText");

		}

		var pH = $(pInfo).outerHeight();

		var parentOfP = $(pInfo).parent();
		var h = remainingChars == 0 && pH > 30 ? pH : $(parentOfP).data("initialHeight");

		$(parentOfP).css("height", h);


	}

	//#endregion


	function adjustTextPosition() {

		featuredImg = $(".woocommerce-product-gallery__image img");

		regionW = $("#ubwchsd_selected_region_w").val();
		regionH = $("#ubwchsd_selected_region_h").val();
		areaX = $("#ubwchsd_selected_region_x").val();
		areaY = $("#ubwchsd_selected_region_y").val();

		var imgWidth = parseInt($(featuredImg).attr("width"));
		var imgHeight = parseInt($(featuredImg).attr("height"));
		var imgCalculatedW = $(featuredImg).width();
		var imgCalculatedH = $(featuredImg).height();

		var coefW = imgCalculatedW / imgWidth;
		var coefH = imgCalculatedH / imgHeight;

		regionW = parseInt(regionW * coefW);
		regionH = parseInt(regionH * coefH);
		areaX = parseInt(areaX * coefW);
		areaY = parseInt(areaY * coefH);

	}


	function sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
				break;
			}
		}
	}


	$(".woocommerce-product-gallery__image img").on("load", function(){

		applyImageEffects();
	});


	//#region --- APPLY CHANGES + DRAW TEXT ---


	function applyImageEffects() {

		loadVariationParameters();

		adjustTextPosition();
		adjustLightboxAndLargeTextPositions();

		var line1Text = $("#ub_wchsd_line1").val();
		var line2Text = $("#ub_wchsd_line2").val();

		var fontName = $("#text-style option:selected").val();


		var fontNameParam = encodeURI(definedFonts.get(fontName));

		var finishType = $("#engraving-options option:selected").val();

		if (finishType.toLowerCase().includes("white")) finishType = "uv";
		if (finishType.toLowerCase().includes("engraved")) finishType = "engraved";


		if (line1Text == '' || fontName == '' || finishType == '') return false;


		line2Text = line2Text.replace(/\n/g, "_lbr_");


		/* ADJUST RATIO IF ONE OF THE LINES IS NUMERIC */
		var line1Ratio = ratioLineH;

		var line1IntVal = parseInt(line1Text, 10);
		var line2IntVal = parseInt(line2Text, 10);

		if ((isNaN(line1IntVal) && !isNaN(line2IntVal)) || (!isNaN(line1IntVal) && isNaN(line2IntVal))) {

			line1Ratio = !isNaN(line1IntVal) ? ratioLineH_big : (1-ratioLineH_big);

		}

		line1Ratio = parseFloat(line1Ratio).toFixed(2);

		regionLine1H = regionH * line1Ratio;
		regionLine2H = regionH - regionLine1H;

		largeImg_regionLine1H = largeImg_regionH * line1Ratio;
		largeImg_regionLine2H = largeImg_regionH - largeImg_regionLine1H;

		areaLine2Y = areaY + regionLine1H;
		largeImg_areaLine2Y = parseInt(largeImg_areaY) + parseInt(largeImg_regionLine1H);


		/* SET CURRENT IMAGE AS BACKGROUND OF DIV */

		var featuredImageDiv = $(".woocommerce-product-gallery__image");
		var currImageSrc = $(featuredImg).attr("src");
		$(featuredImageDiv).css("background-image", "url(" + currImageSrc + ")");

		var x1 = areaX;
		var y1 = areaY;

		var x2 = parseInt(x1) + parseInt(regionW);
		var y2 = parseInt(y1) + parseInt(regionH);

		var url = $("#ubwchsd_img_editor_url_fsize").val();

		var newVarID = $("input.variation_id").val();
		var varEl = $("#v" + newVarID);

		url += "?hsd_line1=" + encodeURI(line1Text);
		url += "&hsd_line2=" + encodeURI(line2Text);
		url += "&hsd_x1=" + x1;
		url += "&hsd_x2=" + x2;
		url += "&hsd_y1=" + y1;
		url += "&hsd_y2=" + y2;
		url += "&hsd_font_name=" + fontNameParam;
		url += "&hsd_line1Ratio=" + (line1Ratio*100);
		url += "&random=" + Math.random();

		var fileURL = url;


		var fontFace = 'Courgette';
		if (fontName.includes('damento')) fontFace ='Fondamento';
		if (fontName.includes('askervi')) fontFace ='LibreBaskerville';
		if (fontName.includes('oboto')) fontFace ='Roboto';
		if (fontName.includes('oman')) fontFace ='TimesNewRoman';


		var request = $.ajax({
			url: fileURL,
			method: "GET"
		  });


		  request.done(function( msg ) {

			var fontSizes = msg.split('|');

			var line1FontSize = parseInt(fontSizes[0]);
			var line2FontSize = parseInt(fontSizes[1]);

			lastUsedFontSizeLine1 = line1FontSize;
			lastUsedFontSizeLine2 = line2FontSize;

			var line1BottomMargin = parseInt(fontSizes[2]);
			var line2BottomMargin = parseInt(fontSizes[3]);

			drawText(line1Text, line2Text, fontFace, lastUsedFontSizeLine1, lastUsedFontSizeLine2, (finishType == "uv" ? 0 : 1), line1BottomMargin, line2BottomMargin);

		  });



		  // LARGE IMAGE


		  var url = $("#ubwchsd_img_editor_url_fsize").val();

		  var large_x2 = parseInt(largeImg_areaX) + parseInt(largeImg_regionW);
		  var large_y2 = parseInt(largeImg_areaY) + parseInt(largeImg_regionH);

		  url += "?hsd_line1=" + encodeURI(line1Text);
		  url += "&hsd_line2=" + encodeURI(line2Text);
		  url += "&hsd_x1=" + largeImg_areaX;
		  url += "&hsd_x2=" + large_x2;
		  url += "&hsd_y1=" + largeImg_areaY
		  url += "&hsd_y2=" + large_y2;
		  url += "&hsd_font_name=" + fontNameParam;
		  url += "&hsd_line1Ratio=" + (line1Ratio*100);
		  url += "&random=" + Math.random();

		  var fileURL = url;

		  var request2 = $.ajax({
			url: fileURL,
			method: "GET"
		  });

		  request2.done(function( msg ) {

			var fontSizes = msg.split('|');

			var line1LargeFontSize = parseInt(fontSizes[0]);
			var line2LargeFontSize = parseInt(fontSizes[1]);

			lastUsedLargeFontSizeLine1 = line1LargeFontSize;
			lastUsedLargeFontSizeLine2 = line2LargeFontSize;

			drawText(line1Text, line2Text, fontFace, lastUsedLargeFontSizeLine1, lastUsedLargeFontSizeLine2, (finishType == "uv" ? 0 : 1), 0, 0, true);


		  });




	}

    function drawText(line1Text, line2Text, fontFace, line1fontSizeInPt, line2fontSizeInPt, isEngraved, line1_bottom_margin = 0, line2_bottom_margin = 0,  isLargeText=false) {


		var style_color = "color : #fff;";
		var style_overlay_color = "color : transparent;";


		if (isEngraved == 1) {

			style_color = "color : #c0c0c0;";
			style_overlay_color = "-webkit-text-stroke: 2px rgba(51,51,51,0.3); -webkit-text-fill-color: transparent;";

		}


		if (isLargeText == false) {

			var styleLine1 = 'font-family : ' + fontFace +  ' ; font-size : ' + line1fontSizeInPt  + 'pt; left : ' + areaX + 'px; top : ' + areaY + 'px; width : ' + regionW + 'px; height : ' + regionLine1H + 'px; line-height : ' + (regionLine1H-line1_bottom_margin) + 'px;';

			var styleLine2 = 'font-family : ' + fontFace +  ' ; font-size : ' + line2fontSizeInPt  + 'pt; left : ' + areaX + 'px; top : ' + areaLine2Y + 'px; width : ' + regionW + 'px; height : ' + regionLine2H + 'px; line-height : ' + (regionLine2H - line2_bottom_margin) + 'px;';



			$(pLine1).attr("style", styleLine1 + style_color);
			$(pLine2).attr("style", styleLine2 + style_color);

			$(pLine1_overlay).attr("style", styleLine1 + style_overlay_color);
			$(pLine2_overlay).attr("style", styleLine2 + style_overlay_color);

			$(pLine1).text(line1Text);
			$(pLine1_overlay).text(line1Text);

			$(pLine2).text(line2Text);
			$(pLine2_overlay).text(line2Text);

		}
		else
		{

			var styleLine1 = 'font-family : ' + fontFace +  ' ; font-size : ' + line1fontSizeInPt  + 'pt; left : ' + largeImg_areaX + 'px; top : ' + largeImg_areaY + 'px; width : ' + largeImg_regionW + 'px; height : ' + largeImg_regionLine1H + 'px; line-height : ' + (largeImg_regionLine1H-line1_bottom_margin) + 'px;';

			var styleLine2 = 'font-family : ' + fontFace +  ' ; font-size : ' + line2fontSizeInPt  + 'pt; left : ' + largeImg_areaX + 'px; top : ' + largeImg_areaLine2Y + 'px; width : ' + largeImg_regionW + 'px; height : ' + largeImg_regionLine2H + 'px; line-height : ' + (largeImg_regionLine2H - line2_bottom_margin) + 'px;';

			$(pLine1Large).attr("style", styleLine1 + style_color);
			$(pLine2Large).attr("style", styleLine2 + style_color);

			$(pLine1Large_overlay).attr("style", styleLine1 + style_overlay_color);
			$(pLine2Large_overlay).attr("style", styleLine2 + style_overlay_color);

			$(pLine1Large).text(line1Text);
			$(pLine1Large_overlay).text(line1Text);

			$(pLine2Large).text(line2Text);
			$(pLine2Large_overlay).text(line2Text);

		}


	}

	//#endregion




	// Added on 11.06.2023
	$("form").on("woocommerce_variation_has_changed", function(){
		displayRelevantSlide();

	});



	// Added on 11.06.2023
	function loadVariationParameters() {

		var newVarID = $("input.variation_id").val();

		if (newVarID == "") return ;

		var varEl = $("#v" + newVarID);

		$("#ubwchsd_selected_region_x").val($(varEl).attr("data-posx"));
		$("#ubwchsd_selected_region_y").val($(varEl).attr("data-posy"));
		$("#ubwchsd_selected_region_w").val($(varEl).attr("data-areaw"));
		$("#ubwchsd_selected_region_h").val($(varEl).attr("data-areah"));

		$("#ubwchsd_selected_region_x_large").val($(varEl).attr("data-largeposx"));
		$("#ubwchsd_selected_region_y_large").val($(varEl).attr("data-largeposy"));
		$("#ubwchsd_selected_region_w_large").val($(varEl).attr("data-largeareaw"));
		$("#ubwchsd_selected_region_h_large").val($(varEl).attr("data-largeareah"));

		$("#ubwchsd_url_featured_img").val($(varEl).attr("data-imgurl"));
		$("#ubwchsd_url_featured_img_large").val($(varEl).attr("data-largeimgurl"));

		$("#ubwchsd_large_img_w").val($(varEl).attr("data-largeimagew"));
		$("#ubwchsd_large_img_h").val($(varEl).attr("data-largeimageh"));

		$(".ubwchsd-lb-innerContainer img").attr("src", decodeURIComponent($(varEl).attr("data-largeimgurl")));

	}

	/* START UP
    ***************/

	DiviArea.addAction( 'show_area_custom', function( area ) {
		relocateCustomFields();

		setTimeout(()=>{
			applyImageEffects();
		}, 400);

		const images = $('.variations_form').data('product_variations');
		$('.images').css({'opacity':'1'})

		const sizes  = $('#select-size');

		sizes.change(function( e ){
			const value = $(this).val();

			for(const image of images){
				if(image.attributes['attribute_select-size'] === value){
					$('.wp-post-image').attr('src', image.image.src );
				}
			}
		});
	});
});





