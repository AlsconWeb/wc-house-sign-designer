<?php
/**
 * WooCommerce House Sign Designer Uninstall method
 * Called when plugin is deleted
 *
 */


if (!defined('WP_UNINSTALL_PLUGIN')) { die; }


function ubwchsd_delete_options() {
	

	delete_option("ub_wchsd_plugin_enabled");
	
		
	global $wpdb;

	$table_name = $wpdb->prefix."wchsd_db";
	
	$query = "DROP TABLE IF EXISTS `$table_name`";
	
	$wpdb->query($query);	
	

}

ubwchsd_delete_options();