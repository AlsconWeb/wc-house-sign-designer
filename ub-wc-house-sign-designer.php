<?php
   /*
   Plugin Name: WooCommerce House Sign Designer
   Plugin URI: https://thriftysites.co.uk
   Description: A plugin used for customizing house signs in WooCommerce
   Version: 2.5
   Author: ThriftySites
   Author URI: https://thriftysites.co.uk
   Requires PHP : 7.4
   Requires at least: 6.1
   WC requires at least: 7.4
   WC tested up to: 7.5
   */


if (!defined('ABSPATH')) { die(); }

/* Add querystring support
**************************/
function wwp_custom_query_vars_filter($vars) {

   $vars[] .= 'hsd_line1';    
   $vars[] .= 'hsd_line2';
   $vars[] .= 'hsd_feat_img';   
   $vars[] .= 'hsd_font_name';
   $vars[] .= 'hsd_font_color';
   $vars[] .= 'hsd_engraved';   
   $vars[] .= 'hsd_x1';
   $vars[] .= 'hsd_x2';
   $vars[] .= 'hsd_y1';
   $vars[] .= 'hsd_y2';   
   $vars[] .= 'hsd_output';    
   $vars[] .= 'random';
   $vars[] .= 'hsd_action';
   $vars[] .= 'hsd_line1Ratio';
    
   return $vars;
}

add_filter( 'query_vars', 'wwp_custom_query_vars_filter' );

/* Load application
*******************/
require 'main.php';

$App = new UB_WCHsd_App();



/* Read settings
****************/
$iniFilePath = $App->RootPath.'settings.ini';
$iniArray = parse_ini_file($iniFilePath, true);

$App->Defined_Fonts = $iniArray["fonts"];
$App->Defined_Sizes = $iniArray["sizes"];
$App->Defined_EngravingOptions = $iniArray["finishing_types"];




/* Get post id
 * it's a single product page
*****************************/
add_action('wp', 'run_plugin');

function run_plugin() {

   global $App;

   $postID = get_the_ID();
   $App->CurrentPostID = $postID;
  

   $App->Run();


}




/* Load admin class
*********************/
if (is_admin()) {
	
	require $App->AdminRootPath.'admin.class.php';
	$AdminApp = new UB_WCHsd_Admin($App);

   $App->AdminApp = $AdminApp;
	
}






