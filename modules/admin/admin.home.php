<?php


#region ROW, CONTAINER HTML TAGS

$bootstrap_container_start = '<div class="container-fluid">';
$bootstrap_container_end = '</div>';

$bootstrap_row_start = '<div class="row">';
$bootstrap_row_end = '</div>';

$bootstrap_col_start_first = '<div class="col firstCol">';
$bootstrap_col_start_first_8col = '<div class="col-8 firstCol">';
$bootstrap_col_start_last = '<div class="col lastCol">';
$bootstrap_col_start_single = '<div class="col singleCol">';
$bootstrap_col_end = '</div>';

#endregion



$WC_Installed = $this->app->WC_Installed;
$WC_Activated = $this->app->WC_Activated;

$gd_enabled = $this->app->isGDEnabled();


#region CHECK IF WC IS INSTALLED AND ACTIVATED 

if ($WC_Installed == false || $WC_Activated == false || $gd_enabled == false) {

	echo '<div class="staticErrorBox">';

	echo '<h5 class="thin-header"><i class="icon-exclamation-sign"></i>&nbsp;Problem(s) detected</h5>';

	echo '<p class="single-line">';
	
	if ($WC_Installed == false) echo '- WooCommerce is necessary for this plugin to run. Please install it and try again.<br/>';
	if ($WC_Installed != false && $WC_Activated == false) echo '- WooCommerce is not activated yet. Please activate it in Plugins section.<br/>';
	if ($gd_enabled == false) echo '- PHP GD extension that is necessary for image manipulation is not installed. Please consult your hosting provider.';

	echo '</p>';

	echo '</div>';

}

if ($WC_Installed === false || $WC_Activated === false || $gd_enabled === false) return false;





#endregion


/* WC installed + activated. GD enabled. Go on.
***********************************************/

$this->IncludeNecessaryWCRepository();


#region CHECK IF WC HAS ANY VARIABLE PRODUCTS

$wcProuductsCount = $this->app->GetNumberOfWCVariableProducts();

$noWCProductsFound = $wcProuductsCount == 0;


if ($noWCProductsFound) {

	echo '<div class="staticErrorBox">';
	echo '<h5 class="thin-header"><i class="icon-exclamation-sign"></i>&nbsp;No variable product found</h5>';
	echo '<p>At least one variable product in WooCommerce is required for this plugin. Either this plugin can create one for you or you can go to Products section of WooCommerce and create it yourself.<br/><br/>';

	echo '<p class="hsd_errorbox_form">Enter product title&nbsp;&nbsp;&nbsp;<input type="text" id="hsd_new_product_title" name="hsd_new_product_title" size="40">&nbsp;&nbsp;&nbsp;';

	echo '<input type="button" id="ubwchsd-create-new-wc-product" class="button-primary ubwchsd-btn-blue btn-create-new-wc-product" value="Create new variable product in WC">';
	
	echo '<div class="ubwchsd-spinner spinner-create-product-in-WC"></div>';

	echo '</div>';

	return false;
}

#endregion


/* Beginning of wrapper
*************************/
echo $bootstrap_container_start;


#region PLUGIN ENABLED/DISABLED SWITCH

$plugin_enabled = $this->app->IsPluginEnabled();
$plugin_status = $plugin_enabled ? 'checked' : '';


echo $bootstrap_row_start;

echo $bootstrap_col_start_single;

		echo '<div id="ubwchsd-div-plugin-status" class="ubwchsd-box-inner">';

		echo 'Plugin status&nbsp;&nbsp;<input type="checkbox" '.$plugin_status.' id="cbPluginStatus" class="toggle-switch-plugin-status" data-size="sm">';

		echo '<div class="ubwchsd-spinner spinner-pluginstatus"></div>';

		echo '</div>';


echo $bootstrap_col_end;

echo $bootstrap_row_end;

#endregion


/* Show selected product
************************/
$this->ImportAttributesFromWC(); // Refresh attributes table

$totalProducts = $this->app->TotalProductsInPluginTable();

$savedProduct = $this->app->GetSelectedProduct();


$savedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];
$savedProductName = $savedProduct == '' ? '' : explode('|', $savedProduct)[1];


echo '<input type="hidden" id="ubwchsd_saved_product_id" name="ubwchsd_saved_product_id" value="'.$savedProductID.'">';
echo '<input type="hidden" id="saved_product_name" name="saved_product_name" value="'.$savedProductName.'">';


$allProducts = $this->app->GetAllProducts();

$product_options='';

$noProductSelected = true;

foreach($allProducts as $fetchedProduct) {
	
	$product_options .= '<option value="'.$fetchedProduct->product_id.'">'.$fetchedProduct->product_name.'</option>';

}

#region PRODUCT SELECTION

echo $bootstrap_row_start;

echo $bootstrap_col_start_single;

		echo '<div id="ubwchsd-product-selection" class="ubwchsd-box-inner box-smoke-color">';

		echo '<h5 class="thin-header"><i class="icon-align-right"></i>&nbsp;Selected product';
		if ($savedProduct != '') echo ' : <span class="selected-product-title">['.$savedProductName.']</span>&nbsp;<input type="button" id="showProductSelectionTable" class="button-primary ubwchsd-btn-blue btn-showProductSelectionTable" value="Change selection">';

		//echo '<a href="#" id="link_delete_options">&nbsp;[Delete options]</a>';

		echo '</h5>';

		
		echo '<table style="width : 100%">';
		
			echo '<tr>';
			echo '<td class="ubwchsd_variations_td2">';
			echo 'Choose product to style from the list';
			echo '</td>';
			
	
			echo '<td class="ubwchsd_variations_td3">';
			
			echo '<div style="position : relative; float : left;">';			
			echo '<select name="sb_all_products" id="sb_all_products">'.$product_options.'</select>&nbsp;';
			echo '</div>';
			
			echo '<div style="position : relative; float : left;">';
			echo '<input type="button" id="ubwchsd-save_selected_product" class="button-primary ubwchsd-btn-blue btn-save-selected-product" value="Set">';
            echo '&nbsp;<input type="button" id="btnCancelProductSelection" class="button-primary ubwchsd-btn-grey btn-cancelProductSelection" value="Cancel">';
			echo '<div class="ubwchsd-spinner spinner-selected-product-save moveSpinnerToLeft"></div>';
			echo '</div>';			
			
			echo '</td>';
			
			echo '</tr>';
			
		echo '</table>';



		echo '</div>';


echo $bootstrap_col_end;

echo $bootstrap_row_end;

#endregion


if ($savedProduct == '') return false;

#region CHECK IF ALL 3 ATTRIBUTES EXIST

$totalAttributes = $this->app->TotalAttributesInPluginTable($savedProductID);

if ($totalAttributes < 2) {

	echo '<div class="notice notice-error inline">';

	echo '<p>Selected product does not have necessary attributes. Please add following attributes to this product in WooCommerce and try again:<br/><br/>- Select size<br/>- Engraving options<br/>NOTE : Do NOT forget to check <strong>Used for variations</strong> checkbox for each attribute.</p>';


	echo '</div>';	

	return false;
}

#endregion


global $wpdb;

$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;

$records = $wpdb->get_results("SELECT * FROM $attributes_table WHERE product_id=$savedProductID ORDER BY attribute_name");


$savedSizeAttr = $this->app->GetSizeAttributeField();
$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();



/* Generate options for select fields
*************************************/
$size_attr_values = '<option value="none">---</option>';
$finishing_types_attr_values = '<option value="none">---</option>';


foreach($records as $record) {
	
	$attribute_name = $record->attribute_name;
	$attribute_slug = $record->attribute_slug;
	
	$isSizeSelected = $attribute_slug == $savedSizeAttr ? "selected " : "";
	$isFinishingTypeSelected = $attribute_slug == $savedFinishingTypeAttr ? "selected " : "";
	
	$size_attr_values .= '<option '.$isSizeSelected.'value="'.$attribute_slug.'">'.$attribute_name.'</option>';
	$finishing_types_attr_values .= '<option '.$isFinishingTypeSelected.'value="'.$attribute_slug.'">'.$attribute_name.'</option>';
}

$sizeFieldFound = strpos($size_attr_values, "selected") !== false;
$finishingTypeFieldFound = strpos($finishing_types_attr_values, "selected") !== false;

$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;

#region CHECK VARIATION PARAMETERS

$variations_table = $wpdb->prefix.$this->app->DB_Table_VariationImages;

/* Get no of size values
*********************/
$arr_var_img = [];

$query_prod_sizes = "SELECT DISTINCT(value_text) AS ProdSize FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedSizeAttr')";
$result_prod_sizes = $wpdb->get_results($query_prod_sizes);

foreach ($result_prod_sizes as $result_prod_size) {

    $size_txt = $result_prod_size->ProdSize;

    if (!isset($arr_var_img[$size_txt])) {

        $arr_var_img[$size_txt]["Size"] = $size_txt;
        $arr_var_img[$size_txt]["AssignedImages"] = '';
        $arr_var_img[$size_txt]["SetImages"] = '';
        $arr_var_img[$size_txt]["VariationIDs"] = '';
    }

}



$query_variation_images = "SELECT * FROM `$variations_table` WHERE `ProductID`=$savedProductID";
$result_var_images = $wpdb->get_results($query_variation_images);


foreach($result_var_images as $result_img) {


    $result_size = $result_img->SelectSizeValue;

    $arr_var_img[$result_size]["AssignedImages"] .= $arr_var_img[$result_size]["AssignedImages"] == '' ? $result_img->ImageSrc : '|'.$result_img->ImageSrc;

	if (isset($arr_var_img[$result_size]["AssignedFullSizeImages"])) 
	{

		$arr_var_img[$result_size]["AssignedFullSizeImages"] .= $arr_var_img[$result_size]["AssignedFullSizeImages"] == '' ? $result_img->ImageFullSizeSrc : '|'.$result_img->ImageFullSizeSrc;

	}
	else 
	{
		$arr_var_img[$result_size]["AssignedFullSizeImages"] = $result_img->ImageFullSizeSrc;
	}

    $var_id = $result_img->VariationID;
    $arr_var_img[$result_size]["VariationIDs"] .= $arr_var_img[$result_size]["VariationIDs"] == '' ? $var_id : '|'.$var_id;


    $set_image = $result_img->SavedImageSrc;

    if ($set_image != '') {

        $arr_var_img[$result_size]["SetImages"] .= $arr_var_img[$result_size]["SetImages"] == '' ? $set_image : '|'.$set_image;

    }

    $arr_var_img[$result_size]["PosX"] = $result_img->PosX;
    $arr_var_img[$result_size]["PosY"] = $result_img->PosY;
    $arr_var_img[$result_size]["AreaW"] = $result_img->AreaW;
    $arr_var_img[$result_size]["AreaH"] = $result_img->AreaH;
    $arr_var_img[$result_size]["ImageID"] = $result_img->ImageID;

}

$vars_error_message = '';
$bool_critical_error = false;

$selectbox_options = ''; /* Options for selectbox in Sign design tab */


foreach ($arr_var_img as $var_img)
{

    $var_size = $var_img["Size"];
    $var_ids = $var_img["VariationIDs"];
    $var_assignedImgs = $var_img["AssignedImages"];
    $var_assignedFullSizeImgs = $var_img["AssignedFullSizeImages"];
    $var_setImgs = $var_img["SetImages"];


    /* HTML code
    ************/
    $addedOptions = '';
    $variationIDs = str_replace('|', '_', $var_ids);
    $elementID = "p".$savedProductID."v".$variationIDs;


    if (stripos($addedOptions, $elementID) === false) {

        $addedOptions .= $elementID.'-';
        $selectbox_options .= '<option value="'.$elementID.'">'.$var_size.'</option>';
    }

    //if (stripos($selectbox_options, $elementID) === false) $selectbox_options .= '<option value="'.$elementID.'">'.$var_size.'</option>';


    $img_src = explode('|', $var_assignedImgs)[0];
    $img_fullsize_src = explode('|', $var_assignedFullSizeImgs)[0];

    echo '<input type="hidden" id="'.$elementID.'" data-img="'.$img_src.'" data-img-fullsize="'.$img_fullsize_src.'"  data-posx="'.$var_img["PosX"].'" data-posy="'.$var_img["PosY"].'" data-areaw="'.$var_img["AreaW"].'" data-areah="'.$var_img["AreaH"].'" data-var-ids="'.$var_ids.'">';


    /* Check variation IDs
    ************************/
    $bool_isVarIDs_OK = false;

    if ($var_ids == '')
    {
        $vars_error_message .= '- You need to create 2 variations with size <strong>'.$var_size.'</strong> or delete this size from the "Select size" attribute.';
    }
    else
    {
        $arr_var_ids = explode('|', $var_ids);

        if (count($arr_var_ids) == 2) {

            $bool_isVarIDs_OK = true;

        }
        else {

            $vars_error_message .= '- You need to create exactly 2 variations with size <strong>'.$var_size.'</strong> or delete this size from the "Select size" attribute.';
        }

    }

    if ($vars_error_message != '') $vars_error_message .= '<br/>';


    if ($bool_isVarIDs_OK === false) {

        $bool_critical_error = true;

    }
    else {


        /* Variation IDs are OK. Check assigned images
        **********************************************/
        $var_assigned_image = '';
        $bool_isAssignedImages_OK = false;

        if ($var_assignedImgs == '')
        {
            $vars_error_message .= '- You need to assign the same image to variations with size <strong>'.$var_size.'</strong>';
        }
        else
        {
            $arr_var_assigned_imgs = explode('|', $var_assignedImgs);

            if (count($arr_var_assigned_imgs) != 2)
            {
                $vars_error_message .= '- You need to assign exactly the same image to 2 variations with size <strong>'.$var_size.'</strong>';
            }
            else
            {
                if ($arr_var_assigned_imgs[0] != $arr_var_assigned_imgs[1])
                {
                    $vars_error_message .= '- You need to assign exactly the same image to 2 variations with size <strong>'.$var_size.'</strong>';
                }
                else
                {
                    $bool_isAssignedImages_OK = true;
                    $var_assigned_image = $arr_var_assigned_imgs[0];
                }
            }

        }

        if ($vars_error_message != '') $vars_error_message .= '<br/>';

        if ($bool_isAssignedImages_OK === false)
        {
            $bool_critical_error = true;
        }
        else
        {

            /* Assigned images are OK. Check set images
            *******************************************/
            if ($var_setImgs == '')
            {
                $vars_error_message .= '- You have not set the area for image with size <strong>'.$var_size.'</strong> yet. Please click Sign Design tab and set it.';
            }
            else
            {
                $arr_setImgs = explode('|', $var_setImgs);

                if (count($arr_setImgs) == 0)
                {
                    if ($var_setImgs != $var_assigned_image) {

                        $vars_error_message .= '- There is a problem with assigned image with size <strong>'.$var_size.'</strong>. Please click Sign Design tab and check it.';

                    }
                }
                else
                {
                    if ($arr_setImgs[0] != $var_assigned_image) {

                        $vars_error_message .= '- There is a problem with assigned image with size <strong>'.$var_size.'</strong>. Please click Sign Design tab and check it.';

                    }
                }
            }

        }

    }


}

if ($vars_error_message != '')
{

    echo '<div class="notice notice-error inline">';

    echo '<p>'.$vars_error_message.'</p>';

    echo '</div>';

    echo '<br/><br/>';

    if ($bool_critical_error !== false) return false;


}

/* Save as JSON
***************/
echo '<input type="hidden" id="ubwchsd_var_images" name="ubwchsd_var_images" value=\''.json_encode($arr_var_img, JSON_FORCE_OBJECT).'\'>';


#endregion

/* Prepare layout
*************************/
echo $bootstrap_row_start;

echo $bootstrap_col_start_single;

/* Display error message
************************/
echo '<div class="errorBox">ERROR MESSAGE</div>';

$post_thumbnail_id = get_post_thumbnail_id($savedProductID);

if ($post_thumbnail_id == 0) {

	echo '<div class="staticErrorBox">';
	echo '<h5 class="thin-header"><i class="icon-exclamation-sign"></i>&nbsp;Product image not set</h5>';
	echo '<p class="single-line">Please go to selected product\'s page and set product image.</p>';

	echo '</div>';	

}


startTabContainer();

addTabHeader(1, 'Product attributes', false, false);
addTabHeader(2, 'Size prices', false, !$sizeFieldFound);
addTabHeader(3, 'Engraving option prices', false, !$finishingTypeFieldFound);
addTabHeader(4, 'Variations', false, !($sizeFieldFound && $finishingTypeFieldFound));
addTabHeader(5, 'Sign design', false, !($sizeFieldFound && $finishingTypeFieldFound && ($post_thumbnail_id != 0)));

startTabPagesContainer();

#region TAB 1 : PRODUCT ATTRIBUTES

addTabPage(1, true);
/********************************** PRODUCT ATTRIBUTES TAB PAGE ***************************************** */

echo '<table class="ub_wchsd_size_values">';


	/* SIZE ATTRIBUTE
	*****************/
	echo '<tr>';
	echo '<td class="ubwchsd_variations_td1">';
	echo '<div class="numberCircle">1</div>';
	echo '</td>';		
	
	
	echo '<td class="ubwchsd_variations_td2">';
	echo 'Size attribute';
	echo '</td>';
	

	echo '<td class="ubwchsd_variations_td3">';
	echo '<select name="sb_size_variation" id="sb_size_variation">'.$size_attr_values.'</select>';
	echo '</td>';
	
	echo '</tr>';
	
	
	/* FINISHING TYPE ATTRIBUTE
	***************************/
	echo '<tr>';
	echo '<td class="ubwchsd_variations_td1">';
	echo '<div class="numberCircle">2</div>';
	echo '</td>';		
	
	
	echo '<td class="ubwchsd_variations_td2">';
	echo 'Engraving options attribute';
	echo '</td>';
	

	echo '<td class="ubwchsd_variations_td3">';
	echo '<select name="sb_finishing_type" id="sb_finishing_type">'.$finishing_types_attr_values.'</select>';
	echo '</td>';
	
	echo '</tr>';			
	
	
	echo '<tr><td colspan="3" class="var_save_btn" style="border : none;">';
	echo '<div style="position : relative">';
	echo '<input type="button" id="ubwchsd-save_variations" class="button-primary ubwchsd-btn-blue btn-variation-props-save" value="Save">';
	echo '<div class="ubwchsd-spinner spinner-variation-props-save moveSpinnerToLeft"></div>';
	echo '</div>';
	echo '</td></tr>';


echo '</table>';

/********************************** END OF PRODUCT ATTRIBUTES TAB PAGE ***************************************** */
endTabPage();

#endregion

#region TAB 2 : SIZE ATTRIBUTE VALUES

addTabPage(2, true);
/********************************** SIZE ATTRIBUTE VALUES TAB PAGE ***************************************** */

$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedSizeAttr')";
$size_values = $wpdb->get_results($query1);


		
		echo '<table class="ub_wchsd_size_values">';
		echo '<tr><th>#</th><th>Attribute value</th><th>Regular price</th><th>Sale price</th></tr>';
		//echo '<tr><td colspan="4" style="border: none;"></td>';

			$ord=1;
			foreach($size_values as $size_value) {

				$size_value_regular_price = $size_value->regular_price;
				if (intval($size_value_regular_price) == 0) $size_value_regular_price = '';

				$size_value_sale_price = $size_value->sale_price;
				if (intval($size_value_sale_price) == 0) $size_value_sale_price = '';

				echo '<tr>';
				
				echo '<td class="ubwchsd_variations_td1">';
				echo '<div class="numberCircle">'.$ord.'</div>';
				echo '</td>';		
				
				
				echo '<td>';
				echo '<strong>'.$size_value->value_text.'</strong>';
				echo '</td>';
				
		
				echo '<td class="ubwchsd_variations_td3">';
				echo '<input type="number" class="size_attribute_values" value="'.$size_value_regular_price.'" id="size_value_'.$ord.'_regular_price" data-value-slug="'.$size_value->value_slug.'" data-value-attr-record-id="'.$size_value->attr_record_id.'" data-value-price-type="regular" maxlength="4" size="4">';
				echo '</td>';
				
				
				echo '<td class="ubwchsd_variations_td3">';
				echo '<input type="number" class="size_attribute_values" value="'.$size_value_sale_price.'" id="size_value_'.$ord.'_sale_price" data-value-slug="'.$size_value->value_slug.'" data-value-attr-record-id="'.$size_value->attr_record_id.'" data-value-price-type="sale" maxlength="4" size="4">';
				echo '</td>';
				
				echo '</tr>';
			

				$ord++;
			}
			
			
			echo '<tr><td colspan="4" class="var_save_btn">';
			echo '<div style="position : relative">';
			echo '<input type="button" id="ubwchsd-save_attribute_values" class="button-primary ubwchsd-btn-blue btn-save-size-attribute-values" value="Save">';
			echo '<div class="ubwchsd-spinner spinner-save-size-attribute-values moveSpinnerToLeft"></div>';
			echo '</div>';
			echo '</td></tr>';
		
		
		echo '</table>';
		


/********************************** END OF SIZE ATTRIBUTE VALUES TAB PAGE ***************************************** */
endTabPage();

#endregion

#region TAB 3 : FINISHING TYPE ATTRIBUTE VALUES

addTabPage(3, true);
/********************************** FINISHING TYPE ATTRIBUTE VALUES TAB PAGE ***************************************** */

$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;

$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedFinishingTypeAttr')";
$finishing_type_values = $wpdb->get_results($query1);

	
		
		echo '<table class="ub_wchsd_size_values">';
		echo '<tr><th>#</th><th>Attribute value</th><th>Regular price</th><th>Sale price</th></tr>';
		//echo '<tr><td colspan="4" style="border: none;"></td></tr>';

			$ord=1;
			foreach($finishing_type_values as $finishing_type_value) {
				
				$finishing_type_regular_price = $finishing_type_value->regular_price;
				if (intval($finishing_type_regular_price) == 0) $finishing_type_regular_price = '';

				$finishing_type_sale_price = $finishing_type_value->sale_price;
				if (intval($finishing_type_sale_price) == 0) $finishing_type_sale_price = '';

				echo '<tr>';
				
				echo '<td class="ubwchsd_variations_td1">';
				echo '<div class="numberCircle">'.$ord.'</div>';
				echo '</td>';		
				
				
				echo '<td>';
				echo '<strong>'.$finishing_type_value->value_text.'</strong>';
				echo '</td>';
				
		
				echo '<td class="ubwchsd_variations_td3">';
				echo '<input type="number" class="finishing_type_attribute_values" value="'.$finishing_type_regular_price.'" id="finishing_type_value_'.$ord.'_regular_price" data-value-slug="'.$finishing_type_value->value_slug.'" data-value-attr-record-id="'.$finishing_type_value->attr_record_id.'" data-value-price-type="regular" maxlength="4" size="5">';
				echo '</td>';
				
				
				echo '<td class="ubwchsd_variations_td3">';
				echo '<input type="number" class="finishing_type_attribute_values" value="'.$finishing_type_sale_price.'" id="finishing_type_value_'.$ord.'_sale_price" data-value-slug="'.$finishing_type_value->value_slug.'" data-value-attr-record-id="'.$finishing_type_value->attr_record_id.'" data-value-price-type="sale" maxlength="4" size="5">';
				echo '</td>';

				
				echo '</tr>';
			
				$ord++;
			}
			
			
			
			echo '<tr><td colspan="4" class="var_save_btn" style="border: none;">';
			echo '<div style="position : relative">';
			echo '<input type="button" id="ubwchsd-save_finishing_type_values" class="button-primary ubwchsd-btn-blue btn-save_finishing_type_values" value="Save">';
			echo '<div class="ubwchsd-spinner spinner-save_finishing_type_values moveSpinnerToLeft"></div>';
			echo '</div>';
			echo '</td></tr>';
		
		
		echo '</table>';
		


/********************************** END OF FINISHING TYPE ATTRIBUTE VALUES TAB PAGE ***************************************** */
endTabPage();

#endregion

#region TAB 4 : VARIATIONS

addTabPage(4, true);
/********************************** VARIATIONS TAB PAGE ***************************************** */
$totalVariations = $this->NoOfVariations();


if ($totalVariations == 0) {
	echo '<div style="position : relative; margin-bottom : 20px;">';
	echo '<input type="button" id="ubwchsd-create-new-variations" class="button-primary ubwchsd-btn-green" value="Create variations">&nbsp;';
	//if ($totalVariations > 0) echo '<input type="button" id="ubwchsd-remove-all-variations-and-create-new" class="button-primary ubwchsd-btn-red" value="Remove all variations and create again">&nbsp;';
	//if ($totalVariations > 0) echo '<div class="ubwchsd-spinner spinner-remove-and-create-variations"></div>';
	echo '<div class="ubwchsd-spinner spinner-create-all-variations"></div>';

	echo '</div>';

}

echo '<h5 class="thin-header">Current variations</h5>';

echo '<div id="variations_list">';
echo $this->ListAllVariations();
echo '</div>';

/********************************** END OF VARIATIONS TAB PAGE ***************************************** */
endTabPage();

#endregion

#region TAB 5 : SIGN DESIGN

addTabPage(5, true);
/********************************** SIGN DESIGN TAB PAGE ***************************************** */
echo '<div style="position : relative;">';


$font_options = '';
$allFonts = '';
$PluginFonts = $this->app->FetchAllFonts();

foreach($PluginFonts as $font) {

	$font_options .= '<option value="'.$font->font_file_name.'|'.$font->font_group.'">'.$font->font_name.'</option>';

	$allFonts .= $font->font_name.'|';

}

$allFonts = substr($allFonts, 0,  strlen($allFonts)-1);



/* Get selected region parameters
*********************************/
$regionParams = $this->app->GetRegionParameters();

echo '<input type="hidden" id="ubwchsd_selected_region_x" name="ubwchsd_selected_region_x" value="'.$regionParams[0].'">';
echo '<input type="hidden" id="ubwchsd_selected_region_y" name="ubwchsd_selected_region_y" value="'.$regionParams[1].'">';
echo '<input type="hidden" id="ubwchsd_selected_region_w" name="ubwchsd_selected_region_w" value="'.$regionParams[2].'">';
echo '<input type="hidden" id="ubwchsd_selected_region_h" name="ubwchsd_selected_region_h" value="'.$regionParams[3].'">';

$regionSetStatus = $regionParams[0] == '' ? '<span id="region_set_status" color="red">Region not set</span>' : '<span id="region_set_status" color="green">Region set</span>';

if ($savedProductID != '') {

    echo '<div class="ubwchsd_sdesign_img_chooser_wrapper">';
    echo 'Choose a size from the list&nbsp;&nbsp;<select id="cb_select_size" name="cb_select_size">';

    echo '<option value="">Select a size</option>';

    echo $selectbox_options;

    echo '</select>';
    echo '&nbsp;&nbsp;<input type="button" id="ubwchsd-choose-size" class="button-primary ubwchsd-btn-blue" value="Go">';
    echo '</div>';




		$imageSizes = ["large", "woocommerce_single"];

		$img_info_large = wp_get_attachment_image_src($post_thumbnail_id, "large");
		$img_info_thumbnail = wp_get_attachment_image_src($post_thumbnail_id, "woocommerce_single");


		echo '<div class="variation_image_properties_container">';
		
			echo '<div class="variation_image_panel">';

			echo '<input type="hidden" id="ubwchsd_url_variation_img" name="ubwchsd_url_variation_img" value="'.$img_info_thumbnail[0].'">';
			echo '<input type="hidden" id="ubwchsd_img_editor_url" name="ubwchsd_img_editor_url" value="'.$this->app->Img_Editor.'">';
			echo '<img id="img_large_variation" src="'.$img_info_thumbnail[0].'" data-original-image="'.$img_info_thumbnail[0].'"/><br/>';

			echo '<table>
				<tr>
					<td>Left top X&nbsp;&nbsp;<input type="text" id="selected_area_x" value="" size = "4" readonly></td>
					<td>Left top Y&nbsp;&nbsp;<input type="text" id="selected_area_y" value="" size = "4" readonly></td>
					<td>Width&nbsp;&nbsp;<input type="text" id="selected_area_width" value="" size = "4" readonly></td>
					<td>Height&nbsp;&nbsp;<input type="text" id="selected_area_height" value="" size = "4" readonly></td>
				</tr>
			</table>';

			echo '</div>';		

			echo '<div class="featured_image_properties">';
			
			/* Set region
			******************************************************************/
			echo '<p class="extra-info">You need to set the region on the plaque which will be used for house number and address. Use your mouse to move and resize the region.<br/><br/>Current status : <strong>'.$regionSetStatus.'</strong></p>';

			echo '<br/><input type="button" id="ubwchsd-test-plaque" class="button-primary ubwchsd-btn-blue" value="Start testing settings for previewing">';
			echo '&nbsp;&nbsp;<input type="button" id="ubwchsd-save-parameters" class="button-primary ubwchsd-btn-green" value="Save selected settings">';
			echo '<div class="ubwchsd-spinner spinner-save_region_values"></div>';
			echo '<br/><br/>';
			echo '<table class="tbl-region-settings">';

			echo '<tr>';

			echo '<td>Line 1</br><input type="text" id="test-line1" name="test-line1" size="18" maxlength="16" value="10"></td>';
			echo '<td>Line 2</br><input type="text" id="test-line2" name="test-line2" size="18" maxlength="16" value="Downing Street">';
			echo '<td>Text style</br><select id="test-fonts" name="test-fonts">'.$font_options.'</select></td>';

			echo '<td>Engraving option</br><select id="test-finishing" name="test-fonts">
				<option value="uv" selected>Painted in white</option>
				<option value="unpainted_engraved">Unpainted engraved</option>
				</select></td>';

			echo '</tr>';

			echo '<tr><td class="4" id="td-color-palette"></td></tr>';

			echo '</table>';

			echo '</div>';	

			echo '<div class="clearBoth">&nbsp;</div>';

		echo '</div>';



}

echo '</div>';


/********************************** END OF FEATURED IMAGE TAB PAGE ***************************************** */
endTabPage();

#endregion

endTabPagesContainer();


endTabContainer();

echo $bootstrap_col_end;

echo $bootstrap_row_end;

echo $bootstrap_container_end;

#region TAB PAGES HTML CODES

function startTabContainer() {
	
	echo '<div class="ub_tabs_wrapper">';
}

function endTabContainer() {

	echo '</div>';

}


function addTabHeader($tabNo, $title, $isSelected = false, $isDisabled = false) {

	echo sprintf('<div class="ub_tab_header%s%s" data-tab="%d">%s</div>',
				  $isSelected === false ? '' : ' selected',
				  $isDisabled === false ? '' : ' disabledTab',
				  $tabNo,
				  $title);

}

function startTabPagesContainer() {

	echo '<div class="ub_tab_page_container clearBoth">';

}

function endTabPagesContainer() {

	echo '</div>';

}

function addTabPage($tabNo, $isHidden) {

	echo sprintf('<div class="ub_tab_page%s" data-tab="%d">',
				  $isHidden === false ? '' : ' hiddenTabPage',
				  $tabNo);

}

function endTabPage() {

	echo '</div>';

}

#endregion
