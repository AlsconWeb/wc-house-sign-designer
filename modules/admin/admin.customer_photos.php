<?php

global $wpdb;

#region ROW, CONTAINER HTML TAGS

$bootstrap_container_start = '<div class="container-fluid">';
$bootstrap_container_end = '</div>';

$bootstrap_row_start = '<div class="row">';
$bootstrap_row_end = '</div>';

$bootstrap_col_start_first = '<div class="col firstCol">';
$bootstrap_col_start_first_8col = '<div class="col-8 firstCol">';
$bootstrap_col_start_last = '<div class="col lastCol">';
$bootstrap_col_start_single = '<div class="col singleCol">';
$bootstrap_col_end = '</div>';

#endregion


$WC_Installed = $this->app->WC_Installed;
$WC_Activated = $this->app->WC_Activated;

$gd_enabled = $this->app->isGDEnabled();


#region CHECK IF WC IS INSTALLED AND ACTIVATED 

if ($WC_Installed == false || $WC_Activated == false || $gd_enabled == false) {

	echo '<div class="staticErrorBox">';

	echo '<h5 class="thin-header"><i class="icon-exclamation-sign"></i>&nbsp;Problem(s) detected</h5>';

	echo '<p class="single-line">';
	
	if ($WC_Installed == false) echo '- WooCommerce is necessary for this plugin to run. Please install it and try again.<br/>';
	if ($WC_Installed != false && $WC_Activated == false) echo '- WooCommerce is not activated yet. Please activate it in Plugins section.<br/>';
	if ($gd_enabled == false) echo '- PHP GD extension that is necessary for image manipulation is not installed. Please consult your hosting provider.';

	echo '</p>';

	echo '</div>';

}

if ($WC_Installed === false || $WC_Activated === false || $gd_enabled === false) return false;





#endregion


/* WC installed + activated. GD enabled. Go on.
***********************************************/

$this->IncludeNecessaryWCRepository();


#region CHECK IF WC HAS ANY VARIABLE PRODUCTS

$wcProuductsCount = $this->app->GetNumberOfWCVariableProducts();

$noWCProductsFound = $wcProuductsCount == 0;


if ($noWCProductsFound) {

	echo '<div class="staticErrorBox">';
	echo '<h5 class="thin-header"><i class="icon-exclamation-sign"></i>&nbsp;No variable product found</h5>';
	echo '<p>At least one variable product in WooCommerce is required for this plugin. Either this plugin can create one for you or you can go to Products section of WooCommerce and create it yourself.<br/><br/>';

	echo '<p class="hsd_errorbox_form">Enter product title&nbsp;&nbsp;&nbsp;<input type="text" id="hsd_new_product_title" name="hsd_new_product_title" size="40">&nbsp;&nbsp;&nbsp;';

	echo '<input type="button" id="ubwchsd-create-new-wc-product" class="button-primary ubwchsd-btn-blue btn-create-new-wc-product" value="Create new variable product in WC">';
	
	echo '<div class="ubwchsd-spinner spinner-create-product-in-WC"></div>';

	echo '</div>';

	return false;
}

#endregion


/* Beginning of wrapper
*************************/
echo $bootstrap_container_start;



$this->CreateCustomerPhotosDB();

echo '<input type="button" id="ubwchsd-add-new-photo" class="button-primary ubwchsd-btn-blue btn-add-new-photo" value="Add new photo from Media Library">';
echo '<input type="hidden" id="ubwchsd-new-image-ids" value="">';
echo '<input type="hidden" id="ubwchsd-image-ids" value="">';
echo '<br/><br/><br/>';


/* Get variation prices
***********************/
echo $this->GetVariationPrices();


/* Get saved product
********************/
$savedProduct = $this->app->GetSelectedProduct();

$savedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];
$savedProductName = $savedProduct == '' ? '' : explode('|', $savedProduct)[1];

echo '<input type="hidden" id="ubwchsd_saved_product_id" name="ubwchsd_saved_product_id" value="'.$savedProductID.'">';
echo '<input type="hidden" id="saved_product_name" name="saved_product_name" value="'.$savedProductName.'">';

$variations_table = $wpdb->prefix.$this->app->DB_Table_VariationImages;
$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;


/* Get size values
*********************/
$arr_sizes = [];
$size_options = '';

$savedSizeAttr = $this->app->GetSizeAttributeField();

$query_prod_sizes = "SELECT DISTINCT(value_text) AS ProdSize FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedSizeAttr') ORDER BY `value_text`";


$result_prod_sizes = $wpdb->get_results($query_prod_sizes);



foreach ($result_prod_sizes as $result_prod_size) {

    $arr_sizes[] = $result_prod_size->ProdSize;

    $size_class_name = strtolower($result_prod_size->ProdSize);
    $size_class_name = str_replace("cm", "", $size_class_name);
    $size_class_name = str_replace(" ", "", $size_class_name);

    $size_options .= '<option value="'.$size_class_name.'">'.$result_prod_size->ProdSize.'</option>';

}



/* Get finishing types
**********************/
$arr_finishing_types = [];
$finishingType_options = '';

$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();

$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedFinishingTypeAttr')";
$finishing_type_values = $wpdb->get_results($query1);

foreach($finishing_type_values as $finishing_type) {

    $arr_finishing_types[] = $finishing_type->value_text;
    $finishingType_options .= '<option value="'.$finishing_type->value_text.'">'.$finishing_type->value_text.'</option>';

}


/* Get fonts
************/
$arr_fonts = [];
$font_options = '';

$PluginFonts = $this->app->FetchAllFonts();

foreach($PluginFonts as $pluginFont) {

    $arr_fonts[] = $pluginFont->font_name;
    $font_options .= '<option value="'.$pluginFont->font_name.'">'.$pluginFont->font_name.'</option>';
}


/* Actions panel
*****************/
echo '<div class="ubwchsd-actions-wrapper">';

echo '<div class="ubwchsd-actions-panel ubwchsd-panel-yellow">';
echo 'Please wait...';
echo '</div>';

echo '<div class="ubwchsd-actions-panel ubwchsd-panel-ivory">';
echo '<strong>Slate size</strong>&nbsp;&nbsp;<select id="ubwchsd_size">'.$size_options.'</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<strong>Font name</strong>&nbsp;&nbsp;<select id="ubwchsd_fonts">'.$font_options.'</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<strong>Finishing type</strong>&nbsp;&nbsp;<select id="ubwchsd_engraving">'.$finishingType_options.'</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<input type="button" id="ubwchsd-set-props" class="button-primary ubwchsd-btn-blue btn-set-props" value="Save">';
echo '</div>';

echo '<div class="ubwchsd-actions-panel ubwchsd-panel-white">';
echo '<input type="button" id="ubwchsd-unselect-all" class="button-primary ubwchsd-btn-grey btn-unselect-all" value="Deselect all">';
echo '</div>';

echo '<div class="ubwchsd-actions-panel ubwchsd-panel-white">';
echo '<input type="button" id="ubwchsd-delete-selected" class="button-primary ubwchsd-btn-red btn-delete-selected" value="Delete selected photos">';
echo '</div>';

echo '<div class="ubwchsd-actions-panel no-border">';
echo '<div class="ubwchsd-spinner spinner-customer-photos-page"></div>';
echo '</div>';

echo '</div>';


startTabContainer();


addTabHeader(100, "Unsaved photos", true);

$total_sizes = count($arr_sizes);

for ($i=0; $i < $total_sizes; $i++) {

	addTabHeader($i, $arr_sizes[$i]);
	echo '<input type="hidden" class="ubwchsd-size-info" id="ubwchsd_size_'.$i.'" data-hidden-size-value="'.$arr_sizes[$i].'" data-hidden-size-index="'.$i.'">';
}

addTabHeader(101, "Fallback image", true);



startTabPagesContainer();


/* Undefined images panel
*************************/
addTabPage(100, false);



echo '<div class="ubwchsd-size-panel ubwchsd-undefined-size">';

echo '</div>';


endTabPage();


$customer_photos_table = $wpdb->prefix.$this->app->DB_Table_CustomerPhotos;


for ($i=0; $i < $total_sizes; $i++) {

	addTabPage($i, false);
	echo '<div class="ubwchsd-size-panel">';

	$query = "SELECT * FROM `$customer_photos_table` WHERE `size_in_attribute`='".$arr_sizes[$i]."'";
	$results = $wpdb->get_results($query);
	
	$images_html = '';

	$total_pics = 0;
	$html_pic = '';

	foreach($results as $result) {


		$images_html .= '<div class="ubwchsd-image-panel" data-attachment-id="'.$result->image_attach_id.'"><div class="ubwchsd-spinner spinner-customer-photos-page"></div>';

		$img_url = wp_get_attachment_image_url($result->image_attach_id, 'large');

		$img_file_name = isset($img_url) && $img_url != '' ? basename($img_url) : '';

		$images_html .= '<img src="'.$img_url.'" data-filename="'.$img_file_name.'">';
		$images_html .= '<p>'.$result->font_name.'</p>';
		$images_html .= '<p>'.$result->engraving_option.'</p>';

		$images_html .= '<p class="img_fileName">'.$img_file_name.'</p>';

		$images_html .= '</div>';

		if ($total_pics <=5) {

			$total_pics++;

			$html_pic .= '<div class="ubhsd-pfoc-slide" data-slideid="'.$total_pics.'">';
			$html_pic .= '<img src="'.$img_url.'">';
			$html_pic .= '</div>';
		}

	}

	echo $images_html;


	wp_reset_query();

	echo '</div>';
	endTabPage();
}


/* DEFAULT IMAGE TAB
********************/
addTabPage(101, false);

$fbImage = explode('|', $this->app->GetFallbackImage())[0];

echo '<div class="ubwchsd-size-info" data-hidden-size-index="101">';

echo '<div class="ubwchsd-default-image">';
echo '<img src="'.$fbImage.'">';
echo '</div>';

echo '<input type="button" id="ubwchsd-select-fallback-image" class="button-primary ubwchsd-btn-blue btn-select-fallback-image" value="Select photo from Media Library">';

echo '</div>';

endTabPage();


/* End of tab container
***********************/
endTabPagesContainer();
endTabContainer();


/* End of wrapper
*****************/
echo $bootstrap_container_end;


#region TAB PAGES HTML CODES

function startTabContainer() {
	
	echo '<div class="ub_tabs_wrapper hidden_wrapper">';
}

function endTabContainer() {

	echo '</div>';

}


function addTabHeader($tabNo, $title, $isSelected = false, $isDisabled = false) {

	echo sprintf('<div class="ub_tab_header%s%s" data-tab="%d">%s</div>',
				  $isSelected === false ? '' : ' selected',
				  $isDisabled === false ? '' : ' disabledTab',
				  $tabNo,
				  $title);

}

function startTabPagesContainer() {

	echo '<div class="ub_tab_page_container clearBoth">';

}

function endTabPagesContainer() {

	echo '</div>';

}

function addTabPage($tabNo, $isHidden) {

	echo sprintf('<div class="ub_tab_page%s" data-tab="%d">',
				  $isHidden === false ? '' : ' hiddenTabPage',
				  $tabNo);

}

function endTabPage() {

	echo '</div>';

}

#endregion