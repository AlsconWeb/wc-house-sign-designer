<?php

/**
 * Admin page
 *
 * @version 1.0
 * @author Hakan
 */
//require 'admin.wc.php';

class UB_WCHsd_Admin //extends UB_WCHsd_WC
{
    public $app = null;
    public $FeaturedImageIDs = [];

#region INITIALIZATION


    function __construct(UB_WCHsd_App $main_app) {

        $this->app =  $main_app;


        /* Register styles and scripts
         * ***************************/
        add_action('admin_enqueue_scripts', array($this, 'registerScripts'));

        /* Register ajax calls
         * *******************/
        add_action( 'wp_ajax_setPluginStatus', array( $this, 'setPluginStatus' ) );
		add_action( 'wp_ajax_setSizeAndFinishingAttributes', array( $this, 'setSizeAndFinishingAttributes' ) );
		add_action( 'wp_ajax_setSelectedProduct', array( $this, 'setSelectedProduct' ) );
		add_action( 'wp_ajax_ResetEverything', array( $this, 'ResetEverything' ) );
		add_action( 'wp_ajax_saveSizeAttributeValues', array( $this, 'saveSizeAttributeValues' ) );
		add_action( 'wp_ajax_saveFinishingTypeValues', array( $this, 'saveFinishingTypeValues' ) );
		add_action( 'wp_ajax_deleteAllVariationsAndCreateAllVariants', array( $this, 'deleteAllVariationsAndCreateAllVariants' ) );
		add_action( 'wp_ajax_updateVariationPrices', array( $this, 'updateVariationPrices' ) );
		add_action( 'wp_ajax_setRegionParameters', array( $this, 'setRegionParameters' ) );
		add_action( 'wp_ajax_CreateProduct', array( $this, 'CreateProduct' ) );
		add_action( 'wp_ajax_GetImagePalette', array( $this, 'GetImagePalette' ) );
		add_action( 'wp_ajax_addUpdateCustomerPhoto', array($this, 'addUpdateCustomerPhoto'));
		add_action( 'wp_ajax_deleteCustomerPhoto', array($this, 'deleteCustomerPhoto'));
		add_action( 'wp_ajax_SetFallBackImage', array($this, 'SetFallBackImage'));


		$action = filter_input( INPUT_GET, "hsd_action", FILTER_SANITIZE_STRING );

		if ($action == "resetall") $this->app->ResetAll();


        /* Create admin section menu
         * *************************/
        add_action( 'admin_menu', array($this,'registerAdminSectionMenu'));



    }



    function registerAdminSectionMenu() {

        add_menu_page(
            'WooCommerce House Sign Designer',
            'WooCommerce House Sign Designer',
            'manage_options',
            'ubwchsd_main_menu',
            array($this, 'ubwchsd_main_menu_page'),
            'dashicons-admin-appearance',
            110
        );

        add_menu_page(
            'Photos from customers',
            'Photos from customers',
            'manage_options',
            'ubwchsd_customer_photos',
            array($this, 'ubwchsd_customer_photos_page'),
            'dashicons-admin-media',
            20
        );		

    }

    

    function registerScripts() {


        $current_screen = get_current_screen();

        if ( strpos($current_screen->base, 'ubwchsd') === false) {
            return;
        }
        else
        {


			$style_file_url = plugins_url('assets/admin/css/admin.style.css', $this->app->RootFile);
			$style_file_path = $this->app->RootPath.'assets/admin/css/admin.style.css';			
			$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-admin', $style_file_url, 'style', $style_file_path);

            wp_enqueue_style('open-sans', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap&display=swap');

            wp_enqueue_style('font-awesome', '//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css');
            wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');

            wp_enqueue_style('ubwchsd-bootstraptoogle', plugins_url('vendors/bootstrap-toggle/css/bootstrap4-toggle.css', $this->app->RootFile ));
            wp_register_script('ubwchsd-bootstraptoogle', plugins_url('vendors/bootstrap-toggle/js/bootstrap4-toggle.js',$this->app->RootFile ), array('jquery'), '', true);
            wp_enqueue_script('ubwchsd-bootstraptoogle');

			wp_enqueue_style('ubwchsd-croppr', plugins_url('vendors/croppr/croppr.css', $this->app->RootFile ));
            wp_register_script('ubwchsd-croppr', plugins_url('vendors/croppr/croppr.js',$this->app->RootFile ), array('jquery'), '', true);
            wp_enqueue_script('ubwchsd-croppr');
		
			if (!did_action( 'wp_enqueue_media' ) ) wp_enqueue_media(); /* Added 11.07.2023 for displaying Media Library */
			

			// Common.js
			$script_file_url = plugins_url('assets/admin/js/admin.script.js', $this->app->RootFile);
			$script_file_path = $this->app->RootPath.'assets/admin/js/admin.script.js';
			$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-admin', $script_file_url, 'script', $script_file_path, array('jquery'));
					


            /* Register ajax variables
             * ***********************/
            wp_localize_script( 'ubwchsd-admin', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), array('isEnabled' => true, 
																														  'sizeVar' => '', 
																														  'finishingVar' => '', 
																														  'finishingTypeVar' => '',
																														  'selectedProductID'=>'', 
																														  'selectedProductName'=>'',
																														  'sizeAttrValue'=>'',
																														  'finishingTypeValue'=>'',
																														  'regionX' => '',
																														  'regionY' => '',
																														  'regionW' => '',
																														  'regionH' => '',
																														  'productTitle' => '',
																														  'imageFile' => '',
																														  'noOfColors' => '',
                                                                                                                          'variationIDs' => '',
																														  'image_attach_id' => '',
																														  'size_in_attribute' => '',
																														  'size_for_class' => '',
																														  'font_name' => '',
																														  'engraving_option' => '',
																														  'variation_price' => '',
																														  'fallbackImageURL' => '',
																														  'fallbackImageW' => '',
																														  'fallbackImageH' => ''
																														)));
      						
        }


    }
	
	
	function enqueue_scripts_styles_with_filetime_as_version($my_handle, $relpath, $type, $file_full_path, $my_deps=array()) {
		
		$uri = $relpath;
		$vsn = filemtime($file_full_path);
		if($type == 'script') wp_enqueue_script($my_handle, $uri, $my_deps, $vsn);
		else if($type == 'style') wp_enqueue_style($my_handle, $uri, $my_deps, $vsn);      
		
	}	


    /* Include WooCommerce classes
     * ***************************/
    public function IncludeNecessaryWCRepository() {

        if(!interface_exists('WC_Customer_Data_Store_Interface')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/interfaces/class-wc-customer-data-store-interface.php'); }
        if(!interface_exists('WC_Object_Data_Store_Interface')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/interfaces/class-wc-object-data-store-interface.php'); }

        if(!class_exists('WC_Data_Store_WP')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/data-stores/class-wc-data-store-wp.php'); }
        if(!class_exists('WC_Customer_Data_Store')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/data-stores/class-wc-customer-data-store.php'); }


        if(!class_exists('WC_Data_Store')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/class-wc-data-store.php'); }
        if(!class_exists('WC_Object_Query')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/abstracts/abstract-wc-object-query.php'); }
        if(!class_exists('WC_Product_Variable')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/class-wc-product-query.php'); }
        if(!function_exists('wc_get_product_types')) { include(WP_PLUGIN_DIR.'/woocommerce/includes/wc-product-functions.php'); }
    }


    function ubwchsd_main_menu_page() {

        echo '<div class="ubwchsd_wrapper">';

        echo '<br/>';
        echo '<h3><strong>WooCommerce House Sign Designer</strong></h3><br/>';

        require 'admin.home.php';

        echo '</div>';

    }

	function ubwchsd_customer_photos_page() {

        echo '<div class="ubwchsd_wrapper">';

        echo '<br/>';
        echo '<h3><strong>Photos from Customers</strong></h3><br/>';

        require 'admin.customer_photos.php';

        echo '</div>';		

	}

#endregion

#region AJAX CALLS


    public function setPluginStatus() {

        //check_ajax_referer( 'setPluginStatus_nonce', 'nonce' );

        $isEnabled = strtolower($_POST['isEnabled']);

        if ($isEnabled == 'true')
            $this->app->EnablePlugin();
        else
            $this->app->DisablePlugin();

        $plugin_status = $this->app->IsPluginEnabled() ? 'enabled' : 'disabled';


        wp_send_json_success( $plugin_status );
    }
	
	
	public function setSizeAndFinishingAttributes() {
		
		//check_ajax_referer( 'setSizeAndFinishingAttributes_nonce', 'nonce' );
		
		
		$sizeVar = $_POST['sizeVar'];
		$finishingTypeVar = $_POST['finishingTypeVar'];
		
		$this->app->SetSizeAttributeField($sizeVar);
		$this->app->SetFinishingTypeAttributeField($finishingTypeVar);
		
		wp_send_json_success("ok");
		
	}
	
	public function SetSelectedProduct() {
		
		$selectedProductID = $_POST["selectedProductID"];
		$selectedProductName = $_POST["selectedProductName"];
		
		$selectedProduct = $selectedProductID.'|'.$selectedProductName;
		
		$this->app->SetSelectedProduct($selectedProduct);
		
		wp_send_json_success("ok");
	}


	
	public function saveSizeAttributeValues() {

		$jsonData = $_POST["sizeAttrValue"];

		$arrValues = json_decode(stripslashes($jsonData));

		$this->saveAttributeValuesToTable($arrValues);

		$this->UpdateVariationPrices();

		$newVariationPriceList = $this->ListAllVariations();

		wp_send_json_success($newVariationPriceList);

	}



	public function saveFinishingTypeValues() {

		$jsonData = $_POST["finishingTypeValue"];

		$arrValues = json_decode(stripslashes($jsonData));

		$this->saveAttributeValuesToTable($arrValues);

		$this->UpdateVariationPrices();

		$newVariationPriceList = $this->ListAllVariations();

		wp_send_json_success($newVariationPriceList);

	}



	public function setRegionParameters() {

        global $wpdb;

		$regionX = $_POST['regionX'];
		$regionY = $_POST['regionY'];
		$regionW = $_POST['regionW'];
		$regionH = $_POST['regionH'];

        $productID = $_POST["selectedProductID"];
        $arr_variationIDs = explode('|', $_POST["variationIDs"]);

		//$this->app->SetRegionParameters([$regionX, $regionY, $regionW, $regionH, $productID, $variationIDs]);
        $variations_table = $wpdb->prefix.$this->app->DB_Table_VariationImages;

        $query_update = "UPDATE `$variations_table` SET `SavedImageSrc`=`ImageSrc`, `PosX`=%d, `PosY`=%d, `AreaW`=%d, `AreaH`=%d  WHERE `ProductID`=%d AND (`VariationID`=%d OR `VariationID`=%d);";
        $wpdb->query($wpdb->prepare($query_update, array($regionX, $regionY, $regionW, $regionH, $productID, $arr_variationIDs[0], $arr_variationIDs[1])));

        $query_get_imgs = "SELECT `ImageSrc`, `ImageFullSizeSrc` FROM `$variations_table` WHERE `ProductID`=%d AND `VariationID`=%d";
        $var_imgs = $wpdb->get_results($wpdb->prepare($query_get_imgs, array($productID, $arr_variationIDs[0])));



        $standard_img_src = $var_imgs[0]->ImageSrc;
        $large_img_src = $var_imgs[0]->ImageFullSizeSrc;

        $standard_img_info = getimagesize($standard_img_src);
        $large_img_info = getimagesize($large_img_src);


        $st_img_w = is_int($standard_img_info[0]) ? intval($standard_img_info[0]) : 0;
        $st_img_h = is_int($standard_img_info[1]) ? intval($standard_img_info[1]) : 0;

        $large_img_w = is_int($large_img_info[0]) ? intval($large_img_info[0]) : 0;
        $large_img_h = is_int($large_img_info[1]) ? intval($large_img_info[1]) : 0;

        if ($st_img_w > 0 && $st_img_h > 0 && $large_img_h > 0 && $large_img_w > 0) {

            $coef_w = intval($large_img_w) / intval($st_img_w);
            $coef_h = intval($large_img_h) / intval($st_img_h);

            $large_img_region_x = intval($regionX) * $coef_w;
            $large_img_region_y = intval($regionY) * $coef_h;
            $large_img_region_w = intval($regionW) * $coef_w;
            $large_img_region_h = intval($regionH) * $coef_h;

            $query_update = "UPDATE `$variations_table` SET `LargePosX`=%d, `LargePosY`=%d, `LargeAreaW`=%d, `LargeAreaH`=%d, `ImageW`=%d, `ImageH`=%d, `LargeImageW`=%d, `LargeImageH`=%d  WHERE `ProductID`=%d AND (`VariationID`=%d OR `VariationID`=%d);";
            $wpdb->query($wpdb->prepare($query_update, array($large_img_region_x, $large_img_region_y, $large_img_region_w, $large_img_region_h, $st_img_w, $st_img_h, $large_img_w, $large_img_h, $productID, $arr_variationIDs[0], $arr_variationIDs[1])));

        }

        $this->SaveHTMLForVariations($productID);

		wp_send_json_success("ok");

	}

    public function SaveHTMLForVariations($productID) {

        global $wpdb;

        $variations_table = $wpdb->prefix.$this->app->DB_Table_VariationImages;

        $query_all_vars = "SELECT * FROM `$variations_table` WHERE `ProductID`=%d";
        $all_vars = $wpdb->get_results($wpdb->prepare($query_all_vars, array($productID)));

        $var_html = '';

        foreach($all_vars as $var) {

            $template = '<input type="hidden" id="%s" data-posx="%s" data-posy="%s" data-areaw="%s" data-areah="%s" data-largeposx="%s" data-largeposy="%s" data-largeareaw="%s" data-largeareah="%s" data-imagew="%s" data-imageh="%s" data-largeimagew="%s" data-largeimageh="%s" data-imgurl="%s" data-largeimgurl="%s">';

            $var_html .= sprintf($template, "v".$var->VariationID, $var->PosX, $var->PosY, $var->AreaW, $var->AreaH, $var->LargePosX, $var->LargePosY, $var->LargeAreaW, $var->LargeAreaH, $var->ImageW, $var->ImageH, $var->LargeImageW, $var->LargeImageH, urlencode($var->ImageSrc), urlencode($var->ImageFullSizeSrc));


        }


        update_option($this->app::OPTION_VARIATION_REGIONS, $var_html);

    }

	public function GetImagePalette() {

		$imgFile = $_POST['imageFile'];
		$noOfColors = $_POST['noOfColors'];
		$regionX = $_POST['regionX'];
		$regionY = $_POST['regionY'];
		$regionW = $_POST['regionW'];
		$regionH = $_POST['regionH'];		

		$result = $this->ImageColorPalette($imgFile, $noOfColors, $regionX, $regionY, $regionW, $regionH);

		wp_send_json_success($result);

	}

	public function SetFallBackImage() {

		$imgFile = $_POST['fallbackImageURL'];		
		$imgW = $_POST['fallbackImageW'];
		$imgH = $_POST['fallbackImageH'];

		update_option($this->app::OPTION_FALLBACK_IMAGE, $imgFile.'|'.$imgW.'|'.$imgH);

		wp_send_json_success("ok");

	}

#endregion	

#region ATTRIBUTE VALUES <--> DATABASE

	function saveAttributeValuesToTable($arrValues) {

		/* Save values to table
		***********************/
		global $wpdb;

		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;

		foreach($arrValues as $arrValue) {

			$value_slug = $arrValue[0];
			$value_attr_record_id = $arrValue[1];
			$price_type = $arrValue[2];
			$price = $arrValue[3];
			//$price = str_replace(",", ".", $price);
			

			if ($price == '') $price = 0;

			$query = sprintf("UPDATE `%s` SET `%s`=%d WHERE `%s`=%d AND `%s`='%s'",
							 $attribute_values_table,
							 $price_type.'_price',
							 number_format(floatval($price),0),
							 'attr_record_id',
							 $value_attr_record_id,
							 'value_slug',
							 $value_slug); 

			$wpdb->query($query);				 


		}

	}



	public function DeleteAllVariationsAndCreateAllVariants() {

		$this->deleteAllVariations();
		$this->CreateMaxVariations();

		$resultHTML = $this->ListAllVariations();

		wp_send_json_success($resultHTML);
	}


	public function DeleteAllAttributesAndVariations() {

		$this->deleteAllVariations();
		$this->deleteAllVariationAttributes();

	}

	
	function deleteAllVariations() {

		global $woocommerce, $products, $wpdb;


		$savedProduct = $this->app->GetSelectedProduct();
		$selectedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];


		$variation_ids = wp_parse_id_list(
			get_posts(
				array(
					'post_parent' => $selectedProductID,
					'post_type'   => 'product_variation',
					'fields'      => 'ids',
					'post_status' => array( 'any', 'trash', 'auto-draft' ),
					'numberposts' => -1,
				)
			)
		);

		if ( ! empty( $variation_ids ) ) {

			foreach ( $variation_ids as $variation_id ) {

				do_action( 'woocommerce_before_delete_product_variation', $variation_id );
				wp_delete_post( $variation_id, true );
				do_action( 'woocommerce_delete_product_variation', $variation_id );

			}

		}

		delete_transient( 'wc_product_children_' . $selectedProductID );

	}


	function deleteAllVariationAttributes() {

		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$selectedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		$product = new WC_Product_Variable($selectedProductID);

		$product_attributes = $product->get_attributes();

		foreach($product_attributes as $product_attribute) {

			if ($product_attribute->get_variation()) {

				$attr_id = $product_attribute->get_id();

				wc_delete_attribute($attr_id);

			}

		}
		

		delete_transient( 'wc_product_children_' . $selectedProductID );

	}


#endregion

#region VARIATIONS

	public function CreateMaxVariations($setInitialValues=false) {


		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$productID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];
		$savedProductID = $productID;


		$savedSizeAttr = $this->app->GetSizeAttributeField();
		$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();

		/* Get size values
		******************/
		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
		$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;
		
		$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedSizeAttr')";
		$size_values = $wpdb->get_results($query1);		


		/* Get finishing type values
		****************************/		
		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
		$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;
		
		$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedFinishingTypeAttr')";
		$finishing_type_values = $wpdb->get_results($query1);		


		foreach($size_values as $size_value) {

			$size_value_name = $size_value->value_text;
			$size_value_slug = $size_value->value_slug;
			$size_value_regular_price = $size_value->regular_price;
			$size_value_sale_price = $size_value->sale_price;


			foreach($finishing_type_values as $finishing_type_value) {

				$finishing_type_value_name = $finishing_type_value->value_text;
				$finishing_type_value_slug = $finishing_type_value->value_slug;
				$finishing_type_value_regular_price = $finishing_type_value->regular_price;
				$finishing_type_value_sale_price = $finishing_type_value->sale_price;


				$total_regular_price = floatval($size_value_regular_price) + floatval($finishing_type_value_regular_price);
				$total_sale_price = floatval($size_value_sale_price) + floatval($finishing_type_value_sale_price);

				/* Create variation
				*******************/
				$variation = new WC_Product_Variation();
				$variation->set_parent_id( $productID );
				
				$variation->set_attributes( array( $savedSizeAttr => $size_value_name, $savedFinishingTypeAttr => $finishing_type_value_name));	
			
				$variation->set_stock_status( 'instock' ); // outofstock, onbackorder
				
				// prices
				$variation->set_regular_price( $setInitialValues ? 0 : $total_regular_price );
				if ($total_sale_price > 0) $variation->set_sale_price( $total_sale_price );
				
				//$variation->set_description( 'Programmatically created variation' );
							
				$variation->save();		

			}


		}

	}


	/* No of variations
	********************/
	public function NoOfVariations() {

		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$productID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		$product = wc_get_product($productID);

		if (!isset($product)) return 0;

		$variations = $product->get_available_variations();		

		$isEmpty = is_null($variations) || empty($variations);

		return ($isEmpty ? 0 : count($variations));

	}


	/* List all variations
	***********************/
	public function ListAllVariations() {

		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$productID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		$product = wc_get_product($productID);

		if (!isset($product)) return '';

		$savedSizeAttr = $this->app->GetSizeAttributeField();
		$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();

		if ($savedSizeAttr == '' || $savedFinishingTypeAttr == '') return '';

		$variations = $product->get_available_variations();
	
		if (is_null($variations) || empty($variations)) return '<span style="color : red; font-weight : bold;">No variation found for this product.</span>';

		$resultHtml = '<table class="ub_wchsd_variation_list">';
		$resultHtml .= '<tr>';
		$resultHtml .= '<th>#</th>';
		$resultHtml .= '<th>Size</th>';
		$resultHtml .= '<th>Finishing type</th>';	
		$resultHtml .= '<th>Regular price</th>';
		$resultHtml .= '<th>Sale price</th>';		
		$resultHtml .= '</tr>';

		$ord=1;

		$min_price = -1;
		$max_price = -1;

		foreach($variations as $variation){

			$resultHtml .= '<tr>';

			$variation_id = $variation["variation_id"];
			$objVar = wc_get_product($variation_id);
			
			$regular_price = $objVar->get_regular_price();
			$sale_price = $objVar->get_sale_price();
			
			$base_price = $sale_price == '' ? $regular_price : $sale_price;
			
			$base_price_val = floatval($base_price);

			$min_price = $min_price == -1 ? $base_price_val : ($base_price_val < $min_price ? $base_price_val : $min_price);
			$max_price = $max_price == -1 ? $base_price_val : ($base_price_val > $max_price ? $base_price_val : $max_price);

			$resultHtml .= '<td>'.$ord.'</td>';
			$resultHtml .= '<td>'.$variation["attributes"]["attribute_".$savedSizeAttr].'</td>';
			$resultHtml .= '<td>'.$variation["attributes"]["attribute_".$savedFinishingTypeAttr].'</td>';
			$resultHtml .= '<td style="text-align: center;">'.$regular_price.'</td>';
			$resultHtml .= '<td style="text-align: center;">'.$sale_price.'</td>';

			$resultHtml .= '</tr>';

			$ord++;

		}

		$resultHtml .= '</table>';

		if ($min_price > -1 && $max_price > -1) $resultHtml .= '<br/><strong>Price range : '.$min_price.' - '.$max_price.'</strong>';

		return $resultHtml;

	}


	/* List all variations
	***********************/
	public function GetVariationPrices() {

		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$productID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		$product = wc_get_product($productID);

		if (!isset($product)) return '';

		$savedSizeAttr = $this->app->GetSizeAttributeField();
		$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();

		if ($savedSizeAttr == '' || $savedFinishingTypeAttr == '') return '';

		$variations = $product->get_available_variations();
	
		if (is_null($variations) || empty($variations)) return '<span style="color : red; font-weight : bold;">No variation found for this product.</span>';

		$result_html = '';

		foreach($variations as $variation){

			$variation_id = $variation["variation_id"];
			$objVar = wc_get_product($variation_id);
			
			$regular_price = $objVar->get_regular_price();
			$sale_price = $objVar->get_sale_price();

			$size = $variation["attributes"]["attribute_".$savedSizeAttr];
			$finishing = $variation["attributes"]["attribute_".$savedFinishingTypeAttr];			

			$result_html .= '<input type="hidden" class="hidden_variation_prices" id="variation_'.$variation_id.'" data-size="'.$size.'" data-finishing="'.$finishing.'" data-regular-price="'.$regular_price.'" data-sale-price="'.$sale_price.'">';

		}


		return $result_html;

	}	


	/* Update variation prices
	***************************/
	public function UpdateVariationPrices() {

		global $woocommerce, $products, $wpdb;

		$savedProduct = $this->app->GetSelectedProduct();
		$productID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];
		$savedProductID = $productID;

		$savedSizeAttr = $this->app->GetSizeAttributeField();
		$savedFinishingTypeAttr = $this->app->GetFinishingTypeAttributeField();

		/* Get size values
		******************/
		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
		$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;
		
		

		$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedSizeAttr')";
		$size_values = $wpdb->get_results($query1);		


		/* Get finishing type values
		****************************/		
		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;
		$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;
		
		$query1 = "SELECT * FROM `$attribute_values_table` WHERE `attr_record_id` IN (SELECT `id` FROM `$attributes_table` WHERE `product_id`=$savedProductID AND `attribute_slug`='$savedFinishingTypeAttr')";
		$finishing_type_values = $wpdb->get_results($query1);		


		$attribute_variants = [];

		foreach($size_values as $size_value) {

			$size_value_name = $size_value->value_text;
			$size_value_slug = $size_value->value_slug;
			$size_value_regular_price = $size_value->regular_price;
			$size_value_sale_price = $size_value->sale_price;


			foreach($finishing_type_values as $finishing_type_value) {

				$finishing_type_value_name = $finishing_type_value->value_text;
				$finishing_type_value_slug = $finishing_type_value->value_slug;
				$finishing_type_value_regular_price = $finishing_type_value->regular_price;
				$finishing_type_value_sale_price = $finishing_type_value->sale_price;


					$fl_size_regular_value = floatval($size_value_regular_price);
					$fl_finishing_type_value_regular_price = floatval($finishing_type_value_regular_price);

					$fl_size_value_sale_price = floatval($size_value_sale_price);
					$fl_finishing_type_value_sale_price = floatval($finishing_type_value_sale_price);
					
					$total_regular_price = $fl_size_regular_value + $fl_finishing_type_value_regular_price;
					$total_sale_price = $fl_size_value_sale_price + $fl_finishing_type_value_sale_price;

					if ($total_sale_price == 0) {

						$total_sale_price = '';
						
					}
					else {

						$total_sale_price = $fl_size_value_sale_price > 0 ? $fl_size_value_sale_price : $fl_size_regular_value;
						$total_sale_price += $fl_finishing_type_value_sale_price > 0 ? $fl_finishing_type_value_sale_price  : $fl_finishing_type_value_regular_price;

					}

					if ($total_regular_price == 0) $total_regular_price = '';

					//$total_regular_price = floatval($size_value_regular_price) + floatval($finishing_type_value_regular_price) + floatval($finishing_options_value_regular_price);
					//$total_sale_price = floatval($size_value_sale_price) + floatval($finishing_type_value_sale_price) + floatval($finishing_options_value_sale_price);


					$attribute_variants[$size_value_name.'|'.$finishing_type_value_name] = 
												array("regular_price"=> $total_regular_price,
														"sale_price" => $total_sale_price);

			}


		}

		$product = wc_get_product($productID);
		$variations = $product->get_available_variations();

		foreach($variations as $variation) {

			$variation_id = $variation["variation_id"];

			$size_attr = $variation["attributes"]["attribute_".$savedSizeAttr];
			$type_attr = $variation["attributes"]["attribute_".$savedFinishingTypeAttr];

			$key = $size_attr.'|'.$type_attr;
			
			$new_regular_price = $attribute_variants[$key]["regular_price"];
			$new_sale_price = $attribute_variants[$key]["sale_price"];

			$variation = wc_get_product($variation_id);

			$variation->set_regular_price($new_regular_price);
			$variation->set_sale_price($new_sale_price);
			$variation->save();

		}


		WC_Cache_Helper::get_transient_version( 'product', true );

		$resultHTML = $this->ListAllVariations();
		
		wp_send_json_success($resultHTML);

	}



#endregion
	
#region IMPORT ATTRIBUTES FROM WC

	public function ImportAttributesFromWC() {
		
		global $woocommerce, $products, $wpdb;
		
		
		/* Create fonts table
		***********************/
		$fonts_table = $wpdb->prefix.$this->app->DB_Table_Fonts;

		$query_create_fonts_table = "CREATE TABLE IF NOT EXISTS `$fonts_table` (`font_name` VARCHAR(200) NOT NULL , `font_file_name` VARCHAR(500) NOT NULL, `font_group` VARCHAR(100) NOT NULL) ENGINE = MyISAM;";

		$wpdb->query($query_create_fonts_table);


		/* PROCESS FONTS
		*******************/
		$this->app->ProcessFonts();



		/* Create Products table (if it doesn't exist)
		*********************************************/		
		$products_table = $wpdb->prefix.$this->app->DB_Table_Products;
		
		$query_create_table = "CREATE TABLE IF NOT EXISTS `$products_table` (`product_id` int NOT NULL, `product_slug` varchar(250) NOT NULL, `product_name` varchar(250) DEFAULT NULL, `is_selected` tinyint NOT NULL DEFAULT '0', `to_delete` tinyint NOT NULL DEFAULT '1', UNIQUE KEY `product_id` (`product_id`)) ENGINE=MyISAM";

		$wpdb->query($query_create_table);		
		
		
		/* Create Attributes table (if it doesn't exist)
		*********************************************/
		$attributes_table = $wpdb->prefix.$this->app->DB_Table_Attributes;
		
		$query_create_table = "CREATE TABLE IF NOT EXISTS `$attributes_table` (`id` int NOT NULL AUTO_INCREMENT, `product_id` int NOT NULL, `attribute_name` varchar(250) DEFAULT NULL, `attribute_slug` varchar(250) NOT NULL, `regular_price` decimal(10,0) NOT NULL DEFAULT '0', `sale_price` decimal(10,0) NOT NULL DEFAULT '0', `is_selected` tinyint NOT NULL DEFAULT '0', `to_delete` tinyint NOT NULL DEFAULT '1', PRIMARY KEY (`id`)) ENGINE=MyISAM";

		$wpdb->query($query_create_table);
		
		
		/* Create Attribute values table (if it doesn't exist)
		*****************************************************/
		$attribute_values_table = $wpdb->prefix.$this->app->DB_Table_Attr_Values;		
		
		$query_create_table = "CREATE TABLE IF NOT EXISTS `$attribute_values_table` (`attr_record_id` int NOT NULL, `value_text` varchar(250) NOT NULL, `value_slug` varchar(250) NOT NULL, `regular_price` decimal(10,0) NOT NULL DEFAULT '0', `sale_price` decimal(10,0) NOT NULL DEFAULT '0', `is_selected` tinyint NOT NULL DEFAULT '0', `to_delete` tinyint NOT NULL DEFAULT '1') ENGINE=MyISAM";

		$wpdb->query($query_create_table);		
		

        /* Create Variations table for storing image area and checking changes in images
        (Added 11.06.2023)
        *************************************************************************/
        $variations_table = $wpdb->prefix.$this->app->DB_Table_VariationImages;

        $query_create_variations_table = "CREATE TABLE IF NOT EXISTS `$variations_table` (`ID` int NOT NULL AUTO_INCREMENT,`ProductID` int NOT NULL DEFAULT '0',`VariationID` int NOT NULL DEFAULT '0',`SelectSizeValue` varchar(255) NOT NULL,`EngravingOptionValue` varchar(255) NOT NULL,`ImageSrc` varchar(5000) NOT NULL,`ImageFullSizeSrc` varchar(5000) NOT NULL,`ImageID` int NOT NULL DEFAULT '0',`SavedImageSrc` varchar(5000) NOT NULL,`PosX` int NOT NULL DEFAULT '0',`PosY` int NOT NULL DEFAULT '0',`AreaW` int NOT NULL DEFAULT '0',`AreaH` int NOT NULL DEFAULT '0', `LargePosX` int NOT NULL DEFAULT '0',`LargePosY` int NOT NULL DEFAULT '0',`LargeAreaW` int NOT NULL DEFAULT '0',`LargeAreaH` int NOT NULL DEFAULT '0',`ImageW` int NOT NULL DEFAULT '0',`ImageH` int NOT NULL DEFAULT '0',`LargeImageW` int NOT NULL DEFAULT '0',`LargeImageH` int NOT NULL DEFAULT '0',`to_delete` tinyint NOT NULL DEFAULT '0', PRIMARY KEY (`ID`)) ENGINE=MyISAM";

        $wpdb->query($query_create_variations_table);


		/* Mark all rows as "to delete"
		*******************************/
		$query_mark1 = "UPDATE `$products_table` SET `to_delete`=1";
		$query_mark2 = "UPDATE `$attributes_table` SET `to_delete`=1";
		$query_mark3 = "UPDATE `$attribute_values_table` SET `to_delete`=1";
        $query_mark4 = "UPDATE `$variations_table` SET `to_delete`=1";

		$wpdb->query($query_mark1);
		$wpdb->query($query_mark2);
		$wpdb->query($query_mark3);
        $wpdb->query($query_mark4);

		/* Fetch variations from WooCommerce
		************************************/
		$query = new WC_Product_Query( array(
			'limit' => -1,
			'status' => 'publish',
			'type' =>  'variable',
			'orderby' => 'name',
			'order' => 'ASC',
			'return' => 'ids',
		) );

		$productIDs = $query->get_products();		
		
		
		
		$products = [];
		
		foreach($productIDs as $productID) {

			$product = new WC_Product_Variable($productID);

            $this->FeaturedImageIDs[intval($productID)] = $product->image_id;

			$prod_name = $product->get_name();
			$prod_slug = $product->get_slug();


            /* Collection variations
            *********************/
            $variations = $product->get_available_variations();



            foreach($variations as $variation) {


                $var_id = $variation["variation_id"];

                $var_size_attr_value = isset($variation["attributes"]["attribute_select-size"]) ? $variation["attributes"]["attribute_select-size"] : '';
                $var_engraving_attr_value = isset($variation["attributes"]["attribute_engraving-options"]) ? $variation["attributes"]["attribute_engraving-options"] : '';

                $var_img_src = $variation["image"]["src"];
                $var_img_full_src = isset($variation["image"]["full_src"]) ? $variation["image"]["full_src"] : '';
                $var_img_id = $variation["image_id"];

                /* Mark existing ones
                *******************/
                $query_update = "UPDATE `$variations_table` SET `to_delete`=0, `ImageSrc`=%s, `ImageFullSizeSrc`=%s  WHERE `ProductID`=%d AND `VariationID`=%d;";
                $wpdb->query($wpdb->prepare($query_update, array($var_img_src, $var_img_full_src, $productID, $var_id)));

                /* Insert new
                ************/
                $query_check = "SELECT COUNT(id) AS totalMatching FROM `$variations_table` WHERE `ProductID`=%d AND `VariationID`=%d;";
                $objTotalMatching = $wpdb->get_var($wpdb->prepare($query_check, array($productID, $var_id)));

                $totalMatching = is_null($objTotalMatching) ? 0 : $objTotalMatching[0][0];

                if ($totalMatching == 0) {

                    $query_insert = "INSERT INTO `$variations_table` (`ProductID`,`VariationID`,`SelectSizeValue`,`EngravingOptionValue`,`ImageSrc`,`ImageFullSizeSrc`,`ImageID`,`to_delete`) VALUES(%d, %d, %s, %s, %s, %s, %d, %d)";
                    $wpdb->query($wpdb->prepare($query_insert, array($productID, $var_id, $var_size_attr_value, $var_engraving_attr_value, $var_img_src, $var_img_full_src, $var_img_id, 0 )));

                }

            }


			/* Get attributes array
			***************************/
            $variation_attributes = $product->get_variation_attributes();


			$attributes = [];
			
			foreach($variation_attributes as $attrName=>$attrArray) {
				
				$attr_name = $attrName;
				$attr_slug = sanitize_title($attr_name);
				
				/* Get values
				*************/
				$valuesObj = $attrArray;			
				$values = '';
				
				foreach($valuesObj as $value) {
					
					$values .= $value.'|';
					
				}
				
				$values = substr($values,0, strlen($values)-1);
				
				$attr_values = $values;
				
				$attributes[] = [$attr_slug, $attr_name, $attr_values];
				
			}
			
			/* Add to products array
			************************/
			$products[] = [$productID, $prod_name, $prod_slug,$attributes];			
			
		}

		
		/* UPDATE TABLES
		****************/
		foreach($products as $product) {
			
			$productID = intval($product[0]);
			$productName = $product[1];
			$productSlug = $product[2];
			
			$attribs = $product[3];
			

			/* Check product
			****************/
			$query_insert = "INSERT INTO `$products_table` (`product_id`,`product_name`,`product_slug`,`to_delete`) VALUES(%d, %s, %s, 0) ON DUPLICATE KEY UPDATE `product_name`=%s, `product_slug`=%s, `to_delete`=0, `product_id`=%d";					
			$wpdb->query($wpdb->prepare($query_insert, array($productID, $productName, $productSlug, $productName, $productSlug, $productID)));
			
			
		
			
			/* Check attributes
			******************/
			foreach($attribs as $attrib) {
				
				$attr_slug = $attrib[0];
				$attr_name = $attrib[1];
				
				$attr_values = $attrib[2];
				
				$query_update = "UPDATE `$attributes_table` SET `to_delete`=0 WHERE `product_id`=%d AND `attribute_slug`=%s;";
				$wpdb->query($wpdb->prepare($query_update, array($productID, $attr_slug)));


				$query_check = "SELECT COUNT(id) AS totalMatching FROM `$attributes_table` WHERE `product_id`=%d AND `attribute_slug`=%s;";
				$objTotalMatching = $wpdb->get_var($wpdb->prepare($query_check, array($productID, $attr_slug)));

				$totalMatching = is_null($objTotalMatching) ? 0 : $objTotalMatching[0][0];

				if ($totalMatching == 0) {
				
					$query_insert = "INSERT INTO `$attributes_table` (`product_id`,`attribute_name`,`attribute_slug`,`to_delete`) VALUES(%d, %s, %s, %d)";				
					$wpdb->query($wpdb->prepare($query_insert, array($productID, $attr_name, $attr_slug, 0)));
				
				}

				

				
				
				/* Find record id from attributes table
				***************************************/
				$query_select = "SELECT id FROM `$attributes_table` WHERE `product_id`=%d AND `attribute_slug`=%s";
				
				$atr_record_id = $wpdb->get_var($wpdb->prepare($query_select, array($productID, $attr_slug)));
				
				
				/* Check values
				***************/
				$values = explode("|", $attr_values);
				

				foreach($values as $value) {
					

					$value_slug = sanitize_title($value);

					$query_update = "UPDATE `$attribute_values_table` SET `to_delete`=0, `value_text`=%s, `value_slug`=%s WHERE `attr_record_id`=%d AND `value_slug`='%s'";
					$wpdb->query($wpdb->prepare($query_update, array($value, $value_slug, $atr_record_id, $value_slug)));

					$query_check = "SELECT COUNT(attr_record_id) AS totalMatching FROM `$attribute_values_table` WHERE `attr_record_id`=%d AND `value_slug`=%s;";
					$objTotalMatching = $wpdb->get_var($wpdb->prepare($query_check, array($atr_record_id, $value_slug)));
					
					$totalMatching = is_null($objTotalMatching) ? 0 : $objTotalMatching[0][0];

					if ($totalMatching == 0) {

						$query_insert = "INSERT INTO `$attribute_values_table` (`attr_record_id`,`value_text`,`to_delete`,`value_slug`) VALUES(%d, %s, %d, %s)";					
						$wpdb->query($wpdb->prepare($query_insert, array($atr_record_id, $value, 0, $value_slug)));

					}
				
					
				}
			
			}
			
		}

		$query_delete = "DELETE FROM `$attributes_table` WHERE `to_delete`=1";
		$wpdb->query($query_delete);

		$query_delete = "DELETE FROM `$products_table` WHERE `to_delete`=1";
		$wpdb->query($query_delete);		

		$query_delete = "DELETE FROM `$attribute_values_table` WHERE `to_delete`=1";
		$wpdb->query($query_delete);

        $query_delete = "DELETE FROM `$variations_table` WHERE `to_delete`=1";
        $wpdb->query($query_delete);

    }
	
#endregion

#region TEST FUNCTIONS

	public function ResetEverything() {

		$this->ResetAll();

		wp_send_json_success("ok");

	}


#endregion	

#region CREATE PRODUCT


public function CreateProduct() {


	$productTitle = $_POST["productTitle"];

	/* Create PRODUCT
	*******************************/
	$product = $this->wc_create_product($productTitle, $productTitle);
	$product_id = $product->get_id();

	$atts = $this->CreateAttributes();


	//Adding attributes to the created product
	$product->set_attributes( $atts );
	$product->save();

	wc_delete_product_transients( $product_id);


	/* Save values
	*************************/
	$this->app->SetSelectedProduct($product_id.'|'.$productTitle);

	/* Create variations
	********************/
	$this->ImportAttributesFromWC();
	$this->CreateMaxVariations(true);

	wp_send_json_success("ok");



}

/**
 * Create all necessary attributes
 * @return Object	Attributes array
 */
public function CreateAttributes() {

	$atts = [];


	/* Create SIZES attribute
	*******************************/
	$new_attribute = $this->CreateSizesAttribute();

	$atts[] = $new_attribute;


	/* Create FONTS attribute
	*******************************/
	$new_attribute = $this->CreateFontsAttribute();

	$atts[] = $new_attribute;	



	/* Create ENGRAVING OPTIONS attribute
	*******************************/
	$new_attribute = $this->CreateEngravingOptionsAttribute();

	$atts[] = $new_attribute;

	return $atts;

}

/**
 * Create SIZES attribute
 * @return Object	Attribute array
 */
public function CreateSizesAttribute() {

	$sizeArr = $this->app->Defined_Sizes;
	$sizeInfo = explode(',', $sizeArr[0]);

	$sizeName = trim($sizeInfo[0]);
	$sizeSlug = trim($sizeInfo[1]);

	$attr_label = $sizeName;

	$options = [];

	$totalSizes = count($sizeInfo);

	for ($i=1; $i < $totalSizes; $i++) {

		 $options[]= $sizeArr[$i];

	}

	$new_attribute = $this->wc_create_attributes($attr_label, $options); 

	$this->app->SetSizeAttributeField($sizeSlug);	

	return $new_attribute;

}


/**
 * Create FONTS attribute
 * @return Object	Attribute array
 */
public function CreateFontsAttribute() {

	$fontsArr = $this->app->Defined_Fonts;
	$fontsInfo = explode(',', $fontsArr[0]);
	$fontsName = trim($fontsInfo[0]);
	$fontsSlug = trim($fontsInfo[1]);

	$attr_label = $fontsName;

	$options = [];

	$totalFontsInfoRows = count($fontsArr);

	for ($i=1; $i < $totalFontsInfoRows; $i++) {

		$tmpArr = explode(',', $fontsArr[$i]);
		$options[]= $tmpArr[0];

	}

	$new_attribute = $this->wc_create_attributes($attr_label, $options); 

	$this->app->SetTextStyleAttributeField($fontsSlug);

	return $new_attribute;

}


/**
 * Create ENGRAVING OPTIONS attribute
 * @return Object	Attribute array
 */
public function CreateEngravingOptionsAttribute() {

	$engravingArr = $this->app->Defined_EngravingOptions;
	$engravingsInfo = explode(',', $engravingArr[0]);
	$engravingsTitle = trim($engravingsInfo[0]);
	$engravingSlug = trim($engravingsInfo[1]);

	$attr_label = $engravingsTitle;

	$options = [];

	$totalEngravingsInfoRows = count($engravingArr);

	for ($i=1; $i < $totalEngravingsInfoRows; $i++) {

		$tmpArr = explode(',', $engravingArr[$i]);
		$options[]= $tmpArr[0];

	}

	$new_attribute = $this->wc_create_attributes($attr_label, $options); 

	$this->app->SetFinishingTypeAttributeField($engravingSlug);

	return $new_attribute;

}

/**
 * Create a variable product on woocommerce
 * @param  string $name    Product name
 * @param  string $description   Description
 * @return Object	WC_Product_Variable Product
 */
function wc_create_product($name, $description){

    $product = new WC_Product_Variable();
    $product->set_description($description);
    $product->set_name($name);
	$product->set_status( 'publish' ); 
	$product->set_catalog_visibility( 'visible' );
    //$product->set_sku('test-shirt');
    //$product->set_price(1);
    //$product->set_regular_price(1);
	$product->set_manage_stock(false);
    //$product->set_stock_status('instock');
    //$product->set_stock_quantity(10000);
    $product->save();

    return $product;
}



/**
 * Create Product Attributes 
 * @param  string $name    Attribute name
 * @param  array $options Options values
 * @return Object	WC_Product_Attribute 
 */
function wc_create_attributes( $name, $options, $isVariation=true){

    $attribute = new WC_Product_Attribute();
    $attribute->set_id(0);
    $attribute->set_name($name);
    $attribute->set_options($options);
    $attribute->set_visible(true);
    $attribute->set_variation($isVariation);
    
	return $attribute;

}


#endregion

#region IMAGE FUNCTIONS

public function ImageColorPalette($imageFile, $numColors, $regionX, $regionY, $regionW, $regionH, $granularity = 5) 
{ 
	$granularity = max(1, abs((int)$granularity)); 
	$colors = array(); 
	$size = @getimagesize($imageFile); 

	if($size === false) 
	{ 
		user_error("Unable to get image size data"); 
		return false; 
	} 

	//$img = @imagecreatefromjpeg($imageFile);
	// Andres mentioned in the comments the above line only loads jpegs, 
	// and suggests that to load any file type you can use this:
	$imgSource = @imagecreatefromstring(file_get_contents($imageFile)); 

	if(!$imgSource) 
	{ 
		user_error("Unable to open image file"); 
		return false; 
	} 

	$img = imagecreatetruecolor(intval($regionW), intval($regionH));

	imagecopymerge($img, 
				   $imgSource, 
				   0, 
				   0, 
				   intval($regionX), 
				   intval($regionY), 
				   intval($regionW), 
				   intval($regionH), 
				   100);



	for($x = 0; $x < $size[0]; $x += $granularity) 
	{ 
		for($y = 0; $y < $size[1]; $y += $granularity) 
		{ 
			$thisColor = imagecolorat($img, $x, $y); 
			$rgb = imagecolorsforindex($img, $thisColor); 
			$red = round(round(($rgb['red'] / 0x33)) * 0x33); 
			$green = round(round(($rgb['green'] / 0x33)) * 0x33); 
			$blue = round(round(($rgb['blue'] / 0x33)) * 0x33); 
			$thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue); 
			if(array_key_exists($thisRGB, $colors)) 
			{ 
				$colors[$thisRGB]++; 
			} 
			else 
			{ 
				$colors[$thisRGB] = 1; 
			} 
		} 
	} 

	arsort($colors); 

	// return array_slice(array_keys($colors), 0, $numColors);
	
	$arr = array_slice(array_keys($colors), 0, $numColors);

	$totalRows = count($arr);

	$result = '';


	$selectedColor = "#".$arr[0];
	$lighterColor = $this->LuminanceLight($selectedColor, 0.5);
	$darkerColor = $this->LuminanceDark($selectedColor, 0.5);


	$result = $selectedColor.','.$lighterColor.','.$darkerColor;
	
	imagedestroy($img);
	imagedestroy($imgSource);

	return $result;
} 


function LuminanceLight($hexcolor, $percent)
{
  if ( strlen( $hexcolor ) < 6 ) {
    $hexcolor = $hexcolor[0] . $hexcolor[0] . $hexcolor[1] . $hexcolor[1] . $hexcolor[2] . $hexcolor[2];
  }
  $hexcolor = array_map('hexdec', str_split( str_pad( str_replace('#', '', $hexcolor), 6, '0' ), 2 ) );

  foreach ($hexcolor as $i => $color) {
    $from = $percent < 0 ? 0 : $color;
    $to = $percent < 0 ? $color : 255;
    $pvalue = ceil( ($to - $from) * $percent );
    $hexcolor[$i] = str_pad( dechex($color + $pvalue), 2, '0', STR_PAD_LEFT);
  }

  return '#' . implode($hexcolor);
}

function LuminanceDark($hexcolor, $percent)
{
  if ( strlen( $hexcolor ) < 6 ) {
    $hexcolor = $hexcolor[0] . $hexcolor[0] . $hexcolor[1] . $hexcolor[1] . $hexcolor[2] . $hexcolor[2];
  }
  $hexcolor = array_map('hexdec', str_split( str_pad( str_replace('#', '', $hexcolor), 6, '0' ), 2 ) );

  foreach ($hexcolor as $i => $color) {
    $from = $percent < 0 ? 0 : $color;
    $to = $percent < 0 ? $color : 0;
    $pvalue = ceil( ($to - $from) * $percent );
    $hexcolor[$i] = str_pad( dechex($color + $pvalue), 2, '0', STR_PAD_LEFT);
  }

  return '#' . implode($hexcolor);
}

#endregion

#region PHOTOS FROM CUSTOMERS

public function CreateCustomerPhotosDB() {

	global $wpdb;

	$cust_photos_table = $wpdb->prefix.$this->app->DB_Table_CustomerPhotos;

	$query_create_table = "CREATE TABLE IF NOT EXISTS `$cust_photos_table` (
		`ID` int NOT NULL AUTO_INCREMENT,
		`image_attach_id` int NOT NULL DEFAULT 0,
		`size_in_attribute` varchar(30) DEFAULT NULL,
		`size_for_class` varchar(20) DEFAULT NULL,
		`font_name` varchar(100) DEFAULT NULL,
		`engraving_option` varchar(100) DEFAULT NULL,
		`variation_price` varchar(10) DEFAULT NULL,
		PRIMARY KEY (`ID`)
	  ) ENGINE=MyISAM";

	$wpdb->query($query_create_table);

}

public function addUpdateCustomerPhoto() {

	global $wpdb;

	$image_attach_id = $_POST['image_attach_id'];
	$size_in_attribute = $_POST['size_in_attribute'];
	$font_name = $_POST['font_name'];
	$engraving_option = $_POST['engraving_option'];
	$variation_price = $_POST['variation_price'];

	$size_class_name = strtolower($size_in_attribute);
    $size_class_name = str_replace("cm", "", $size_class_name);
    $size_class_name = str_replace(" ", "", $size_class_name);

	/* Save
	*******/
	$customer_photos_table = $wpdb->prefix.$this->app->DB_Table_CustomerPhotos;

	$query_check = "SELECT COUNT(ID) AS totalMatching FROM `$customer_photos_table` WHERE `image_attach_id`=%d";
	$objTotalMatching = $wpdb->get_var($wpdb->prepare($query_check, array($image_attach_id)));
	
	$totalMatching = is_null($objTotalMatching) ? 0 : $objTotalMatching[0][0];

	if ($totalMatching == 0) {

		$query_insert = "INSERT INTO `$customer_photos_table` (`image_attach_id`,`size_in_attribute`,`size_for_class`,`font_name`,`engraving_option`,`variation_price`) VALUES(%d, %s, %s, %s, %s, %s)";					
		$wpdb->query($wpdb->prepare($query_insert, array($image_attach_id, $size_in_attribute, $size_class_name, $font_name, $engraving_option, $variation_price)));

	}
	else {

		$query_update = "UPDATE `$customer_photos_table` SET `size_in_attribute`=%s,`size_for_class`=%s,`font_name`=%s,`engraving_option`=%s,`variation_price`=%s WHERE `image_attach_id`=%d";					
		$wpdb->query($wpdb->prepare($query_update, array($size_in_attribute, $size_class_name, $font_name, $engraving_option, $variation_price, $image_attach_id)));

	}

	wp_send_json_success("ok");

}

public function deleteCustomerPhoto() {

	global $wpdb;

	$image_attach_id = $_POST['image_attach_id'];

	/* Save
	*******/
	$customer_photos_table = $wpdb->prefix.$this->app->DB_Table_CustomerPhotos;

	$query_update = "DELETE FROM `$customer_photos_table` WHERE `image_attach_id`=%d";					
	$wpdb->query($wpdb->prepare($query_update, array($image_attach_id)));


	wp_send_json_success("ok");

}

#endregion

}
