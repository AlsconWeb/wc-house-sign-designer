<?php




$line_break = "_lbr_";



$RootFile = __FILE__;
$RootPath = dirname(dirname(__FILE__));
$fontsPath = $RootPath."/assets/fonts/";

$isOutputBase64 = isset($_REQUEST["hsd_output"]) && $_REQUEST["hsd_output"]=="b64";

/* Input parameters
*******************/
$line1 = $_REQUEST["hsd_line1"];
$line2 = $_REQUEST["hsd_line2"];
$img_url = $_REQUEST["hsd_feat_img"];
$pos_x1 = $_REQUEST["hsd_x1"];
$pos_y1 = $_REQUEST["hsd_y1"];
$pos_x2 = $_REQUEST["hsd_x2"];
$pos_y2 = $_REQUEST["hsd_y2"];

$fontInput = explode('|', $_REQUEST["hsd_font_name"]);
$fontName = $fontInput[0];
$fontFile = $fontsPath."/".$fontName;


$finishing = $_REQUEST["hsd_finishing"];
$engraved = $finishing != "uv";
$useSilverColor = $engraved;
$useWhiteColor = $finishing == "uv";

/* End of input parameters
**************************/
$engrave_distance = 2;
$lineSpacingH = 10;

/* Create canvas
*****************/
$canvas = null;

$url_lc = strtolower($img_url);

//try {

	//$canvas = imagecreatefromjpeg($img_url);

//}
//catch(exception $e) {

	//$canvas = imagecreatefrompng($img_url);

//}
//if (stripos($url_lc, '.png') >= 0) $canvas = imagecreatefrompng($img_url);
//if (stripos($url_lc, '.jp') >= 0) $canvas = imagecreatefromjpeg($img_url);

$pngPos = stripos($url_lc, '.png');
$jpgPos = stripos($url_lc, '.jp');

if ($pngPos === false) {

	$canvas = imagecreatefromjpeg($img_url);
}
else {

	$canvas = imagecreatefrompng($img_url);

}


/* CHECK IF FONT FILE IS VALID
******************************/
if (!file_exists($fontFile)) {

	header('Content-Type: image/png');
	imagepng($canvas);
	imagedestroy($canvas);

	return;
}

//$canvas = imagecreatefromjpeg($img_url);

$darkColor = imagecolorallocate($canvas, 99, 99, 99);
$black = imagecolorallocate($canvas, 95, 102, 112);
$white = imagecolorallocate($canvas, 255, 255, 255);
$silver = imagecolorallocate($canvas, 192, 192, 192);
$fontColor = $useWhiteColor ? $white : $silver;

$rowSpacing = 20;
$line1proportion = 0.5;

$area_W = abs(intval($pos_x2)-intval($pos_x1));
$area_H = abs(intval($pos_y2)-intval($pos_y1));

$line2_startY = $pos_y1;

$line1_fontSize = 0;
$line1_calculated_H = 0;
$line1_calculated_W = 0;
$line1_left_padding = 0;
$line1_bottom_padding = 0;

$line2_fontSize = 0;
$line2_calculated_H = 0;
$line2_calculated_W = 0;
$line2_left_padding = 0;
$line2_bottom_padding = 0;


/* Start from the longer line
*****************************/
$line1_len = strlen($line1);
$line2_len = strlen($line2);

$diff = $line1_len - $line2_len;

$targetFontSize = 0;

if ($diff < 0) {

	getSuitableFontSize($area_W, $area_H, (1-$line1proportion), $rowSpacing, $line2, $line2_fontSize, $line2_calculated_H, $line2_calculated_W, $line2_left_padding, $fontFile, $line2_bottom_padding);

	if ($line2_fontSize != -1) {

		$line1_fontSize = $line2_fontSize;

		getSizeForTargetFontSize($line1, $line1_fontSize, $line1_calculated_H, $line1_calculated_W, $line1_left_padding, $fontFile, $line2_calculated_H, $line1_bottom_padding);


	} 
	
	//getSuitableFontSize($area_W, $area_H, $line1proportion, $rowSpacing, $line1, $line1_fontSize, $line1_calculated_H, $line1_calculated_W, $line1_left_padding, $fontFile, $line2_calculated_H);

}
else if ($diff > 0) {

	getSuitableFontSize($area_W, $area_H, $line1proportion, $rowSpacing, $line1, $line1_fontSize, $line1_calculated_H, $line1_calculated_W, $line1_left_padding, $fontFile, $line1_bottom_padding);

	if ($line1_fontSize != -1) {

		$line2_fontSize = $line1_fontSize;

		getSizeForTargetFontSize($line2, $line2_fontSize, $line2_calculated_H, $line2_calculated_W, $line2_left_padding, $fontFile, $line1_calculated_H, $line2_bottom_padding);

	} 
	//getSuitableFontSize($area_W, $area_H, (1-$line1proportion), $rowSpacing, $line2, $line2_fontSize, $line2_calculated_H, $line2_calculated_W, $line2_left_padding, $fontFile, $line1_calculated_H);
}
else {

	getSuitableFontSize($area_W, $area_H, (1-$line1proportion), $rowSpacing, $line2, $line2_fontSize, $line2_calculated_H, $line2_calculated_W, $line2_left_padding, $fontFile, $line2_bottom_padding);

	if ($line2_fontSize != -1) {

		$line1_fontSize = $line2_fontSize;

		getSizeForTargetFontSize($line1, $line1_fontSize, $line1_calculated_H, $line1_calculated_W, $line1_left_padding, $fontFile, $line2_calculated_H, $line1_bottom_padding);

	} 
	
	//getSuitableFontSize($area_W, $area_H, $line1proportion, $rowSpacing, $line1, $line1_fontSize, $line1_calculated_H, $line1_calculated_W, $line1_left_padding, $fontFile);

}


/* ABORT IF INVALID FONT SIZE
*****************************/
if ($line1_fontSize == -1 || $line2_fontSize == -1) {

	header('Content-Type: image/png');
	imagepng($canvas);
	imagedestroy($canvas);

	return;

}

/*
error_log("*********************************************");
error_log("Line 1 => ".$line1);
error_log("Line 2 => ".$line2);
error_log("line1_calculated_H => ".$line1_calculated_H);
error_log("line2_calculated_H => ".$line2_calculated_H);
error_log("rowSpacing => ".$rowSpacing);
*/

$total_height = $line1_calculated_H + $line2_calculated_H + $rowSpacing;
$start_Y_pos =  ($line1_calculated_H > 0 && $line2_calculated_H > 0) ? ($area_H-$total_height)/2 : ($area_H - max($line1_calculated_H, $line2_calculated_H))/2;
$start_Y_pos += $pos_y1-$line1_bottom_padding - $line2_bottom_padding;  // Add top position of region as offset


/* DRAW LINE 1
***************************************/
$pos_x = ($area_W - $line1_calculated_W - $line1_left_padding)/2 + $pos_x1;
$pos_y = $start_Y_pos + $line1_calculated_H;
$start_Y_pos = $pos_y + $rowSpacing + $line1_bottom_padding;

drawText($canvas, $line1, $line1_fontSize, $fontFile, $fontColor, $pos_x, $pos_y, $engraved, $engrave_distance, $darkColor, $black);


/* DRAW LINE 2
**************************************************/
$pos_x = ($area_W - $line2_calculated_W - $line2_left_padding)/2 + $pos_x1;
$pos_y = $start_Y_pos + $line2_calculated_H + $line2_bottom_padding;

drawText($canvas, $line2, $line2_fontSize, $fontFile, $fontColor, $pos_x, $pos_y, $engraved, $engrave_distance, $darkColor, $black);		


if ($isOutputBase64) {

	ob_start();
	imagepng($canvas);
	$imagedata = ob_get_clean();
	imagedestroy($canvas);

	print "data:image/png;base64,".base64_encode($imagedata);

}
else {

	header('Content-Type: image/png');
	imagepng($canvas);
	imagedestroy($canvas);

}








/**
 * Gets suitable font size
 *
 * @param integer $area_W	Region width
 * @param integer $area_H	Region height
 * @param integer $proportion	Proportion to region height
 * @param integer $rowSpacing	 Row spacing
 * @param string $text	 Text to be printed
 * @param integer $line1_fontSize	 line1_fontSize
 * @param integer $line1_calculated_H	line1_calculated_H
 * @param integer $line1_calculated_W	line1_calculated_W
 * @param integer $line1_left_padding	line1_left_padding
 * 
 * @throws Some_Exception_Class If something interesting cannot happen
 * @author Monkey Coder <mcoder@facebook.com>
 * @return Status
 */
function getSuitableFontSize($area_W, $area_H, $proportion, $rowSpacing, $text, &$line_fontSize, &$line_calculated_H, &$line_calculated_W, &$line_left_padding, $fontFile, &$yAxisSpacing, $targetH = 0) {


	$fontSize = 10;
	$maxW = $area_W;
	$maxH = $targetH == 0 ? $area_H * $proportion - $rowSpacing : $targetH;

	$resultOK = true;

	while(true) {

		$boxLine1 = imagettfbbox($fontSize,0, $fontFile, $text);

		if ($boxLine1 === false) {

			$resultOK = false;
			break;
		}		

		$calculatedW = max(abs($boxLine1[2]- $boxLine1[0]), abs($boxLine1[4]- $boxLine1[6]));
		$calculatedH = abs($boxLine1[1] - $boxLine1[7]);

		if ($calculatedH <= $maxH && $calculatedW <= $maxW) {
		
			$line_fontSize = $fontSize;
			$line_calculated_H = $calculatedH;
			$line_calculated_W = $calculatedW;
			$line_left_padding =  $boxLine1[0];

			$yAxisSpacing = $boxLine1[1];

			$fontSize++;
		}
		else {		
			break;
		}
	}

	if ($resultOK == false) $line_fontSize = -1;
}


/**
 * Gets calculated box dimensions for target font
 *
 * @param string $text	Text
 * @param integer $line_fontSize	Target font size
 * @param integer $line_calculated_H	Reference to to-be-calculated H
 * @param integer $line_calculated_W	 Reference to to-be-calculated W
 * @param integer $line_left_padding	 Left padding
 * @param string $fontFile	 Full path to font file
 */
function getSizeForTargetFontSize($text, &$line_fontSize, &$line_calculated_H, &$line_calculated_W, &$line_left_padding, $fontFile, $otherLineH, &$yAxisSpacing) {


	while (true) {


		$boxLine = imagettfbbox($line_fontSize, 0, $fontFile, $text);

		$line_calculated_W = max(abs($boxLine[2]- $boxLine[0]), abs($boxLine[4]- $boxLine[6]));
		$line_calculated_H = abs($boxLine[1] - $boxLine[7]);
		$line_left_padding = $boxLine[0];

		$yAxisSpacing = $boxLine[1];


		/* Compare H with other line's H
		********************************/
		$maxPossible = intval($line_calculated_H) * 1.5;

		if ($otherLineH > $maxPossible) {

			$line_fontSize++;

		}
		else {

			break;

		}

	}

}


/**
 * Gets suitable font size
 *
 * @param string $canvas	
 * @param integer $text	
 * @param integer $fontSize	
 * @param integer $fontFile	 
 * @param string $fontColor	 
 * @param integer $calculatedX	 
 * @param integer $calculatedY	
 * @param integer $isEngraved
 * @param integer $engrave_distance
 * @param integer $darkColor
 * @param integer $black
 * @throws Some_Exception_Class If something interesting cannot happen
 * @author Monkey Coder <mcoder@facebook.com>
 * @return Status
 */
function drawText($canvas, $text, $fontSize, $fontFile, $fontColor, $calculatedX, $calculatedY, $isEngraved, $engrave_distance, $darkColor, $black) {
	
	if ($isEngraved) {

		for ($i=1; $i <=$engrave_distance; $i++) {

		imagettftext($canvas, $fontSize, 0, $calculatedX - $i , $calculatedY - $i, $darkColor, $fontFile, $text);
		imagettftext($canvas, $fontSize, 0, $calculatedX + $i , $calculatedY + $i, $darkColor, $fontFile, $text);
		imagettftext($canvas, $fontSize, 0, $calculatedX - $i , $calculatedY + $i, $darkColor, $fontFile, $text);
		imagettftext($canvas, $fontSize, 0, $calculatedX + $i , $calculatedY - $i, $darkColor, $fontFile, $text);

		}
		//imagettftext($canvas, $fontSize, 0, $calculatedX - $engrave_distance + 1 , $calculatedY + $engrave_distance-1, $black, $fontFile, $text);
		//imagettftext($canvas, $fontSize, 0, $calculatedX + $engrave_distance-1 , $calculatedY + $engrave_distance-1, $black, $fontFile, $text);
		//imagettftext($canvas, $fontSize, 0, $calculatedX - $engrave_distance+1 , $calculatedY - $engrave_distance+1, $black, $fontFile, $text);
		//imagettftext($canvas, $fontSize, 0, $calculatedX + $engrave_distance-1 , $calculatedY - $engrave_distance+1, $black, $fontFile, $text);
		//imagealphablending($canvas, true);
	}

	imagettftext($canvas, $fontSize, 0, $calculatedX , $calculatedY, $fontColor, $fontFile, $text);
}	


?>