<?php


class UbWCHSD_CustomFields {


	function ActivateCustomFields() {

		add_action( 'init', [ $this, 'initiate_calls' ] );

	}

	function initiate_calls() {

		add_action( 'woocommerce_before_add_to_cart_button', [ $this, 'add_custom_fields' ] );
		add_filter( 'woocommerce_add_to_cart_validation', [ $this, 'add_fields_to_cart_validation' ], 10, 3 );
		add_filter( 'woocommerce_add_cart_item_data', [ $this, 'add_fields_to_cart' ], 10, 3 );
		add_filter( 'woocommerce_get_item_data', [ $this, 'get_custom_fields_item_data' ], 10, 2 );
		add_action( 'woocommerce_checkout_create_order_line_item', [
			$this,
			'add_custom_items_to_order_line_item',
		], 10, 3 );

	}

	public function add_custom_fields() {

		$line1_val = filter_input( INPUT_POST, 'ub_wchsd_line1' );
		$line2_val = filter_input( INPUT_POST, 'ub_wchsd_line2' );
		$note      = filter_input( INPUT_POST, 'ub_wchsd_note' );

		?>
		<div style="visibility : hidden;">
			<input type="text" id="ub_wchsd_line1" name="ub_wchsd_line1" class="hsd-line-1" maxLength="16"
				   value="<?php $line1_val ?>"/>
			<input type="text" id="ub_wchsd_line2" name="ub_wchsd_line2" class="hsd-line-2" maxLength="16"
				   value="<?php $line2_val ?>" />
			<textarea id="ub_wchsd_note" name="ub_wchsd_note" class="hsd-note" rows="3"
					  style="width : 100%;"/><?php $note ?></textarea>
		</div>

		<?php
	}


	public function add_fields_to_cart_validation( $passed, $product_id, $qty ) {

		if ( !empty( $_POST['ub_wchsd_line1'] ) && !empty( $_POST['ub_wchsd_line2'] ) ) {

			global $post, $product, $woocommerce;

			$line1_val = filter_input( INPUT_POST, 'ub_wchsd_line1' );
			$line2_val = filter_input( INPUT_POST, 'ub_wchsd_line2' );


			if ( ! $line1_val ) {

				$product = wc_get_product( $product_id );
				wc_add_notice( "Please fill in at least Line 1.", 'error' );

				return false;
			}

		}

		return $passed;

	}


	function add_fields_to_cart( $cart_item_data, $product_id, $variation_id ) {


		$line1_val = filter_input( INPUT_POST, 'ub_wchsd_line1' );
		$line2_val = filter_input( INPUT_POST, 'ub_wchsd_line2' );
		$note      = filter_input( INPUT_POST, 'ub_wchsd_note' );


		$cart_item_data['ubwchsd_line_1'] = $line1_val;
		$cart_item_data['ubwchsd_line_2'] = $line2_val;
		if ( $note != '' ) {
			$cart_item_data['ub_wchsd_note'] = $note;
		}


		return $cart_item_data;
	}


	function get_custom_fields_item_data( $item_data, $cart_item ) {


		if ( isset( $cart_item['ubwchsd_line_1'] ) ) {

			$item_data[] = [
				'key'     => __( 'Line 1', 'hwn' ),
				'display' => wc_clean( $cart_item['ubwchsd_line_1'] ),
			];
		}

		if ( isset( $cart_item['ubwchsd_line_2'] ) ) {

			$item_data[] = [
				'key'     => __( 'Line 2', 'hwn' ),
				'display' => wc_clean( $cart_item['ubwchsd_line_2'] ),
			];
		}

		if ( isset( $cart_item['ub_wchsd_note'] ) ) {

			$item_data[] = [
				'key'     => __( 'Note', 'hwn' ),
				'display' => wc_clean( $cart_item['ub_wchsd_note'] ),
			];
		}


		return $item_data;

	}


	function add_custom_items_to_order_line_item( $order_item, $cart_item_key, $cart_item_values ) {


		if ( ! empty( $cart_item_values['ubwchsd_line_1'] ) ) {
			$order_item->add_meta_data( __( 'Line 1', 'hwn' ), $cart_item_values['ubwchsd_line_1'] );
		}

		if ( ! empty( $cart_item_values['ubwchsd_line_2'] ) ) {
			$order_item->add_meta_data( __( 'Line 2', 'hwn' ), $cart_item_values['ubwchsd_line_2'] );
		}

		if ( ! empty( $cart_item_values['ub_wchsd_note'] ) ) {
			$order_item->add_meta_data( __( 'Note', 'hwn' ), $cart_item_values['ub_wchsd_note'] );
		}

	}

}


