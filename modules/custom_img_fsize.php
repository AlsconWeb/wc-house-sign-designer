<?php

$line_break = "_lbr_";

$RootFile = __FILE__;
$RootPath = dirname(dirname(__FILE__));
$fontsPath = $RootPath."/assets/fonts/";


/* Input parameters
*******************/
$line1 = $_REQUEST["hsd_line1"];
$line2 = $_REQUEST["hsd_line2"];
$pos_x1 = $_REQUEST["hsd_x1"];
$pos_y1 = $_REQUEST["hsd_y1"];
$pos_x2 = $_REQUEST["hsd_x2"];
$pos_y2 = $_REQUEST["hsd_y2"];
$line1_ratio = $_REQUEST["hsd_line1Ratio"];

$fontInput = explode('|', $_REQUEST["hsd_font_name"]);
$fontName = $fontInput[0];
$fontFile = $fontsPath."/".$fontName;


if ($pos_x1 == '' || $pos_x2 == '' || $pos_y1 == '' || $pos_y2 == '') {

    echo '0:0';
    return;
}


$diff_h = intval($pos_x2) - intval($pos_x1);
$diff_v = intval($pos_y2) - intval($pos_y1);


if ($diff_v == 0 || $diff_h == 0) {

    echo '0:0';
    return;
}


/* CHECK IF FONT FILE IS VALID
******************************/
if (!file_exists($fontFile)) {

	echo "-1|-1";
	return;
}


$rowSpacing = 0;
$proportion = 0.5;

$line1_ratioH = 0.5;

if (isset($line1_ratio) && $line1_ratio != '') {

	$validInt = filter_var($line1_ratio, FILTER_VALIDATE_INT);

	if ($validInt !== false) {

		$line1_ratioH = intval($validInt)/100;

	}


}


$area_W = abs(intval($pos_x2)-intval($pos_x1));
$area_H = abs(intval($pos_y2)-intval($pos_y1));


$line1_fontSize = 0;
$line1_calculated_H = 0;
$line1_calculated_W = 0;

$line2_fontSize = 0;
$line2_calculated_H = 0;
$line2_calculated_W = 0;

$line1_bottom_margin = 0;
$line2_bottom_margin = 0;


/* Set fontsize to max
***********************/
$fontSize = intval($area_H); /* was intval($area_H / 1.2); Needs conversion from px to pt but this is ignorable */


if ($line1_ratioH == 0.5) { /* IF LINES HAVE EQUAL HEIGHT */


		/* Choose longer text
		*********************/
		$boxLine1 = imagettfbbox($fontSize,0, $fontFile, $line1);
		$line1_calculatedW = max(abs($boxLine1[2]- $boxLine1[0]), abs($boxLine1[4]- $boxLine1[6]));

		$boxLine2 = imagettfbbox($fontSize,0, $fontFile, $line2);
		$line2_calculatedW = max(abs($boxLine2[2]- $boxLine2[0]), abs($boxLine2[4]- $boxLine2[6]));

		$firstText = ($line1_calculatedW > $line2_calculatedW || $line1_calculatedW == $line2_calculatedW) ? $line1 : $line2;
		$secondText = $firstText == $line1 ? $line2 : $line1;



		/* Calculate font size for first text
		*************************************/
		$maxW = $area_W;
		$maxH = $area_H * $proportion - $rowSpacing;


		while(true) {

			$boxLine1 = imagettfbbox($fontSize,0, $fontFile, $firstText);

			$calculatedW = min(abs($boxLine1[2]- $boxLine1[0]), abs($boxLine1[4]- $boxLine1[6]));
			$calculatedH = min(abs($boxLine1[1] - $boxLine1[7]),abs($boxLine1[3] - $boxLine1[5])); 

			$line1_fontSize = $fontSize;
			$line1_calculated_H = $calculatedH;
			$line1_calculated_W = $calculatedW;

			if ($calculatedH > $maxH || $calculatedW > $maxW) 
			{
				$fontSize--;
			}
			else {		
				break;
			}
		}


		$boxLine = imagettfbbox($fontSize,0, $fontFile, $firstText);
		$line1_bottom_margin = $boxLine[1];



		/* Check font size for second text
		**********************************/
		$line2_fontSize = $line1_fontSize;
		$boxLine2 = imagettfbbox($line1_fontSize,0, $fontFile, $secondText);

		$line2_calculated_W = max(abs($boxLine2[2]- $boxLine2[0]), abs($boxLine2[4]- $boxLine2[6]));
		$line2_calculated_H = abs($boxLine2[1] - $boxLine2[7]);

		$proportion = $line2_calculated_H == 0 ? 1 : $line1_calculated_H / $line2_calculated_H;


		$boxLine = imagettfbbox($line2_fontSize,0, $fontFile, $secondText);
		$line2_bottom_margin = $boxLine[1];


		/* Write output
		*************************/
		echo strval(intval($line1_fontSize*6/7)).'|'.strval(intval($line2_fontSize*6/7)).'|'.strval($line1_bottom_margin).'|'.strval($line2_bottom_margin);


}
else 
{


		/* Calculate font size for first text
		*************************************/
		$maxW = $area_W;
		$maxH = $area_H * $line1_ratioH - $rowSpacing;
		$fontSize = intval($area_H);


		while(true) {

			$boxLine1 = imagettfbbox($fontSize,0, $fontFile, $line1);

			$calculatedW = min(abs($boxLine1[2]- $boxLine1[0]), abs($boxLine1[4]- $boxLine1[6]));
			$calculatedH = min(abs($boxLine1[1] - $boxLine1[7]),abs($boxLine1[3] - $boxLine1[5])); 

			$line1_fontSize = $fontSize;
			$line1_calculated_H = $calculatedH;
			$line1_calculated_W = $calculatedW;

			if ($calculatedH > $maxH || $calculatedW > $maxW) 
			{
				$fontSize--;
			}
			else {		
				break;
			}
		}


		$boxLine = imagettfbbox($fontSize,0, $fontFile, $line1);
		$line1_bottom_margin = $boxLine[1];	
		$line1_fontSize = $fontSize;



		/* Calculate font size for second text
		*************************************/
		$maxW = $area_W;
		$maxH = $area_H * (1-$line1_ratioH) - $rowSpacing;
		$fontSize = intval($area_H);

		while(true) {

			$boxLine1 = imagettfbbox($fontSize,0, $fontFile, $line2);

			$calculatedW = min(abs($boxLine1[2]- $boxLine1[0]), abs($boxLine1[4]- $boxLine1[6]));
			$calculatedH = min(abs($boxLine1[1] - $boxLine1[7]),abs($boxLine1[3] - $boxLine1[5])); 

			$line2_fontSize = $fontSize;
			$line2_calculated_H = $calculatedH;
			$line2_calculated_W = $calculatedW;

			if ($calculatedH > $maxH || $calculatedW > $maxW) 
			{
				$fontSize--;
			}
			else {		
				break;
			}
		}


		$boxLine = imagettfbbox($fontSize,0, $fontFile, $line2);
		$line2_bottom_margin = $boxLine[1];	
		$line2_fontSize = $fontSize;		


		/* Write output
		*************************/
		echo strval(intval($line1_fontSize*6/7)).'|'.strval(intval($line2_fontSize*6/7)).'|'.strval($line1_bottom_margin).'|'.strval($line2_bottom_margin);

}


?>