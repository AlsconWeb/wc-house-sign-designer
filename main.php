<?php
   /*
	Main file
   */

include dirname(__FILE__).'/modules/custom_fields.php';

class UB_WCHsd_App extends UbWCHSD_CustomFields {

	public $RootFile='';
	public $RootPath='';
	public $RootURL = '';
	public $AdminRootPath='';

	protected $page_style = '';
	protected $page_html = '';

	const OPTION_PLUGIN_ENABLED = "ub_wchsd_plugin_enabled";
	const OPTION_SIZE_ATTRIBUTE = "ub_wchsd_size_attribute";
	const OPTION_FINISHING_TYPE_ATTRIBUTE = "ub_wchsd_finishing_type_attribute";
	const OPTION_TEXT_STYLE_ATTRIBUTE = "ub_wchsd_text_style_attribute";
	const OPTION_SELECTED_PRODUCT = "ub_wchsd_selected_product";

	const OPTION_REGION_X = "ub_wchsd_region_x";
	const OPTION_REGION_Y = "ub_wchsd_region_y";
	const OPTION_REGION_WIDTH = "ub_wchsd_region_width";
	const OPTION_REGION_HEIGHT = "ub_wchsd_region_height";

    const OPTION_VARIATION_REGIONS = "ub_wchsd_variation_regions";

	const OPTION_FEATURED_IMAGE = "ub_wchsd_featured_image";

	const OPTION_FALLBACK_IMAGE ="ub_wchsd_fallback_image"; /* Format : Url|Width|Height */

	public $WC_Activated = false;
	public $WC_Installed = false;
	public $WC_Version='';
	public $WC_plugin_file = '';

	private $tmp = null;

	public $DB_Table_Products = "wchsd_prods";
	public $DB_Table_Attributes = "wchsd_attr";
	public $DB_Table_Attr_Values = "wchsd_attr_vals";
	public $DB_Table_Fonts = "wchsd_fonts";
    public $DB_Table_VariationImages = "wchsd_varimages";
	public $DB_Table_CustomerPhotos = "wchsd_cust_photos";

	public $Img_Editor = "";
	public $Img_Editor_FontSize = "";

	public $Defined_Fonts;
	public $Defined_Sizes;
	public $Defined_EngravingOptions;

	public $AdminApp = null;

	public $CustomFields = null;

	public $CurrentPostID = null;


	function __construct() {


		/* Set paths
         * ****************************************/
		$this->RootFile = __FILE__;
		$this->RootPath = plugin_dir_path(__FILE__);
		$this->RootURL = plugin_dir_url(__FILE__);
		$this->AdminRootPath = plugin_dir_path(__FILE__).'modules/admin/';

		$this->Img_Editor = plugins_url('/modules/custom_img.php', $this->RootFile);
		$this->Img_Editor_FontSize = plugins_url('/modules/custom_img_fsize.php', $this->RootFile);

		/* Get WooCommerce info
		***********************/
		$this->GetWooCommerceStatus();

		$this->ActivateCustomFields();

		add_action( 'init', array($this, 'activateShortcode'));
	}


	function Run() {


		$loadProduct = false;

		if ($this->WC_Activated && $this->IsPluginEnabled()) {


			$selectedProduct = $this->GetSelectedProduct();

			if ($selectedProduct != '') {

				$selectedProductID = (explode('|', $selectedProduct))[0];

				if (intval($selectedProductID) == intval($this->CurrentPostID)) {

					$loadProduct = true;

				}

			}


		}

		if ($loadProduct) {

			$this->activate();

		}
		else {

			$this->deactivate();
		}

	}


	function deactivate() {

		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles_forDeactivation'));

		$this->page_html .= '<input type="hidden" id="ub_product_config_page_url" value="'.$this->ProductPageUrl().'">';

		$fallbackImgInfo = explode('|', $this->GetFallbackImage());

		$this->page_html .= '<input type="hidden" id="ub_fallback_img" data-url="'.$fallbackImgInfo[0].'" data-width="'.$fallbackImgInfo[1].'" data-height="'.$fallbackImgInfo[2].'" value="">';

		$this->page_html .= '<style>.ub_slide-no-image {
							position: absolute;
							left : 0;
							top : 0;
							width: 100%;
							height: 100%;
							background: rgba(0,0,0,0.5) url('.$fallbackImgInfo[0].') 50% 50% no-repeat;
							background-size: auto 100%;
							-webkit-border-radius: 8px;
							-moz-border-radius: 8px;
							-ms-border-radius: 8px;
							-o-border-radius: 8px;
							border-radius: 8px;	
							z-index : 10000;
							display: none;
						}</style>';

		add_action('wp_footer', array($this, 'add_html_before_footer'));

	}


	function activate() {


		/* Run initial checks
		*********************/

		$this->runInitialChecks();


		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_styles'));

		$this->movePriceTagToTop();

		add_action( 'woocommerce_before_single_product', array($this,'my_remove_variation_price'));


		/* Get variation names and embed in document
		********************************************/
		$this->embedVariationNamesToDoc();

		add_action('wp_footer', array($this, 'add_html_before_footer'));


		/* Disable image zooming
		************************/
		add_action( 'wp', array($this,'custom_remove_product_zoom'), 99 );


		/* Disable srcset
		*******************/
		add_filter( 'max_srcset_image_width', array($this, 'remove_max_srcset_image_width'));
		add_filter( 'wp_calculate_image_srcset', array($this, 'wdo_disable_srcset'));

		//add_action( 'wp_enqueue_scripts', array($this, 'add_custom_js'), 9999, 1 );  Removed on 11.06.2023


		/* Remove clear button
		*****************************************/
		add_filter( 'woocommerce_reset_variations_link', '__return_empty_string', 9999 );


		/* Remove ligtbox
		*****************************************/
		add_action( 'wp', array($this, 'my_remove_lightbox'), 99 );


		/* Add shortcode for Products page
		**********************************/
		//add_action( 'init', array($this, 'activateShortcode'));


	}

	function activateShortcode() {

		add_shortcode('ub_display_customer_photos', array($this, 'displayCustomerPhotos'));
		add_shortcode('ub_display_customer_photos_grouped', array($this, 'displayCustomerPhotosGroupedByFonts'));

	}

	function ProductPageUrl() {

		$savedProduct = $this->GetSelectedProduct();
		$savedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		return get_permalink($savedProductID);

	}

	function displayCustomerPhotosGroupedByFonts($attributes) {


		$args = shortcode_atts(array(
			'slate_size' => ''
		), $attributes, 'ub_display_customer_photos_grouped');


		$selectedSize = strtolower($args['slate_size']);


		if ($selectedSize == '') return '';


		/* START
		********/
		global $wpdb;

		$product_page_url = $this->ProductPageUrl();


		$customer_photos_table = $wpdb->prefix.$this->DB_Table_CustomerPhotos;

		$query = "SELECT * FROM `$customer_photos_table` WHERE `size_for_class`='".$selectedSize."' ORDER BY `font_name` ASC";
		$results = $wpdb->get_results($query);

		$saved_FontName = '';
		$saved_FinishingOptions = '';
		$saved_Price = '';

		$outputs = [];
		$processedFonts = '';

		foreach($results as $result) {


			$attachment_id = $result->image_attach_id;
			$fontName = $result->font_name;
			$finishingOption = $result->engraving_option;
			$variationPrice = $result->variation_price;
			$selectedSize = $result->size_in_attribute;


			/* Set attributes for URL
			*************************/
			$url_attrs = sprintf('?attribute_select-size=%1$s&attribute_text-style=%2$s&attribute_engraving-options=%3$s', $selectedSize, $fontName, $finishingOption);
			$slide_target_url = $product_page_url.str_replace(" ", "+", $url_attrs).'&attribute_fixing-options=Drill+Holes+%26+Fixing+Kit';

			/* Set parameters
			*****************/
			$img_url = wp_get_attachment_image_url($attachment_id, 'large');
			$finishing_options_class = stripos($finishingOption, "engrave") > -1 ? "slate-engraved" : "slate-painted";



			$output = '<div class="ub_slide-image '.$finishing_options_class.'" ';
			$output .= 'data-fontName="'.$fontName.'" data-finishingOption="'.$finishingOption.'" data-price="'.$variationPrice.'" data-url="'.$slide_target_url.'">';
			$output .= stripos($processedFonts, $fontName) === false ? '<img loading="eager" ' : '<img loading="lazy" decoding="async"  class="img-lazy-load" ';
			$output .= 'src="'.$img_url.'">';
			$output .= '</div>';

			if (!isset($outputs[$fontName])) $outputs[] = $fontName;

			$outputs[$fontName] .= $output;


			if (stripos($processedFonts, $fontName) === false ) $processedFonts .= $fontName.',';

		}


		$processedFonts = substr($processedFonts, 0, strlen($processedFonts)-1);

		$arrFonts = explode(',', $processedFonts);

		$output2 = '<ul class="ub_single-slider">';

		foreach($arrFonts as $font) {



			$output2 .= '<li><div class="ub_slide-block-wrapper"><div class="ub_slide-no-image"><p><span>Awaiting image</span></p></div><div class="ub_slider-block">'.$outputs[$font];

			$output2 .= '</div><p class="ub_slider-block-font-name">Times New Roma</p><p class="ub_slider-block-finishing-option">Unpainted white</p><p class="ub_slider-block-rating" data-rating="★★★★★">★★★★★</p><p class="ub_slider-block-price has-background">&pound;30.0</p><div class="ub_slider-block-button clearBoth"><a href="#">PERSONALIZE YOUR SLATE</a></div></div></li>';


		}

		$output2 .= '</ul>';




		return $output2;







	}

	function displayCustomerPhotos($attributes) {


		$args = shortcode_atts(array(
			'slate_size' => ''
		), $attributes, 'ub_display_customer_photos');


		$selectedSize = strtolower($args['slate_size']);

		if ($selectedSize == '') return '';


		/* START
		********/
		global $wpdb;

		$product_page_url = $this->ProductPageUrl();



		$customer_photos_table = $wpdb->prefix.$this->DB_Table_CustomerPhotos;

		$query = "SELECT * FROM `$customer_photos_table` WHERE `size_for_class`='".$selectedSize."'";
		$results = $wpdb->get_results($query);


		$output = '<div class="ub_slide-block-wrapper">
					<div class="ub_slide-no-image"><p><span>Awaiting image</span></p></div>
					<div class="ub_slider-block">';

		$img_index = 0;

		$saved_FontName = '';
		$saved_FinishingOptions = '';
		$saved_Price = '';

		foreach($results as $result) {

			$attachment_id = $result->image_attach_id;
			$fontName = $result->font_name;
			$finishingOption = $result->engraving_option;
			$variationPrice = $result->variation_price;
			$selectedSize = $result->size_in_attribute;

			if ($img_index == 0) {

				$saved_FontName = $fontName;
				$saved_FinishingOptions = $finishingOption;
				$saved_Price = $variationPrice;

			}


			/* Set attributes for URL
			*************************/
			$url_attrs = sprintf('?attribute_select-size=%1$s&attribute_text-style=%2$s&attribute_engraving-options=%3$s', $selectedSize, $fontName, $finishingOption);
			$slide_target_url = $product_page_url.str_replace(" ", "+", $url_attrs).'&attribute_fixing-options=Drill+Holes+%26+Fixing+Kit';

			/* Set parameters
			*****************/
			$img_url = wp_get_attachment_image_url($attachment_id, 'large');
			$finishing_options_class = stripos($finishingOption, "engrave") > -1 ? "slate-engraved" : "slate-painted";



			$output .= '<div class="ub_slide-image '.$finishing_options_class.'" data-slide="'.($img_index+1).'"';
			$output .= 'data-fontName="'.$fontName.'" data-finishingOption="'.$finishingOption.'" data-price="'.$variationPrice.'" data-url="'.$slide_target_url.'">';
			$output .= $img_index == 0 ? '<img loading="eager" ' : '<img loading="lazy" decoding="async"  class="img-lazy-load" ';
			$output .= 'src="'.$img_url.'">';
			$output .= '</div>';

			$img_index++;

		}


		$output .= '</div>
														
						<p class="ub_slider-block-font-name">'.$saved_FontName.'</p>
						<p class="ub_slider-block-finishing-option">'.$saved_FinishingOptions.'</p>
						<p class="ub_slider-block-rating">★★★★★</p>
						<p class="ub_slider-block-price has-background">&pound;'.$saved_Price.'</p>

						<div class="ub_slider-block-button clearBoth">

							<a href="#">PERSONALIZE YOUR SLATE</a>

						</div>

					</div>';


		return $output;









	}


	function runInitialChecks() {

		$wcProuductsCount = $this->GetNumberOfWCVariableProducts();

		if ($wcProuductsCount == 0) $this->ResetAll(false);

	}


	/* Remove lightbox
	***********************/

	function my_remove_lightbox() {

	   remove_theme_support( 'wc-product-gallery-lightbox' );

	}

	/* Disable genuine woocommerce js file
	**************************************/
	function add_custom_js(){

		if( is_shop() || is_product()){

			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_deregister_script( 'wc-add-to-cart-variation' );

			$version = WC_VERSION;

			// Register the script
			wp_register_script( 'wc-add-to-cart-variation', $this->RootURL.'/assets/frontend/js/add-to-cart-variation.js', array( 'jquery', 'wp-util', 'jquery-blockui' ), $version );

			// Localize the script with new data

			$params = array(
				'wc_ajax_url'                      => WC_AJAX::get_endpoint( '%%endpoint%%' ),
				'i18n_no_matching_variations_text' => esc_attr__( 'Sorry, no products matched your selection. Please choose a different combination.', 'woocommerce' ),
				'i18n_make_a_selection_text'       => esc_attr__( 'Please select some product options before adding this product to your cart.', 'woocommerce' ),
				'i18n_unavailable_text'            => esc_attr__( 'Sorry, this product is unavailable. Please choose a different combination.', 'woocommerce' ),
			);

			wp_localize_script( 'wc-add-to-cart-variation', 'wc_add_to_cart_variation_params', $params );

			// Enqueued script with localized data.
			wp_enqueue_script( 'wc-add-to-cart-variation' );

		}
	}




	/* Embed variation names in document
	************************************/
	function embedVariationNamesToDoc() {

		$attrSlug_Size = $this->GetSizeAttributeField();
		$attrSlug_Finishing = $this->GetFinishingTypeAttributeField();
		$attrSlug_TextStyle = $this->GetTextStyleAttributeField();

		$this->page_html .= '<input type="hidden" id="ubwchsd_attr_slug_size" value="'.$attrSlug_Size.'">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_attr_slug_finishing_type" value="'.$attrSlug_Finishing.'">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_attr_slug_text_style" value="'.$attrSlug_TextStyle.'">';

		$font_options = '';
		$PluginFonts = $this->FetchAllFonts();

		foreach($PluginFonts as $font) {

			$font_options .= '<option value="'.$font->font_file_name.'">'.$font->font_name.'</option>';

		}

		$this->page_html .= '<input type="hidden" id="ubwchsd_fonts_list" name="ubwchsd_fonts_list" value=\''.json_encode($PluginFonts, JSON_FORCE_OBJECT).'\'>';


		/* Get selected region parameters
		*********************************/
		//$regionParams = $this->GetRegionParameters();

		$this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_x" name="ubwchsd_selected_region_x" value="">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_y" name="ubwchsd_selected_region_y" value="">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_w" name="ubwchsd_selected_region_w" value="">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_h" name="ubwchsd_selected_region_h" value="">';

		$this->page_html .= '<input type="hidden" id="ubwchsd_img_editor_url" name="ubwchsd_img_editor_url" value="'.$this->Img_Editor.'">';

		$this->page_html .= '<input type="hidden" id="ubwchsd_img_editor_url_fsize" name="ubwchsd_img_editor_url_fsize" value="'.$this->Img_Editor_FontSize.'">';

		$savedProduct = $this->GetSelectedProduct();

		$savedProductID = $savedProduct == '' ? '' : explode('|', $savedProduct)[0];

		$this->page_html .= '<input type="hidden" id="ubwchsd_saved_product_id" name="ubwchsd_saved_product_id" value="'.$savedProductID.'">';


        $this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_x_large" name="ubwchsd_selected_region_x" value="">';
        $this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_y_large" name="ubwchsd_selected_region_y" value="">';
        $this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_w_large" name="ubwchsd_selected_region_w" value="">';
        $this->page_html .= '<input type="hidden" id="ubwchsd_selected_region_h_large" name="ubwchsd_selected_region_h" value="">';


		$this->page_html .= '<input type="hidden" id="ubwchsd_url_featured_img" name="ubwchsd_url_featured_img" value="">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_url_featured_img_large" name="ubwchsd_url_featured_img_large" value="">';

		$this->page_html .= '<input type="hidden" id="ubwchsd_large_img_w" name="ubwchsd_large_img_w" value="">';
		$this->page_html .= '<input type="hidden" id="ubwchsd_large_img_h" name="ubwchsd_large_img_h" value="">';

        $this->page_html .= get_option($this::OPTION_VARIATION_REGIONS);

	}


	/* Disable srcset
	******************/
	function remove_max_srcset_image_width( $max_width )
	{
		return false;
	}

	function wdo_disable_srcset( $sources )
	{
		return false;
	}



	/* Customize product page
	**************************/

	// Hide product price range
	function my_remove_variation_price() {

	  global $product;

	  if ( $product->is_type( 'variable' ) )
	  {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price' );
	  }

	}



	// Move price to top
	function movePriceTagToTop() {

		remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
		add_action( 'woocommerce_before_variations_form', 'woocommerce_single_variation', 10 );

	}


	/* DISABLE IMAGE ZOOMING
	************************/
	function custom_remove_product_zoom() {

		remove_theme_support( 'wc-product-gallery-zoom' );

	}


	/* Check if single product page
	 * ****************************/
	function Check_WC_SinglePage() {

		$this->wc_is_product_page = is_singular('product');
		$this->tmp = get_post();
	}


	function add_html_before_footer() {

		echo $this->page_html;

	}



	/* Functions for checking and setting plugin enabled/disabled
     * status
     * **********************************************************/

	public function IsPluginEnabled() {

		return get_option($this::OPTION_PLUGIN_ENABLED) == "1";

    }

	public function EnablePlugin() {

		update_option($this::OPTION_PLUGIN_ENABLED, "1");

    }

	public function DisablePlugin() {

		update_option($this::OPTION_PLUGIN_ENABLED, "0");

    }


	/* Functions for getting and setting SIZE attribute
	***********************************************************/
	public function GetSizeAttributeField() {

		return get_option($this::OPTION_SIZE_ATTRIBUTE,'');

	}

	public function SetSizeAttributeField($sizeAttr) {

		update_option($this::OPTION_SIZE_ATTRIBUTE, $sizeAttr);

	}



	/* Functions for getting and setting FINISHING TYPE attribute
	***********************************************************/
	public function GetFinishingTypeAttributeField() {

		return get_option($this::OPTION_FINISHING_TYPE_ATTRIBUTE,'');

	}

	public function SetFinishingTypeAttributeField($finishingTypeAttr) {

		update_option($this::OPTION_FINISHING_TYPE_ATTRIBUTE, $finishingTypeAttr);

	}


	/* Functions for getting and setting TEXT STYLE attribute
	***********************************************************/
	public function GetTextStyleAttributeField() {

		return get_option($this::OPTION_TEXT_STYLE_ATTRIBUTE,'');

	}

	public function SetTextStyleAttributeField($textStyleAttr) {

		update_option($this::OPTION_TEXT_STYLE_ATTRIBUTE, $textStyleAttr);

	}


	/* Functions for getting and setting SELECTED PRODUCT
	*****************************************************/
	public function GetSelectedProduct() {

		$result = get_option($this::OPTION_SELECTED_PRODUCT,'xxx');

		return ($result == 'xxx' ? '' : $result);

	}

	public function SetSelectedProduct($selectedProduct) {


		update_option($this::OPTION_SELECTED_PRODUCT, $selectedProduct);

		$productID = explode('|', $selectedProduct)[0];


		/* Set selected product in table
		********************************/
		global $wpdb;

		$products_table = $wpdb->prefix.$this->DB_Table_Products;

		$query1 = "UPDATE `$products_table` SET is_selected=0";
		$query2 = "UPDATE `$products_table` SET is_selected=1 WHERE product_id=%d";

		$wpdb->query($query1);
		$wpdb->query($wpdb->prepare($query2, array($productID)));


		/* CHECK ATTRIBUTES
		*******************/
		$product = new WC_Product_Variable($productID);


		/* Get attributes array
		***************************/
		$variation_attributes = $product->get_variation_attributes();

		$totalVarAttr = count($variation_attributes);


		/* Delete all attributes and variations
		***************************************/
		$this->AdminApp->DeleteAllAttributesAndVariations();


		/* Crete all attributes
		*****************************************/
		$atts = $this->AdminApp->CreateAttributes();
		$product->set_attributes( $atts );
		$product->save();

		wc_delete_product_transients( $productID);


		/* Create all variations
		************************/
		$this->AdminApp->ImportAttributesFromWC();
		$this->AdminApp->CreateMaxVariations(true);


		/* Update variation prices
		***************************/
		$this->AdminApp->UpdateVariationPrices();


	}


	/* Functions for setting selected region parameters
	*****************************************************/
	public function GetRegionParameters() {

		$posX = get_option($this::OPTION_REGION_X,'');
		$posY = get_option($this::OPTION_REGION_Y,'');
		$width = get_option($this::OPTION_REGION_WIDTH,'');
		$height = get_option($this::OPTION_REGION_HEIGHT,'');

		return [$posX, $posY, $width, $height];
	}

	public function SetRegionParameters($regionParams) {

		update_option($this::OPTION_REGION_X, $regionParams[0]);
		update_option($this::OPTION_REGION_Y, $regionParams[1]);
		update_option($this::OPTION_REGION_WIDTH, $regionParams[2]);
		update_option($this::OPTION_REGION_HEIGHT, $regionParams[3]);

	}


	public function GetSavedFeaturedImage() {

		return get_option($this::OPTION_FEATURED_IMAGE,'');

	}

	public function SetSavedFeatureImage($featImage) {

		update_option($this::OPTION_FEATURED_IMAGE, $featImage);

	}


	/* Function for getting fallback image
	*******************************************/
	public function GetFallbackImage() {

		return get_option($this::OPTION_FALLBACK_IMAGE, '');

	}



	/* Font functions
	************************/
	public function FetchAllFonts() {

		global $wpdb;

		$this->ProcessFonts();

		$fonts_table = $wpdb->prefix.$this->DB_Table_Fonts;

		$query = "SELECT * FROM `$fonts_table` ORDER BY `font_name`";

		return $wpdb->get_results($query);

	}

	public function ProcessFonts() {

		global $wpdb;

		$fonts_table = $wpdb->prefix.$this->DB_Table_Fonts;

		$total_defined_fonts = count($this->Defined_Fonts)-1;

		$query_fonts_count = "SELECT COUNT(`font_name`) as TotalFonts FROM `$fonts_table`";
		$objTotalFonts = $wpdb->get_var($query_fonts_count);

		$total_fonts_in_table = isset($objTotalFonts) ? $objTotalFonts[0][0] : 0;

		if ($total_fonts_in_table != $total_defined_fonts) {

			$query_delete_fonts = "DELETE FROM `$fonts_table`";
			$wpdb->query($query_delete_fonts);

			for($i=1; $i <= $total_defined_fonts; $i++) {

				$fontInfo = explode(',', $this->Defined_Fonts[$i]);

				$font_name = $fontInfo[0];
				$font_file = $fontInfo[1];
				$font_group = $fontInfo[2];

				$query_insert_font = "INSERT INTO `$fonts_table` VALUES(%s, %s, %s)";
				$wpdb->query($wpdb->prepare($query_insert_font, array($font_name, $font_file, $font_group)));

			}

		}

	}


	public function GetNumberOfWCVariableProducts() {

		global $wpdb, $woocommerce, $product;

		try {


			$args = array(
				'status'            => 'publish',
				'type'              => 'variable',
				'limit'				=> -1,
				'return'			=> 'ids',
			);


			$products = wc_get_products($args);


			return count($products);

		}
		catch(Exception $err){

			return 0;

		}
	}

	public function GetAllProducts() {

		global $wpdb;

		$products_table = $wpdb->prefix.$this->DB_Table_Products;

		$query = "SELECT * FROM `$products_table`";

		return $wpdb->get_results($query);
	}

	public function TotalProductsInPluginTable() {

		global $wpdb;

		$products_table = $wpdb->prefix.$this->DB_Table_Products;

		$query = "SELECT COUNT(product_id) FROM `$products_table`";
		$objTotalMatching = $wpdb->get_var($query);

		$result = isset($objTotalMatching) ? $objTotalMatching[0][0] : 0;

		return $result;

	}

	public function TotalAttributesInPluginTable($product_id) {

		global $wpdb;

		$attributes_table = $wpdb->prefix.$this->DB_Table_Attributes;

		$query = "SELECT COUNT(id) FROM `$attributes_table` WHERE `product_id`=%d";
		$objTotalMatching = $wpdb->get_var($wpdb->prepare($query, array($product_id)));

		return $objTotalMatching[0][0];

	}

	public function ResetAll($dropTables=true) {


		update_option($this::OPTION_SIZE_ATTRIBUTE, '');
		update_option($this::OPTION_FINISHING_TYPE_ATTRIBUTE, '');
		update_option($this::OPTION_SELECTED_PRODUCT, '');

		update_option($this::OPTION_REGION_X, '');
		update_option($this::OPTION_REGION_Y, '');
		update_option($this::OPTION_REGION_WIDTH, '');
		update_option($this::OPTION_REGION_HEIGHT, '');

		global $wpdb;

		$products_table = $wpdb->prefix.$this->DB_Table_Products;
		$attributes_table = $wpdb->prefix.$this->DB_Table_Attributes;
		$attribute_values_table = $wpdb->prefix.$this->DB_Table_Attr_Values;

		$query_delete = $dropTables ? "DROP TABLE `$attributes_table`" : "DELETE FROM `$attributes_table`";
		$wpdb->query($query_delete);

		$query_delete = $dropTables ? "DROP TABLE `$products_table`" : "DELETE FROM `$products_table`";
		$wpdb->query($query_delete);

		$query_delete = $dropTables ? "DROP TABLE `$attribute_values_table`" : "DELETE FROM `$attribute_values_table`";
		$wpdb->query($query_delete);

	}



	/* Check WooCommerce
     * ******************/
	public function GetWooCommerceStatus() {

		if (!function_exists('get_plugins')) require_once(ABSPATH.'wp-admin/includes/plugin.php');

        $plugin_folder = get_plugins('/'.'woocommerce');
        $plugin_file = 'woocommerce.php';


        if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {

			$this->WC_Version = $plugin_folder[$plugin_file]['Version'];
			$this->WC_Installed = true;

			$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));

			$this->WC_Activated = in_array('woocommerce/woocommerce.php', array_map('strtolower', $active_plugins)) ? true : false;

        }

    }

	/* FRONTEND SCRIPTS
     * ****************/
	function enqueue_scripts_styles() {

		wp_dequeue_script('ubwchsd-deact');
		wp_deregister_script('ubwchsd-deact');


		$style_file_url = plugins_url('assets/frontend/css/common.css',__FILE__ );
		$style_file_path = $this->RootPath.'assets/frontend/css/common.css';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-common', $style_file_url, 'style', $style_file_path);

		$script_file_url = plugins_url('assets/frontend/js/common.js', $this->RootFile);
		$script_file_path = $this->RootPath.'assets/frontend/js/common.js';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-common', $script_file_url, 'script', $script_file_path, array('jquery'));

		$script_file_url1 = plugins_url('assets/frontend/js/ubslider.js', $this->RootFile);
		$script_file_path1 = $this->RootPath.'assets/frontend/js/ubslider.js';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-slider', $script_file_url1, 'script', $script_file_path1, array('jquery'));



    }


	function enqueue_scripts_styles_forDeactivation() {


		$style_file_url = plugins_url('assets/frontend/css/common.css',__FILE__ );
		$style_file_path = $this->RootPath.'assets/frontend/css/common.css';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-common', $style_file_url, 'style', $style_file_path);

		$script_file_url = plugins_url('assets/frontend/js/common_deact.js', $this->RootFile);
		$script_file_path = $this->RootPath.'assets/frontend/js/common_deact.js';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-deact', $script_file_url, 'script', $script_file_path, array('jquery'));

		/*
		$script_file_url1 = plugins_url('assets/frontend/js/ubslider.js', $this->RootFile);
		$script_file_path1 = $this->RootPath.'assets/frontend/js/ubslider.js';
		$this->enqueue_scripts_styles_with_filetime_as_version('ubwchsd-slider', $script_file_url1, 'script', $script_file_path1, array('jquery'));
		*/
    }


	function enqueue_scripts_styles_with_filetime_as_version($my_handle, $relpath, $type, $file_full_path, $my_deps=array()) {

		$uri = $relpath;
		$vsn = filemtime($file_full_path);
		if($type == 'script') wp_enqueue_script($my_handle, $uri, $my_deps, $vsn);
		else if($type == 'style') wp_enqueue_style($my_handle, $uri, $my_deps, $vsn);

	}


	/* Check if GD installed and enabled
     * *********************************/
	public function isGDEnabled() {

        return (extension_loaded('gd') & function_exists('gd_info'));

    }


}
